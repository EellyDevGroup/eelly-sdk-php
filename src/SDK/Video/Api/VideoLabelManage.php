<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Video\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class VideoLabelManage
{
    /**
     * 获取短视频标签列表
     *
     * @param array $condition  查询条件
     * @return array
     *
     * @author chentuying
     * @since 2020-09-09
     *
     * @internal
     */
    public function getLabelList(array $condition = []): array
    {
        return EellyClient::request('video/videoLabelManage', 'getLabelList', true, $condition);
    }

    /**
     * 更新短视频标签
     *
     * @param int   $labelId
     * @param array $data
     * @return bool
     *
     * @author chentuying
     * @since 2020-09-09
     *
     * @internal
     */
    public function updateLabel(int $labelId, array $data): bool
    {
        return EellyClient::request('video/videoLabelManage', 'updateLabel', true, $labelId, $data);
    }

    /**
     * 批量更新短视频标签
     *
     * @param array $labelIds
     * @param array $data
     * @return int  更新条数
     *
     * @author chentuying
     * @since 2020-09-09
     *
     * @internal
     */
    public function updateLabelBatch(array $labelIds, array $data): int
    {
        return EellyClient::request('video/videoLabelManage', 'updateLabelBatch', true, $labelIds, $data);
    }

    /**
     * 删除短视频标签
     *
     * @param int $labelId
     * @return bool
     *
     * @author chentuying
     * @since 2020-09-09
     *
     * @internal
     */
    public function deleteLabel(int $labelId): bool
    {
        return EellyClient::request('video/videoLabelManage', 'deleteLabel', true, $labelId);
    }

    /**
     * 获取短视频标签接口配置
     *
     * @return array
     *
     * @author chentuying
     * @since 2020-09-09
     *
     * @internal
     */
    public function getLabelApiConfig(): array
    {
        return EellyClient::request('video/videoLabelManage', 'getLabelApiConfig', true);
    }

    /**
     * 获取一条短视频标签
     *
     * @param int $labelId
     * @return array
     *
     * @author chentuying
     * @since 2020-09-09
     *
     * @internal
     */
    public function getLabel(int $labelId): array
    {
        return EellyClient::request('video/videoLabelManage', 'getLabel', true, $labelId);
    }

    /**
     * 添加短视频标签
     *
     * @param array $data
     * @return bool
     *
     * @author chentuying
     * @since 2020-09-10
     *
     * @internal
     */
    public function addLabel(array $data): bool
    {
        return EellyClient::request('video/videoLabelManage', 'addLabel', true, $data);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}