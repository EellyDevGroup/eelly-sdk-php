<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\Video\Api;

use Eelly\SDK\EellyClient as Client20200109;
use Eelly\SDK\GetInstanceTrait;

/**
 * This class has been auto-generated by shadon compiler (2020-01-09 06:22:00).
 */
class VideoStore
{
    use GetInstanceTrait;
    /**
     * 更新短视频店铺信息.
     *
     * @param int   $storeId 店铺ID
     * @param array $values  店铺数据
     *
     * @return bool
     *
     * @Async
     *
     * @internal
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function updateStoreInfo(int $storeId, array $values) : bool
    {
        return Client20200109::requestJson('video/videoStore', 'updateStoreInfo', ['storeId' => $storeId, 'values' => $values], true);
    }
    /**
     * 更新短视频店铺信息.
     *
     * @param int   $storeId 店铺ID
     * @param array $values  店铺数据
     *
     * @return bool
     *
     * @Async
     *
     * @internal
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function updateStoreInfoAsync(int $storeId, array $values)
    {
        return Client20200109::requestJson('video/videoStore', 'updateStoreInfo', ['storeId' => $storeId, 'values' => $values], false);
    }
    /**
     * 获取后台短视频店铺管理列表数据
     *
     * @param array $conditions 查询条件
     * @param int $page   页码
     * @param int $limit  每页显示数量
     * @return array
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.12.10
     *
     * @internal
     */
    public static function adminListVideoStore(array $conditions, int $page = 1, int $limit = 50) : array
    {
        return Client20200109::requestJson('video/videoStore', 'adminListVideoStore', ['conditions' => $conditions, 'page' => $page, 'limit' => $limit], true);
    }
    /**
     * 获取后台短视频店铺管理列表数据
     *
     * @param array $conditions 查询条件
     * @param int $page   页码
     * @param int $limit  每页显示数量
     * @return array
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.12.10
     *
     * @internal
     */
    public static function adminListVideoStoreAsync(array $conditions, int $page = 1, int $limit = 50)
    {
        return Client20200109::requestJson('video/videoStore', 'adminListVideoStore', ['conditions' => $conditions, 'page' => $page, 'limit' => $limit], false);
    }
    /**
     * 后台短视频店铺管理统计数据接口
     *
     * @return array
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.12.11
     */
    public static function adminVideoTotalData() : array
    {
        return Client20200109::requestJson('video/videoStore', 'adminVideoTotalData', [], true);
    }
    /**
     * 后台短视频店铺管理统计数据接口
     *
     * @return array
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.12.11
     */
    public static function adminVideoTotalDataAsync()
    {
        return Client20200109::requestJson('video/videoStore', 'adminVideoTotalData', [], false);
    }
}