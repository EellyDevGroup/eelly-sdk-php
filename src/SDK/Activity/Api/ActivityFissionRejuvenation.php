<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Activity\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author chenyuhua<1243035045@qq.com>
 */
class ActivityFissionRejuvenation
{
    public static function sendPullNewCoupon(int $userId, int $type): bool
    {
        return EellyClient::requestJson('activity/activityFissionRejuvenation', __FUNCTION__, [
            'userId' => $userId,
            'type' => $type,
        ]);
    }

    /**
     * 支付回调检测并插入数据
     *
     * @param int $userId
     * @return bool
     */
    public static function addRewardLogByCallBack(int $userId): bool
    {
        return EellyClient::requestJson('activity/activityFissionRejuvenation', __FUNCTION__, [
            'userId' => $userId,
        ]);
    }
}