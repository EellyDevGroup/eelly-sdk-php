<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Activity\Api;

use Eelly\SDK\EellyClient;
use Eelly\DTO\UidDTO;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class ActivitySpreadProfit
{
    /**
     * 订单状态变更，更新用户推广收益信息
     *
     * @param array $data 请求参数
     * @param int data[type] 0.啥都不敢 1.订单支付 2.交易完成 3取消订单
     * @param int data[orderId] 订单id
     * @return bool
     *
     * @author wechan
     * @since 2019年7月27日
     */
    public function changeActivitySpreadProfit(array $data = []):bool
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__, [
            'data' => $data,
        ]);
    }
  
    /**
     * 我的收益列表
     */
    public function listMyIncome(string $type = '1', string $dateMonth = '', UidDTO $user = null):array
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__,  [
            'type' => $type,
            'dateMonth' => $dateMonth,
            'user' => $user
        ]);
    }
  
    /**
     * 累计收益列表
     */
    public function listTotalIncome(UidDTO $user = null):array
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__,  [
            'user' => $user
        ]);
    }

    /**
     * 收益文本内容
     */
    public function IncomeContent():array
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__,  []);
    }

    /**
     * 粉丝攻略
     */
    public function fanRaiders():array
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__,  []);
    }

    /**
     * 根据用户id获取收益相关的数据
     */
    public function getIncomeInfoByUserId(int $userId):array
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__,  [
            'userId' => $userId
        ]);
    }

    public function getIncomeInfoByUserIdAsync(int $userId)
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', 'getIncomeInfoByUserId',  [
            'userId' => $userId
        ], false);
    }

    /**
     * @inheritdoc
     */
    public function listIncomeData(array $userIds):array
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__,  [
            'userIds' => $userIds
        ]);
    }

    /**
     * @inheritdoc
     */
    public function listSpreadProfitData(array $conditions, int $page = 1, int $limit = 50, string $order = 'created_time DESC'):array
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__,  [
            'conditions' => $conditions,
            'page' => $page,
            'limit' => $limit,
            'order' => $order,
        ]);
    }

    public function getLiveManageSpreadStat(array $conditions): array
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__,  [
            'conditions' => $conditions
        ]);
    }

    public function getOrderSpreadType(array $orderIds): array
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__,  [
            'orderIds' => $orderIds
        ]);
    }

    public function getOrderSpreadDetail(int $orderId): array
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__,  [
            'orderId' => $orderId
        ]);
    }

    /**
     * 招粉丝赚钱接口
     *
     * ### 返回数据说明
     *  参数        | 类型  | 说明
     * ----------- | ---- |-----
     * fanTitle | string | 直属粉丝标题
     * fanContent | array | 直属粉丝文案内容
     * fanContent['content'] | string | 文案内容
     * fanContent['text']    | string | 需要标红的文本
     * nextTitle | string | 下级粉丝标题
     * nextContent | array | 下级粉丝文案内容
     * nextContent['content'] | string | 文案内容
     * nextContent['text']    | string | 需要标红的文本
     * inviteStr | string | 底部邀请文案内容
     * ruleUrl | string | 活动规则链接地址
     * list | array | 返回的分享动态数据
     *
     * @param int $userId 用户id(不传就默认是游客身份)
     * @return array
     */
    public function listFanEarnMoney(int $userId = 0):array
    {
        return EellyClient::requestJson('activity/activitySpreadProfit', __FUNCTION__,  [
            'userId' => $userId
        ]);
    }


    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
