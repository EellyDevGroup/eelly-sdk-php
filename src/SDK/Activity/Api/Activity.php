<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Activity\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\Activity\Service\ActivityInterface;
use Eelly\SDK\Activity\DTO\ActivityDTO;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class Activity
{
    /**
     * 根据活动id获取活动信息.
     *
     * @param array $activityId 活动id
     *
     * @throws \Eelly\SDK\Activity\Exception\ActivityException
     *
     * @return ActivityDTO 单条活动结果
     * @requestExample({"activityId": 1})
     * @returnExample({"data":{"activityId":2,"storeId":0,"title":"活动","image":"/G0/G4/xxxxxxxx","content":"活动","applyStartTime":1504317208,"applyEndTime":1504317208,"activityStartTime":1504317208,"activityEndTime":1504317208,"dayStartTime":1504317208,"dayEndTime":1504317208,"status":"0","sort":"1","range":"0","adminId":1,"adminName":"molimoq","remark":"molimoq","createdTime":null},"returnType":"Eelly\\SDK\\Activity\\DTO\\ActivityDTO"})
     *
     * @author wechan<liweiquan@eelly.net>
     *
     * @since 2017年9月2日
     */
    public function getActivity(int $activityId): ActivityDTO
    {
        return EellyClient::request('activity/activity', __FUNCTION__, true, $activityId);
    }

    /**
     * 根据活动id获取活动信息.
     *
     * @param array $activityId 活动id
     *
     * @throws \Eelly\SDK\Activity\Exception\ActivityException
     *
     * @return ActivityDTO 单条活动结果
     * @requestExample({"activityId": 1})
     * @returnExample({"data":{"activityId":2,"storeId":0,"title":"活动","image":"/G0/G4/xxxxxxxx","content":"活动","applyStartTime":1504317208,"applyEndTime":1504317208,"activityStartTime":1504317208,"activityEndTime":1504317208,"dayStartTime":1504317208,"dayEndTime":1504317208,"status":"0","sort":"1","range":"0","adminId":1,"adminName":"molimoq","remark":"molimoq","createdTime":null},"returnType":"Eelly\\SDK\\Activity\\DTO\\ActivityDTO"})
     *
     * @author wechan<liweiquan@eelly.net>
     *
     * @since 2017年9月2日
     */
    public function getActivityAsync(int $activityId)
    {
        return EellyClient::request('activity/activity', __FUNCTION__, false, $activityId);
    }

    /**
     * 根据活动id获取活动信息.
     *
     * @param array  $data                      活动id
     * @param string $data['title']             活动标题
     * @param string $data['image']             活动图片
     * @param string $data['content']           活动内容
     * @param string $data['applyStartTime']    报名开始时间
     * @param string $data['applyEndTime']      报名结束结束
     * @param string $data['activityStartTime'] 活动开始时间
     * @param string $data['activityEndTime']   活动结束时间
     * @param string $data['dayStartTime']      活动每天开始时间：0 不限，每天的第N秒数
     * @param string $data['dayEndTime']        活动每天结束时间：0 不限，每天的第N秒数，结束时间比开始时间大
     * @param string $data['status']            活动状态：0 未审核 1 审核通过 2 审核失败
     * @param int    $data['sort']              活动优先级排序
     * @param int    $data['range']             活动影响范围：0 平台级 1 店铺级 2 商品级
     * @param int    $data['adminId']           管理员ID
     * @param string $data['adminName']         管理员名称
     * @param string $data['remark']            活动备注
     * @param string $data['createdTime']       添加时间
     *
     * @throws \Eelly\SDK\Activity\Exception\ActivityException
     *
     * @return bool sql操作结果
     * @requestExample({"data":{"title":"\u6d3b\u52a8","image":"\/G0\/G4\/xxxxxxxx","content":"\u6d3b\u52a8","applyStartTime":1504317208,"applyEndTime":1504317208,"activityStartTime":1504317208,"activityEndTime":1504317208,"dayStartTime":1504317208,"dayEndTime":1504317208,"status":0,"sort":1,"range":0,"adminId":1,"adminName":"molimoq","remark":"\u6d3b\u52a8","createdTime":1504317208}})
     * @returnExample(true)
     *
     * @author wechan<liweiquan@eelly.net>
     *
     * @since 2017年9月2日
     */
    public function addActivity(array $data): bool
    {
        return EellyClient::request('activity/activity', __FUNCTION__, true, $data);
    }

    /**
     * 根据活动id获取活动信息.
     *
     * @param array  $data                      活动id
     * @param string $data['title']             活动标题
     * @param string $data['image']             活动图片
     * @param string $data['content']           活动内容
     * @param string $data['applyStartTime']    报名开始时间
     * @param string $data['applyEndTime']      报名结束结束
     * @param string $data['activityStartTime'] 活动开始时间
     * @param string $data['activityEndTime']   活动结束时间
     * @param string $data['dayStartTime']      活动每天开始时间：0 不限，每天的第N秒数
     * @param string $data['dayEndTime']        活动每天结束时间：0 不限，每天的第N秒数，结束时间比开始时间大
     * @param string $data['status']            活动状态：0 未审核 1 审核通过 2 审核失败
     * @param int    $data['sort']              活动优先级排序
     * @param int    $data['range']             活动影响范围：0 平台级 1 店铺级 2 商品级
     * @param int    $data['adminId']           管理员ID
     * @param string $data['adminName']         管理员名称
     * @param string $data['remark']            活动备注
     * @param string $data['createdTime']       添加时间
     *
     * @throws \Eelly\SDK\Activity\Exception\ActivityException
     *
     * @return bool sql操作结果
     * @requestExample({"data":{"title":"\u6d3b\u52a8","image":"\/G0\/G4\/xxxxxxxx","content":"\u6d3b\u52a8","applyStartTime":1504317208,"applyEndTime":1504317208,"activityStartTime":1504317208,"activityEndTime":1504317208,"dayStartTime":1504317208,"dayEndTime":1504317208,"status":0,"sort":1,"range":0,"adminId":1,"adminName":"molimoq","remark":"\u6d3b\u52a8","createdTime":1504317208}})
     * @returnExample(true)
     *
     * @author wechan<liweiquan@eelly.net>
     *
     * @since 2017年9月2日
     */
    public function addActivityAsync(array $data)
    {
        return EellyClient::request('activity/activity', __FUNCTION__, false, $data);
    }

    /**
     * 更新活动信息.
     *
     * @param int    $activityId                活动id
     * @param array  $data                      活动数据
     * @param string $data['title']             活动标题
     * @param string $data['image']             活动图片
     * @param string $data['content']           活动内容
     * @param string $data['applyStartTime']    报名开始时间
     * @param string $data['applyEndTime']      报名结束结束
     * @param string $data['activityStartTime'] 活动开始时间
     * @param string $data['activityEndTime']   活动结束时间
     * @param string $data['dayStartTime']      活动每天开始时间：0 不限，每天的第N秒数
     * @param string $data['dayEndTime']        活动每天结束时间：0 不限，每天的第N秒数，结束时间比开始时间大
     * @param string $data['status']            活动状态：0 未审核 1 审核通过 2 审核失败
     * @param int    $data['sort']              活动优先级排序
     * @param int    $data['range']             活动影响范围：0 平台级 1 店铺级 2 商品级
     * @param int    $data['adminId']           管理员ID
     * @param string $data['adminName']         管理员名称
     * @param string $data['remark']            活动备注
     *
     * @throws \Eelly\SDK\Activity\Exception\ActivityException
     *
     * @return bool sql操作结果
     * @requestExample({"activityId":1,"data":{"title":"\u6d3b\u52a8","image":"\/G0\/G4\/xxxxxxxx","content":"\u6d3b\u52a8","applyStartTime":1504317208,"applyEndTime":1504317208,"activityStartTime":1504317208,"activityEndTime":1504317208,"dayStartTime":1504317208,"dayEndTime":1504317208,"status":0,"sort":1,"range":0,"adminId":1,"adminName":"molimoq","remark":"\u6d3b\u52a8"}})
     * @returnExample(true)
     *
     * @author wechan<liweiquan@eelly.net>
     *
     * @since 2017年9月2日
     */
    public function updateActivity(int $activityId, array $data): bool
    {
        return EellyClient::request('activity/activity', __FUNCTION__, true, $activityId, $data);
    }

    /**
     * 更新活动信息.
     *
     * @param int    $activityId                活动id
     * @param array  $data                      活动数据
     * @param string $data['title']             活动标题
     * @param string $data['image']             活动图片
     * @param string $data['content']           活动内容
     * @param string $data['applyStartTime']    报名开始时间
     * @param string $data['applyEndTime']      报名结束结束
     * @param string $data['activityStartTime'] 活动开始时间
     * @param string $data['activityEndTime']   活动结束时间
     * @param string $data['dayStartTime']      活动每天开始时间：0 不限，每天的第N秒数
     * @param string $data['dayEndTime']        活动每天结束时间：0 不限，每天的第N秒数，结束时间比开始时间大
     * @param string $data['status']            活动状态：0 未审核 1 审核通过 2 审核失败
     * @param int    $data['sort']              活动优先级排序
     * @param int    $data['range']             活动影响范围：0 平台级 1 店铺级 2 商品级
     * @param int    $data['adminId']           管理员ID
     * @param string $data['adminName']         管理员名称
     * @param string $data['remark']            活动备注
     *
     * @throws \Eelly\SDK\Activity\Exception\ActivityException
     *
     * @return bool sql操作结果
     * @requestExample({"activityId":1,"data":{"title":"\u6d3b\u52a8","image":"\/G0\/G4\/xxxxxxxx","content":"\u6d3b\u52a8","applyStartTime":1504317208,"applyEndTime":1504317208,"activityStartTime":1504317208,"activityEndTime":1504317208,"dayStartTime":1504317208,"dayEndTime":1504317208,"status":0,"sort":1,"range":0,"adminId":1,"adminName":"molimoq","remark":"\u6d3b\u52a8"}})
     * @returnExample(true)
     *
     * @author wechan<liweiquan@eelly.net>
     *
     * @since 2017年9月2日
     */
    public function updateActivityAsync(int $activityId, array $data)
    {
        return EellyClient::request('activity/activity', __FUNCTION__, false, $activityId, $data);
    }

    /**
     * 删除单条活动.
     *
     * @param array $activityId 活动id
     *
     * @throws \Eelly\SDK\Activity\Exception\ActivityException
     *
     * @return array 单条活动结果
     * @requestExample({"activityId": 1})
     * @returnExample(true)
     *
     * @author wechan<liweiquan@eelly.net>
     *
     * @since 2017年9月2日
     */
    public function deleteActivity(int $activityId): bool
    {
        return EellyClient::request('activity/activity', __FUNCTION__, true, $activityId);
    }

    /**
     * 删除单条活动.
     *
     * @param array $activityId 活动id
     *
     * @throws \Eelly\SDK\Activity\Exception\ActivityException
     *
     * @return array 单条活动结果
     * @requestExample({"activityId": 1})
     * @returnExample(true)
     *
     * @author wechan<liweiquan@eelly.net>
     *
     * @since 2017年9月2日
     */
    public function deleteActivityAsync(int $activityId)
    {
        return EellyClient::request('activity/activity', __FUNCTION__, false, $activityId);
    }

    /**
     * 获取省钱月卡活动数据
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2020.09.01
     */
    public function getActivityCoupon(): array
    {
        return EellyClient::request('activity/activity', __FUNCTION__, true);
    }

    /**
     * 编辑拉新活动
     *
     * @param array  $params
     * @param int    $params['activityId']  活动ID 添加时可不传
     * @param string $params['title']       活动标题
     * @param int    $params['activityStartTime']     活动开始时间
     * @param int    $params['activityEndTime']       活动结束时间
     * @param int    $params['pulledCondition']['type']  被邀请人奖励达成方式 0 注册+安装APP 1 领取新人红包
     * @param int    $params['pulledCondition']['acoId'] 被邀请人奖励优惠券ID
     * @param int    $params['pullCondition']['target']  邀请人奖励完成金额指标
     * @param array  $params['pullCondition']['rewards'] 邀请人数对应奖励设置
     * @param array  $params['pullCondition']['startNum'] 邀请人数
     * @param array  $params['pullCondition']['endNum']   邀请人数
     * @param array  $params['pullCondition']['amount']   邀请人数对应奖励金额
     *
     * @return bool
     *
     * @author chenyuhua
     * @since 2020.10.13
     */
    public function addPullNewActivity(array $params): bool
    {
        return EellyClient::request('activity/activity', __FUNCTION__, true, $params);
    }

    /**
     * 获取拉新活动信息
     *
     * @param int $activityId 活动ID
     * @return array
     *
     * @author chenyuhua
     * @since 2020.10.13
     */
    public function getPullNewActivity(int $activityId): array
    {
        return EellyClient::request('activity/activity', __FUNCTION__, true, $activityId);
    }

    /**
     * 获取指定范围内的拉新活动数据
     *
     * @param array $params 查询条件
     * @return array
     *
     * @author chenyuhua
     * @since 2020.10.13
     */
    public function getPullNewActivityList(array $params, int $page, int $limit): array
    {
        return EellyClient::request('activity/activity', __FUNCTION__, true, $params, $page, $limit);
    }

    /**
     * 返回需要统计数据的拉新活动
     *
     * @param int $endTime 活动结束时间
     * @return array
     *
     * @author chenyuhua
     * @since 2020.10.13
     */
    public function getSummaryPullNewActivity(int $endTime): array
    {
        return EellyClient::request('activity/activity', __FUNCTION__, true, $endTime);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}