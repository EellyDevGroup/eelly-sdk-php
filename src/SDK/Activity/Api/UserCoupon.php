<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Activity\Api;

use Eelly\DTO\UidDTO;
use Eelly\SDK\EellyClient;

/**
 * class UserCoupon
 * 
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class UserCoupon
{
    public static function checkIsSendWechatCoupon(int $userId, int $couponStockId): bool
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'userId' => $userId,
            'couponStockId' => $couponStockId
        ]);
    }

    public static function receive(int $acoId, int $issueFlag, UidDTO $user = null): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'acoId' => $acoId,
            'issueFlag' => $issueFlag,
            'user' => $user
        ]);
    }

    public static function receiveBySync(int $acoId, int $issueFlag, string $couponSn = '', int $userId): bool
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'acoId' => $acoId,
            'issueFlag' => $issueFlag,
            'couponSn' => $couponSn,
            'userId' => $userId
        ]);
    }

    public static function receiveForInternal(int $acoId, int $issueFlag, string $couponSn = '', int $userId): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'acoId' => $acoId,
            'issueFlag' => $issueFlag,
            'couponSn' => $couponSn,
            'userId' => $userId
        ]);
    }

    public static function getAvailableWechatCouponList(): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, []);
    }

    public static function getAvailablePlatformNewCostomerCouponList(): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, []);
    }

    public static function getUserAvailableWechatCouponList(int $appIdType, int $userId): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'appIdType' => $appIdType,
            'userId' => $userId
        ]);
    }

    public static function getAllStoreLowestCoupon(int $userId): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'userId' => $userId
        ]);
    }

    public static function groupChatCouponList(int $storeId): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'storeId' => $storeId
        ]);
    }

    public static function getAllStoreNewcomerExclusive(): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, []);
    }

    public static function getUserCouponDetail(int $userId, int $acuId): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'userId' => $userId,
            'acuId' => $acuId
        ]);
    }

    public static function userUseCoupon(int $userId, int $acuId): bool
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'userId' => $userId,
            'acuId' => $acuId
        ]);
    }

    public function checkOrderToGetStoreCouponList(int $storeId, int $userId, int $totalAmount): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'storeId' => $storeId,
            'userId' => $userId,
            'totalAmount' => $totalAmount
        ]);
    }

    public static function countLiveCouponAmount(int $storeId)
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'storeId' => $storeId
        ]);
    }

    public static function countUserCoupon(int $userId): int
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'userId' => $userId
        ]);
    }

    public static function getUserValidCoupon(int $amount, int $userId, int $acuId, int $storeId)
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'amount' => $amount,
            'userId' => $userId,
            'acuId' => $acuId,
            'storeId' => $storeId
        ]);
    }
    
    public static function liveCouponList(int $storeId, int $userId): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'storeId' => $storeId,
            'userId' => $userId
        ]);
    }

    public function getUserCouponAcuId(int $acuId):array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'acuId' => $acuId,
        ]);
    }

    public static function countStoreOpenCoupon(int $storeId):int
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'storeId' => $storeId,
        ]);
    }

    public static function sendLiveRoomCoupon(int $liveId, int $storeId, int $userId, int $type): array
    {
        return EellyClient::requestJson('activity/activityReward', 'sendLiveRoomCoupon', [
            'liveId' => $liveId,
            'storeId' => $storeId,
            'userId' => $userId,
            'type' => $type
        ], true);
    }

    public static function checkUserSignInReceive(int $userId): bool
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'userId' => $userId,
        ]);
    }

    public static function checkUserNewcomersCoupon(int $userId): bool
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'userId' => $userId,
        ]);
    }

    public static function checkUserHaveNewRegister(int $userId): bool
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'userId' => $userId,
        ]);
    }

    public static function getUserNewRegisterCoupon(int $userId): array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'userId' => $userId,
        ]);
    }

    /**
     * 批量查看店铺是否有优惠券
     *
     * @param array $storeIds  店铺id
     *
     * @return array
     *
     * @author chentuying
     * @since 2020-12-10
     */
    public static function getStoreCouponInfo(array $storeIds):array
    {
        return EellyClient::requestJson('activity/userCoupon', __FUNCTION__, [
            'storeIds' => $storeIds,
        ]);
    }
}
