<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Activity\Api;

use Eelly\SDK\EellyClient;

/**
 * class GoodsStockActivity
 * 
 * @author wechan
 */
class GoodsStockActivity
{
    /**
     * 添加备货节抽奖次数
     *
     * @param int $orderId 订单id
     *
     * @return bool
     *
     * @author wehcan
     * @since 2020年09月21日
     */
    public function addLuckDrawTime(int $orderId):bool
    {
        return EellyClient::requestJson('activity/goodsStockActivity', __FUNCTION__, [
            'orderId' => $orderId,
        ]);
    }
}
