<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\Activity\Api;

use Eelly\SDK\EellyClient as Client20200618;
use Eelly\SDK\GetInstanceTrait;
use Eelly\DTO\UidDTO;

/**
 * This class has been auto-generated by shadon compiler (2020-06-18 02:25:03).
 */
class MonthCoupon
{
    use GetInstanceTrait;
    /**
     * 商品详情月卡banner.
     *
     * ### 未开通月卡的用户（含游客）
     * 开省钱月卡享160元权益，此单减5元
     * 去看看
     *
     * ### 已开通月卡且未到期用户
     * 尊敬的月卡会员，还剩xx元可领
     * 去领券
     *
     * ### 月卡已过期的用户
     * 续费省钱月卡享160元权益，此单减5元
     * 去续费
     *
     * @param int $uid
     *
     * @return array
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function monthCardBanner(int $uid) : array
    {
        return Client20200618::requestJson('activity/monthCoupon', 'monthCardBanner', ['uid' => $uid], true);
    }
    /**
     * 商品详情月卡banner.
     *
     * ### 未开通月卡的用户（含游客）
     * 开省钱月卡享160元权益，此单减5元
     * 去看看
     *
     * ### 已开通月卡且未到期用户
     * 尊敬的月卡会员，还剩xx元可领
     * 去领券
     *
     * ### 月卡已过期的用户
     * 续费省钱月卡享160元权益，此单减5元
     * 去续费
     *
     * @param int $uid
     *
     * @return array
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function monthCardBannerAsync(int $uid)
    {
        return Client20200618::requestJson('activity/monthCoupon', 'monthCardBanner', ['uid' => $uid], false);
    }
    /**
     * 月卡会员引导页.
     *
     * ### 返回字段说明
     *
     * 字段|类型|说明
     * --|--|--
     * helpUrl | string | 帮助链接
     * totalMoney | int | 平台券金额总额+店铺优惠券总额
     * platformMoney |int | 平台券金额总额
     * storeMoney |int| 店铺优惠券总额
     * signInNum |int | 补签卡次数
     * freightTimes |int | 运费补贴次数
     * totalLive | int | 直播总数
     * specialPrice | string | 限时特价按钮文案
     * monthCard | object | 已购月卡数据信息
     * monthCard.username  | string | 用户名
     * monthCard.expireIn  | string | 有效期 (空字符串表示未购买)
     * monthCard.avatar  | string |  头像
     * monthCard.notify | bool | 是否开启提醒
     * monthCard.monthInfo | object | 本月情况
     * monthCard.monthInfo.money | int | 本月剩余金额
     * monthCard.monthInfo.perDay | int | 每日可领
     * monthCard.monthInfo.get | int | 已领
     * monthCard.monthInfo.total | int | 总共
     * openMessages | string[] | 当前开通或续费的用户消息
     * platformCards | array | 平台卡券列表
     * platformCards[].cardId | int | 卡券ID
     * platformCards[].money | int | 卡券金额
     * platformCards[].min | string | 最低使用金额（满5.01元可用）
     * platformCards[].status | int | 优惠券状态: 0 没按钮(未登录) 1 领取(待领取) 2 待解锁(点击后吐司提示“今日已领 明日再来”) 3 去使用 4 已使用 5 已过期(未使用，但时间已过期) 6 已失效
     * storeCards | array | 店铺卡券列表
     * storeCards[].cardId | int | 卡券ID
     * storeCards[].money | int | 卡券金额
     * storeCards[].min | string | 最低使用金额（满5.01元可用）
     * storeCards[].status | int | 优惠券状态: 0 未领 1 已领
     * storeCards[].storeId | int | 店铺ID
     * storeCards[].storeName | string | 店铺名
     * liveList | array | 直播列表（同首页直播）
     * liveList[].saveMoney | string | 每单省钱金额文案（开卡省5元/单）
     * agreement | object | 省钱月卡协议
     * agreement.title | string | 标题
     * agreement.contents | string[] | 内容列表
     * agreement.linked | object | 链接对象
     * agreement.linked.url | string | 链接
     * agreement.linked.title | string | 链接标题
     * serviceTool | object | 增值服务参数
     * serviceTool.cid | int | 月卡ID
     * serviceTool.tid | int | 工具ID
     *
     * @returnExample(
     *     {
     *     "helpUrl":"https://help.eelly.com/abc.html",
     *  "totalMoney": 129,
     *  "platformMoney": 50,
     *  "storeMoney": 100,
     *  "signInNum": 7,
     *  "freightTimes": 2,
     *  "totalLive": 102,
     *  "specialPrice": "限时特价￥9.9/月开通",
     *  "monthCard": {
     *  "username": "捡破烂的鱼",
     *  "expireIn": "有效期至2020.06.29",
     *  "avatar": "https:\/\/img06.eelly.com\/noavatar.png",
     *  "notify": false,
     *  "storeCards": [
     *  {
     *  "cardId": 1,
     *  "money": 5,
     *  "min": "满5.01元可使用",
     *  "status": 0,
     *  "storeId":123444,
     *  "storeName": "韩国小姐旗舰店"
     *  }
     *  ],
     *  "monthInfo": {
     *  "money": 30,
     *  "perDay": 1,
     *  "get": 0,
     *  "total": 8
     *  }
     *  },
     *  "openMessages": [
     *  "V6超客 唐*龙 成功开通月卡",
     *  "V9超客 唐*坤 成功续费月卡"
     *  ],
     *  "platformCards": [
     *  {
     *  "cardId": 1,
     *  "money": 5,
     *  "min": "满5.01元可使用",
     *  "status": 0
     *  },
     *  {
     *  "cardId": 2,
     *  "money": 10,
     *  "min": "满300元可使用",
     *  "status": 0
     *  }
     *  ],
     *  "liveList": [
     *  {
     *  "liveId": "601057",
     *  "userId": "2286762",
     *  "storeId": "2286762",
     *  "title": "0602若琳",
     *  "image": "https:\/\/eellytest.oss-cn-shenzhen.aliyuncs.com\/user955330\/other\/20190731\/7834498554651.gif?t=1564558945",
     *  "region": "0",
     *  "pushUrl": "rtmp:\/\/push.eelly.com\/live\/3344_test_601057?bizid=3344&txSecret=843d2e8b97f84a478c19956f9ef351b4&txTime=5ED7E170",
     *  "share": "0",
     *  "scheduleTime": "1591059600",
     *  "startTime": "1591076655",
     *  "endTime": "1591077600",
     *  "validDate": "1603641600",
     *  "lpId": "20",
     *  "planDate": "20200602",
     *  "isPay": "1",
     *  "status": "1",
     *  "showFlag": "15",
     *  "liveFlag": "0",
     *  "sort": "250",
     *  "subscribeTime": "1591055950",
     *  "liveType": "6",
     *  "createdTime": "1572073562",
     *  "updateTime": "2020-06-02 13:44:16",
     *  "isContinuousBroadCast": 0,
     *  "isStoreVip": 0,
     *  "isFirstStyle": 0,
     *  "activityLogo": "",
     *  "businessTag": "0",
     *  "storeName": "若琳的小店444",
     *  "marketName": "",
     *  "userName": "若琳的小店444",
     *  "districtName": "",
     *  "saveMoney": "开卡省5元/单"
     *  },
     *  {
     *  "liveId": "601423",
     *  "userId": "158252",
     *  "storeId": "158252",
     *  "title": "0528",
     *  "image": "https:\/\/eellytest.oss-cn-shenzhen.aliyuncs.com\/store158252\/goods\/20191022\/3583858071751.jpg?t=1571708584",
     *  "region": "0",
     *  "pushUrl": "rtmp:\/\/push.eelly.com\/live\/3344_test_601423?bizid=3344&txSecret=c3f5274e146acf51ddffcda79e0a1b5b&txTime=5ED15689",
     *  "share": "0",
     *  "scheduleTime": "1590595200",
     *  "startTime": "1590647881",
     *  "endTime": "1590678000",
     *  "validDate": "1593532799",
     *  "lpId": "24",
     *  "planDate": "20200528",
     *  "isPay": "0",
     *  "status": "1",
     *  "showFlag": "15",
     *  "liveFlag": "0",
     *  "sort": "250",
     *  "subscribeTime": "1590647869",
     *  "liveType": "5",
     *  "createdTime": "1590461780",
     *  "updateTime": "2020-05-28 14:38:03",
     *  "isContinuousBroadCast": 1,
     *  "isStoreVip": 0,
     *  "isFirstStyle": 0,
     *  "activityLogo": "",
     *  "businessTag": "0",
     *  "storeName": "窈窕衣色",
     *  "marketName": "",
     *  "userName": "窈窕衣色",
     *  "districtName": "",
     *  "saveMoney": "开卡省5元/单"
     * }
     * ],
     *     "agreement":{
     *          "title": "省钱月卡开通说明",
     *          "contents": [
     *                "1、省钱月卡自开通之日起，30天内有效。例如你于2020年6月1日10：00：00开通省钱月卡，则有效期至2020年7月1日23：59：59。",
     *                "2、开通省钱月卡后，需要你主动来领取，平台不会自动发放，平台通用券每日限领1个，领完即止。",
     *                "3、省钱月卡的平台通用券，支持跨店满减，可与店铺优惠券叠加使用，需订单金额（运费除外）满足优惠券满减条件即可使用。",
     *                "4、省钱月卡开通后不支持退款。更多规则说明请仔细阅读《衣联V+省钱月卡服务协议》。"
     *          ],
     *     "linked": {
     *           "url": "https://hd.eelly.com/guize/index.html?app=article_mobile&act=getMobileView&id=52974",
     *           "title": "《衣联V+省钱月卡服务协议》"
     *     }
     *    },
     *     "serviceTool": {
     *         "tid": 123,
     *         "cid": 456
     *     }
     * })
     *
     * @author hehui<runphp@dingtalk.com>
     *
     * @ChangeLog([
     *     ["2020-06-01","新增接口","hehui<runphp@dingtalk.com>"]
     * ])
     */
    public static function guidePage() : array
    {
        return Client20200618::requestJson('activity/monthCoupon', 'guidePage', [], true);
    }
    /**
     * 月卡会员引导页.
     *
     * ### 返回字段说明
     *
     * 字段|类型|说明
     * --|--|--
     * helpUrl | string | 帮助链接
     * totalMoney | int | 平台券金额总额+店铺优惠券总额
     * platformMoney |int | 平台券金额总额
     * storeMoney |int| 店铺优惠券总额
     * signInNum |int | 补签卡次数
     * freightTimes |int | 运费补贴次数
     * totalLive | int | 直播总数
     * specialPrice | string | 限时特价按钮文案
     * monthCard | object | 已购月卡数据信息
     * monthCard.username  | string | 用户名
     * monthCard.expireIn  | string | 有效期 (空字符串表示未购买)
     * monthCard.avatar  | string |  头像
     * monthCard.notify | bool | 是否开启提醒
     * monthCard.monthInfo | object | 本月情况
     * monthCard.monthInfo.money | int | 本月剩余金额
     * monthCard.monthInfo.perDay | int | 每日可领
     * monthCard.monthInfo.get | int | 已领
     * monthCard.monthInfo.total | int | 总共
     * openMessages | string[] | 当前开通或续费的用户消息
     * platformCards | array | 平台卡券列表
     * platformCards[].cardId | int | 卡券ID
     * platformCards[].money | int | 卡券金额
     * platformCards[].min | string | 最低使用金额（满5.01元可用）
     * platformCards[].status | int | 优惠券状态: 0 没按钮(未登录) 1 领取(待领取) 2 待解锁(点击后吐司提示“今日已领 明日再来”) 3 去使用 4 已使用 5 已过期(未使用，但时间已过期) 6 已失效
     * storeCards | array | 店铺卡券列表
     * storeCards[].cardId | int | 卡券ID
     * storeCards[].money | int | 卡券金额
     * storeCards[].min | string | 最低使用金额（满5.01元可用）
     * storeCards[].status | int | 优惠券状态: 0 未领 1 已领
     * storeCards[].storeId | int | 店铺ID
     * storeCards[].storeName | string | 店铺名
     * liveList | array | 直播列表（同首页直播）
     * liveList[].saveMoney | string | 每单省钱金额文案（开卡省5元/单）
     * agreement | object | 省钱月卡协议
     * agreement.title | string | 标题
     * agreement.contents | string[] | 内容列表
     * agreement.linked | object | 链接对象
     * agreement.linked.url | string | 链接
     * agreement.linked.title | string | 链接标题
     * serviceTool | object | 增值服务参数
     * serviceTool.cid | int | 月卡ID
     * serviceTool.tid | int | 工具ID
     *
     * @returnExample(
     *     {
     *     "helpUrl":"https://help.eelly.com/abc.html",
     *  "totalMoney": 129,
     *  "platformMoney": 50,
     *  "storeMoney": 100,
     *  "signInNum": 7,
     *  "freightTimes": 2,
     *  "totalLive": 102,
     *  "specialPrice": "限时特价￥9.9/月开通",
     *  "monthCard": {
     *  "username": "捡破烂的鱼",
     *  "expireIn": "有效期至2020.06.29",
     *  "avatar": "https:\/\/img06.eelly.com\/noavatar.png",
     *  "notify": false,
     *  "storeCards": [
     *  {
     *  "cardId": 1,
     *  "money": 5,
     *  "min": "满5.01元可使用",
     *  "status": 0,
     *  "storeId":123444,
     *  "storeName": "韩国小姐旗舰店"
     *  }
     *  ],
     *  "monthInfo": {
     *  "money": 30,
     *  "perDay": 1,
     *  "get": 0,
     *  "total": 8
     *  }
     *  },
     *  "openMessages": [
     *  "V6超客 唐*龙 成功开通月卡",
     *  "V9超客 唐*坤 成功续费月卡"
     *  ],
     *  "platformCards": [
     *  {
     *  "cardId": 1,
     *  "money": 5,
     *  "min": "满5.01元可使用",
     *  "status": 0
     *  },
     *  {
     *  "cardId": 2,
     *  "money": 10,
     *  "min": "满300元可使用",
     *  "status": 0
     *  }
     *  ],
     *  "liveList": [
     *  {
     *  "liveId": "601057",
     *  "userId": "2286762",
     *  "storeId": "2286762",
     *  "title": "0602若琳",
     *  "image": "https:\/\/eellytest.oss-cn-shenzhen.aliyuncs.com\/user955330\/other\/20190731\/7834498554651.gif?t=1564558945",
     *  "region": "0",
     *  "pushUrl": "rtmp:\/\/push.eelly.com\/live\/3344_test_601057?bizid=3344&txSecret=843d2e8b97f84a478c19956f9ef351b4&txTime=5ED7E170",
     *  "share": "0",
     *  "scheduleTime": "1591059600",
     *  "startTime": "1591076655",
     *  "endTime": "1591077600",
     *  "validDate": "1603641600",
     *  "lpId": "20",
     *  "planDate": "20200602",
     *  "isPay": "1",
     *  "status": "1",
     *  "showFlag": "15",
     *  "liveFlag": "0",
     *  "sort": "250",
     *  "subscribeTime": "1591055950",
     *  "liveType": "6",
     *  "createdTime": "1572073562",
     *  "updateTime": "2020-06-02 13:44:16",
     *  "isContinuousBroadCast": 0,
     *  "isStoreVip": 0,
     *  "isFirstStyle": 0,
     *  "activityLogo": "",
     *  "businessTag": "0",
     *  "storeName": "若琳的小店444",
     *  "marketName": "",
     *  "userName": "若琳的小店444",
     *  "districtName": "",
     *  "saveMoney": "开卡省5元/单"
     *  },
     *  {
     *  "liveId": "601423",
     *  "userId": "158252",
     *  "storeId": "158252",
     *  "title": "0528",
     *  "image": "https:\/\/eellytest.oss-cn-shenzhen.aliyuncs.com\/store158252\/goods\/20191022\/3583858071751.jpg?t=1571708584",
     *  "region": "0",
     *  "pushUrl": "rtmp:\/\/push.eelly.com\/live\/3344_test_601423?bizid=3344&txSecret=c3f5274e146acf51ddffcda79e0a1b5b&txTime=5ED15689",
     *  "share": "0",
     *  "scheduleTime": "1590595200",
     *  "startTime": "1590647881",
     *  "endTime": "1590678000",
     *  "validDate": "1593532799",
     *  "lpId": "24",
     *  "planDate": "20200528",
     *  "isPay": "0",
     *  "status": "1",
     *  "showFlag": "15",
     *  "liveFlag": "0",
     *  "sort": "250",
     *  "subscribeTime": "1590647869",
     *  "liveType": "5",
     *  "createdTime": "1590461780",
     *  "updateTime": "2020-05-28 14:38:03",
     *  "isContinuousBroadCast": 1,
     *  "isStoreVip": 0,
     *  "isFirstStyle": 0,
     *  "activityLogo": "",
     *  "businessTag": "0",
     *  "storeName": "窈窕衣色",
     *  "marketName": "",
     *  "userName": "窈窕衣色",
     *  "districtName": "",
     *  "saveMoney": "开卡省5元/单"
     * }
     * ],
     *     "agreement":{
     *          "title": "省钱月卡开通说明",
     *          "contents": [
     *                "1、省钱月卡自开通之日起，30天内有效。例如你于2020年6月1日10：00：00开通省钱月卡，则有效期至2020年7月1日23：59：59。",
     *                "2、开通省钱月卡后，需要你主动来领取，平台不会自动发放，平台通用券每日限领1个，领完即止。",
     *                "3、省钱月卡的平台通用券，支持跨店满减，可与店铺优惠券叠加使用，需订单金额（运费除外）满足优惠券满减条件即可使用。",
     *                "4、省钱月卡开通后不支持退款。更多规则说明请仔细阅读《衣联V+省钱月卡服务协议》。"
     *          ],
     *     "linked": {
     *           "url": "https://hd.eelly.com/guize/index.html?app=article_mobile&act=getMobileView&id=52974",
     *           "title": "《衣联V+省钱月卡服务协议》"
     *     }
     *    },
     *     "serviceTool": {
     *         "tid": 123,
     *         "cid": 456
     *     }
     * })
     *
     * @author hehui<runphp@dingtalk.com>
     *
     * @ChangeLog([
     *     ["2020-06-01","新增接口","hehui<runphp@dingtalk.com>"]
     * ])
     */
    public static function guidePageAsync()
    {
        return Client20200618::requestJson('activity/monthCoupon', 'guidePage', [], false);
    }
    /**
     * 获取用户平台优惠券金额.
     *
     * @param int $uid
     *
     * @return int
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function getUserPlatformMoney(int $uid) : int
    {
        return Client20200618::requestJson('activity/monthCoupon', 'getUserPlatformMoney', ['uid' => $uid], true);
    }
    /**
     * 获取用户平台优惠券金额.
     *
     * @param int $uid
     *
     * @return int
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function getUserPlatformMoneyAsync(int $uid)
    {
        return Client20200618::requestJson('activity/monthCoupon', 'getUserPlatformMoney', ['uid' => $uid], false);
    }
    /**
     * 店铺优惠券列表.
     *
     * 同 guidePage 接口的 storeCards 的内容
     *
     * @param string $page   第几页
     * @param string $limit  分页大小
     * @param UidDTO $uidDTO 登录用户
     *
     * @return array
     *
     * @returnExample([{
     *   "cardId":"92",
     *   "money":"5",
     *   "min":"满10元可使用",
     *   "status":"0",
     *   "storeId":154545,
     *   "storeName":"韩国小姐旗舰店"
     *}])
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function storeCardsList(string $page = '1', string $limit = '10', UidDTO $uidDTO = null) : array
    {
        return Client20200618::requestJson('activity/monthCoupon', 'storeCardsList', ['page' => $page, 'limit' => $limit], true);
    }
    /**
     * 店铺优惠券列表.
     *
     * 同 guidePage 接口的 storeCards 的内容
     *
     * @param string $page   第几页
     * @param string $limit  分页大小
     * @param UidDTO $uidDTO 登录用户
     *
     * @return array
     *
     * @returnExample([{
     *   "cardId":"92",
     *   "money":"5",
     *   "min":"满10元可使用",
     *   "status":"0",
     *   "storeId":154545,
     *   "storeName":"韩国小姐旗舰店"
     *}])
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function storeCardsListAsync(string $page = '1', string $limit = '10', UidDTO $uidDTO = null)
    {
        return Client20200618::requestJson('activity/monthCoupon', 'storeCardsList', ['page' => $page, 'limit' => $limit], false);
    }
    /**
     * 月卡提醒设置.
     *
     * @param bool   $flag   开启或关闭
     * @param UidDTO $uidDTO
     *
     * @return bool
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function notifySetting(bool $flag = true, UidDTO $uidDTO = null) : bool
    {
        return Client20200618::requestJson('activity/monthCoupon', 'notifySetting', ['flag' => $flag], true);
    }
    /**
     * 月卡提醒设置.
     *
     * @param bool   $flag   开启或关闭
     * @param UidDTO $uidDTO
     *
     * @return bool
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function notifySettingAsync(bool $flag = true, UidDTO $uidDTO = null)
    {
        return Client20200618::requestJson('activity/monthCoupon', 'notifySetting', ['flag' => $flag], false);
    }
    /**
     * 推荐的用券商品分页数据.
     *
     * ### 返回字段说明
     *
     * 字段|类型|说明
     * --|--|--
     * todayGoodsCount |int| 今日新款数量
     * page | object | 分页数据
     * page.totalPage | int | 总页数
     * page.currentPage | int | 当前页码
     * items | array | 商品列表
     * items[].goodsId | int |商品ID
     * items[].goodsName | string |商品名
     * items[].storeName | string |店铺名
     * items[].price | float |商品价格(日常价)
     * items[].couponPrice | int |券后价
     * items[].image | string |商品封面图地址
     * items[].video | string |视频地址
     * items[].isVideo | string |1 视频商品 2 非视频商品
     *
     * @param string $page  第几页
     * @param string $limit 分页大小
     *
     * @return array
     *
     * @returnExample({
     *  "todayGoodsCount":1200,
     *  "items":[
     *      {
     *      "storeName":"测试店铺名",
     *      "goodsId":1234,
     *      "goodsName":"商品标题标题标题",
     *      "price":19.9,
     *      "couponPrice":14.9,
     *      "image":"https://img06.eelly.com",
     *      "video":"https://img06.eelly.com",
     *      "isVideo":"1"
     *      }
     *  ],
     *  "page":{
     *      "totalPage":17,
     *      "currentPage":1
     *  }})
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function goodsList(string $page = '1', string $limit = '12') : array
    {
        return Client20200618::requestJson('activity/monthCoupon', 'goodsList', ['page' => $page, 'limit' => $limit], true);
    }
    /**
     * 推荐的用券商品分页数据.
     *
     * ### 返回字段说明
     *
     * 字段|类型|说明
     * --|--|--
     * todayGoodsCount |int| 今日新款数量
     * page | object | 分页数据
     * page.totalPage | int | 总页数
     * page.currentPage | int | 当前页码
     * items | array | 商品列表
     * items[].goodsId | int |商品ID
     * items[].goodsName | string |商品名
     * items[].storeName | string |店铺名
     * items[].price | float |商品价格(日常价)
     * items[].couponPrice | int |券后价
     * items[].image | string |商品封面图地址
     * items[].video | string |视频地址
     * items[].isVideo | string |1 视频商品 2 非视频商品
     *
     * @param string $page  第几页
     * @param string $limit 分页大小
     *
     * @return array
     *
     * @returnExample({
     *  "todayGoodsCount":1200,
     *  "items":[
     *      {
     *      "storeName":"测试店铺名",
     *      "goodsId":1234,
     *      "goodsName":"商品标题标题标题",
     *      "price":19.9,
     *      "couponPrice":14.9,
     *      "image":"https://img06.eelly.com",
     *      "video":"https://img06.eelly.com",
     *      "isVideo":"1"
     *      }
     *  ],
     *  "page":{
     *      "totalPage":17,
     *      "currentPage":1
     *  }})
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function goodsListAsync(string $page = '1', string $limit = '12')
    {
        return Client20200618::requestJson('activity/monthCoupon', 'goodsList', ['page' => $page, 'limit' => $limit], false);
    }
    /**
     * 领取优惠券.
     *
     * @param string      $cardId 优惠券ID
     * @param UidDTO|null $uidDTO 需要登录
     *
     * @throws \Throwable
     *
     * @return array
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function getCoupon(string $cardId, UidDTO $uidDTO = null) : array
    {
        return Client20200618::requestJson('activity/monthCoupon', 'getCoupon', ['cardId' => $cardId], true);
    }
    /**
     * 领取优惠券.
     *
     * @param string      $cardId 优惠券ID
     * @param UidDTO|null $uidDTO 需要登录
     *
     * @throws \Throwable
     *
     * @return array
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function getCouponAsync(string $cardId, UidDTO $uidDTO = null)
    {
        return Client20200618::requestJson('activity/monthCoupon', 'getCoupon', ['cardId' => $cardId], false);
    }
    /**
     * 支付成功返回信息.
     *
     * ### 返回字段说明
     *
     * 字段|类型|说明
     * --|--|--
     * platformMoney |int | 平台券金额总额
     * storeMoney |int| 店铺优惠券总额
     * signInNum |int | 补签卡次数
     * freightTimes |int | 运费补贴次数
     *
     * @param UidDTO|null $uidDTO 需要登录
     *
     * @return array
     *
     * @returnExample({
     *  "platformMoney": 50,
     *  "storeMoney": 100,
     *  "signInNum": 7,
     *  "freightTimes": 2
     * })
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function paySuccessInfo(UidDTO $uidDTO = null) : array
    {
        return Client20200618::requestJson('activity/monthCoupon', 'paySuccessInfo', [], true);
    }
    /**
     * 支付成功返回信息.
     *
     * ### 返回字段说明
     *
     * 字段|类型|说明
     * --|--|--
     * platformMoney |int | 平台券金额总额
     * storeMoney |int| 店铺优惠券总额
     * signInNum |int | 补签卡次数
     * freightTimes |int | 运费补贴次数
     *
     * @param UidDTO|null $uidDTO 需要登录
     *
     * @return array
     *
     * @returnExample({
     *  "platformMoney": 50,
     *  "storeMoney": 100,
     *  "signInNum": 7,
     *  "freightTimes": 2
     * })
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function paySuccessInfoAsync(UidDTO $uidDTO = null)
    {
        return Client20200618::requestJson('activity/monthCoupon', 'paySuccessInfo', [], false);
    }
    /**
     * 首页弹窗消息.
     *
     * 如果用户已经开通数据为空对象
     *
     * ### 返回字段说明
     *
     * 字段|类型|说明
     * --|--|--
     * specialPrice | string | 月卡特价文案
     * platformMoney |int | 平台券金额总额
     * storeMoney |int| 店铺优惠券总额
     * signInNum |int | 补签卡次数
     * freightTimes |int | 运费补贴次数
     *
     * @param UidDTO|null $uidDTO 需要登录
     *
     * @return array
     *
     * @returnExample({
     *  "specialPrice":"9.9元得以下特权",
     *  "platformMoney": 50,
     *  "storeMoney": 100,
     *  "signInNum": 7,
     *  "freightTimes": 2
     * })
     *
     * @VZResponse
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function homePopup(UidDTO $uidDTO = null) : array
    {
        return Client20200618::requestJson('activity/monthCoupon', 'homePopup', [], true);
    }
    /**
     * 首页弹窗消息.
     *
     * 如果用户已经开通数据为空对象
     *
     * ### 返回字段说明
     *
     * 字段|类型|说明
     * --|--|--
     * specialPrice | string | 月卡特价文案
     * platformMoney |int | 平台券金额总额
     * storeMoney |int| 店铺优惠券总额
     * signInNum |int | 补签卡次数
     * freightTimes |int | 运费补贴次数
     *
     * @param UidDTO|null $uidDTO 需要登录
     *
     * @return array
     *
     * @returnExample({
     *  "specialPrice":"9.9元得以下特权",
     *  "platformMoney": 50,
     *  "storeMoney": 100,
     *  "signInNum": 7,
     *  "freightTimes": 2
     * })
     *
     * @VZResponse
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function homePopupAsync(UidDTO $uidDTO = null)
    {
        return Client20200618::requestJson('activity/monthCoupon', 'homePopup', [], false);
    }
}