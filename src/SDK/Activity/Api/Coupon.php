<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Activity\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\Activity\Service\CouponInterface;
use Eelly\SDK\Activity\DTO\CouponDTO;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class Coupon
{
    /**
     * 根据优惠券id获取优惠券信息
     *
     * @param array $couponIds 优惠券ids
     *
     * @return array
     * @author wechan
     * @since 2020年4月08日
     */
    public function getCouponByCouponIds(array $couponIds):array
    {
        return EellyClient::requestJson('activity/coupon', __FUNCTION__, ['couponIds' => $couponIds]);
    }

    public static function adminCouponPage(array $condition, int $page = 1, int $limit = 30): array
    {
        return EellyClient::requestJson('activity/coupon', __FUNCTION__, [
            'condition' => $condition,
            'page' => $page,
            'limit' => $limit,
        ]);
    }

    public static function getConponInfoByAcoIds(array $acoIds):array
    {
        return EellyClient::requestJson('activity/coupon', __FUNCTION__, ['acoIds' => $acoIds]);
    }

    public static function getAcoIdsByName(string $name):array
    {
        return EellyClient::requestJson('activity/coupon', __FUNCTION__, ['name' => $name]);
    }

    public static function getGoodsWednesdayCouponList(int $userId):array
    {
        return EellyClient::requestJson('activity/coupon', __FUNCTION__, ['userId' => $userId]);
    }

    public static function getSignInCouponListByUserGrade(int $userGrade, array $days):array
    {
        return EellyClient::requestJson('activity/coupon', __FUNCTION__, [
            'userGrade' => $userGrade,
            'days' => $days
        ]);
    }

    public static function getMinLimitStoreCoupons(array $storeIds):array
    {
        return EellyClient::requestJson('activity/coupon', __FUNCTION__, [
            'storeIds' => $storeIds,
        ]);
    }

    /**
     * 获取省钱月卡活动的优惠券使用情况
     *
     * @param array $acoIds 优惠券ID
     * @param int $startTime 活动开始时间
     * @param int $endTime 活动结束时间
     * @return array
     *
     * @author chenyuhua
     * @since 2020.09.01
     */
    public function getCouponInfoByActivityDuration(array $acoIds, int $startTime, int $endTime): array
    {
        return EellyClient::requestJson('activity/coupon', __FUNCTION__, [
            'acoIds' => $acoIds,
            'startTime' => $startTime,
            'endTime' => $endTime,
        ]);
    }

    /**
     * 获取可用的拉新专享券
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2020.10.14
     */
    public function getPullNewCoupons(): array
    {
        return EellyClient::requestJson('activity/coupon', __FUNCTION__, []);
    }

    /**
     * 获取已使用的平台优惠券信息
     *
     * @param int $userId 买家Id
     *
     * @return array
     *
     * @author chentuying
     * @since 2021-1-13
     */
    public function getUsedCouponInfo(int $userId):array
    {
        return EellyClient::requestJson('activity/coupon', __FUNCTION__, ['userId' => $userId]);
    }

    /**
     * 获取可用的新人专享券
     *
     * @param int $storeId
     * @return array
     *
     * @author chenyuhua
     * @since 2021.05.25
     */
    public function getNewRegisterCoupons(int $storeId = 0): array
    {
        return EellyClient::requestJson('activity/coupon', __FUNCTION__, ['storeId' => $storeId]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
