<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Activity\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\Activity\Service\ActivityInterface;
use Eelly\SDK\Activity\DTO\ActivityDTO;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class ActivityFullReturn
{
    /**
     * 回调发送站内信提醒
     *
     * @param array $order
     * @return bool
     *
     * @author chenyuhua
     * @since 2020.10.23
     */
    public function sendFullOrderReturnMessageByCallBack(array $order): bool
    {
        return EellyClient::requestJson('activity/activityFullReturn', __FUNCTION__, [
            'order' => $order,
        ], true);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}