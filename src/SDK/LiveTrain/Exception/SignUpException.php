<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\LiveTrain\Exception;

use Eelly\Exception\LogicException;

class SignUpException extends LogicException
{
    public const PHONE_ERROR = '手机号格式错误';

    public const NAME_EMPTY = '"称呼" 不能为空';
}
