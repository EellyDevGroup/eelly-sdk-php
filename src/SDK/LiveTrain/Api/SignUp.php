<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\LiveTrain\Api;

use Eelly\SDK\EellyClient;
use Eelly\DTO\UidDTO;

/**
 * 直播培训 报名
 *
 * @author twb<1174865138@qq.com>
 */
class SignUp
{

    /**
     * 直播培训报名页面 表单提交
     *
     * @param string $name 用户名
     * @param string $phone 手机号码
     * @param int    $pageFrom 来源栏目页面,1:我要预约栏目 ; 2:预约报名栏目 ; 3: 领取秘籍栏目 ; 4: 我要入驻栏目; 等等...
     *
     * @return array 结果
     *
     * @author twb<1174865138@qq.com>
     * @since 2021-05-17T14:26:32+0800
     */
    public function addSignUp(string $name, string $phone, int $pageFrom): array
    {
        return EellyClient::requestJson('liveTrain/signUp', __FUNCTION__, ['name' => $name, 'phone' => $phone, 'pageFrom' => $pageFrom]);
    }

    /**
     * 获取后台直播报名信息
     *
     * @param array $conditions 查询条件
     * @param int $page   页码
     * @param int $limit  每页显示数量
     * 
     * @return array
     * 
     * @author twb<1174865138@qq.com>
     * @since 2021-05-12T14:50:57+0800
     *
     */
    public function adminListSignUp(array $conditions, int $page = 1, int $limit = 100): array
    {
        return EellyClient::requestJson('liveTrain/signUp', __FUNCTION__, ['conditions' => $conditions, 'page' => $page, 'limit' => $limit]);
    }


    /**
     * 后台直播报名信息列表 跟进
     *
     * @param array $suIds id数组
     * @param string $userId 操作管理员id
     * @param string $userName 操作管理员name
     * 
     * @return array
     * 
     * @author twb<1174865138@qq.com>
     * @since 2021-05-12T14:50:57+0800
     *
     */
    public function listSetFollow(array $suIds, string $userId, string $userName): array
    {
        return EellyClient::requestJson('liveTrain/signUp', __FUNCTION__, ['suIds' => $suIds, 'userId' => $userId, 'userName' => $userName]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
