<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Exception;

use Eelly\Exception\LogicException;

/**
 * 月卡工具未找到.
 *
 * @author hehui<runphp@dingtalk.com>
 */
class BuyerMonthCardToolNotFound extends LogicException
{
}
