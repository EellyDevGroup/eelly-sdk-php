<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Buyer;

use Eelly\SDK\EellyClient;

/**
 * Class BuyerManage.
 *
 *  modules/Buyer/Service/BuyerManage.php
 *
 * @author zhangyingdi<zhangyingdi@eelly.net>
 */
class BuyerManage
{
    /**
     * 根据传过来的用户id，返回用户设置的标签数据 (管理后台使用)
     *
     * @param array $userIds 用户id数组
     * @return array
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.08.15
     *
     * @internal
     */
    public function listUserTagsByUserIds(array $userIds):array
    {
        return EellyClient::request('eellyOldCode/buyer/buyerManage', __FUNCTION__, true, $userIds);
    }
}
