<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\EellyOldCode\Api;

use Eelly\SDK\EellyClient as Client20191125;
use Eelly\SDK\GetInstanceTrait;

/**
 * This class has been auto-generated by shadon compiler (2019-11-25 03:47:50).
 */
class StoreList
{
    use GetInstanceTrait;
    /**
     * 根据条件获取店铺列表
     *
     * @param array  $condition
     * @param string $fieldScope
     * @param int    $page
     * @param int    $limit
     * @return array
     * @author zhangyangxun
     * @since 2019/11/22
     * @throws
     *
     * @internal
     */
    public static function getStoresByCondition(array $condition, string $fieldScope = 'base', int $page = 0, int $limit = 100) : array
    {
        return Client20191125::requestJson('eellyOldCode/storeList', 'getStoresByCondition', ['condition' => $condition, 'fieldScope' => $fieldScope, 'page' => $page, 'limit' => $limit], true);
    }
    /**
     * 根据条件获取店铺列表
     *
     * @param array  $condition
     * @param string $fieldScope
     * @param int    $page
     * @param int    $limit
     * @return array
     * @author zhangyangxun
     * @since 2019/11/22
     * @throws
     *
     * @internal
     */
    public static function getStoresByConditionAsync(array $condition, string $fieldScope = 'base', int $page = 0, int $limit = 100)
    {
        return Client20191125::requestJson('eellyOldCode/storeList', 'getStoresByCondition', ['condition' => $condition, 'fieldScope' => $fieldScope, 'page' => $page, 'limit' => $limit], false);
    }
}