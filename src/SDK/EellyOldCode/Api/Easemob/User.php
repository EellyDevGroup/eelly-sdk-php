<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Easemob;

use Eelly\SDK\EellyClient;

/**
 * Class User.
 *
 * modules/Easemob/Service/UserService.php
 *
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class User
{
    /**
     * 推送环信+极光接口.
     *
     * @param array  $userIds 用户ID数组
     * @param int    $type    1店+  2厂+
     * @param string $title   推送标题
     * @param string $content 推送内容
     *
     * @author 郑志明<zhengzhiming@eelly.net>
     *
     * @since  2017年4月06日
     */
    public static function manageSendAppInfo($userIds, $type, $title, $content)
    {
        return EellyClient::requestJson('eellyOldCode/easemob/user', __FUNCTION__, [
            'userIds' => $userIds,
            'type'    => $type,
            'title'   => $title,
            'content' => $content,
        ]);
    }

    /**
     * 通知消息列表
     *
     * @param int $type 1 店+  2 厂+
     * @param array $sCon 筛选条件
     * @param int $page 当前页数
     * @param int $limit 限制条数
     *
     * @return array
     *
     * @author chentuying
     * @since 2021-01-22
     */
    public static function infoList(int $type, array $sCon, int $page, int $limit):array
    {
        return EellyClient::requestJson('eellyOldCode/easemob/user', __FUNCTION__, [
            'type' => $type,
            'sCon' => $sCon,
            'page' => $page,
            'limit' => $limit
        ]);
    }
}
