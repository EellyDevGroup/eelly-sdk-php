<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Easemob;

use Eelly\SDK\EellyClient;

class Msg
{
    public static function sendMsgCompatible(array $from, array $to, $msg, array $body = [], $extType = null, array $ext = [])
    {
        return EellyClient::requestJson('eellyOldCode/easemob/msg', __FUNCTION__, [
            'from'    => $from,
            'to'      => $to,
            'msg'     => $msg,
            'body'    => $body,
            'extType' => $extType,
            'ext'     => $ext,
        ]);
    }
}
