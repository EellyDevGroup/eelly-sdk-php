<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Easemob;

use Eelly\SDK\EellyClient;

/**
 * Class Stats.
 *
 * modules/Easemob/Service/StatsService.php
 *
 * @author hehui<runphp@dingtalk.com>
 */
class TinyUrl
{
    /**
     * 获取跳转至店铺首页的二维码链接
     *
     *
     * @param string $qrUrl 未处理的店铺链接
     *
     * @author chenyuhua<chenyuhua@eelly.net>
     *
     * @since  2020年8月12日
     */
    public function getIdByUrl(string $qrUrl = null)
    {
        return EellyClient::request('eellyOldCode/easemob/tinyUrl', __FUNCTION__, true, $qrUrl);
    }
}
