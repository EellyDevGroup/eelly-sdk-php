<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\EellyOldCode\Api;

use Eelly\DTO\UidDTO;
use Eelly\SDK\EellyClient;

class GoodsTiktokUpload
{
    /**
     * 添加商品到抖音待上传商品列表
     *
     * @param int $goodsId
     * @param UidDTO|null $user
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.10
     */
    public function addUploadTiktokGoods(int $goodsId, UidDTO $user = null): array
    {
        return EellyClient::requestJson('eellyOldCode/GoodsTiktokUpload', __FUNCTION__, [
            'goodsId' => $goodsId,
        ]);
    }

    /**
     * 获取抖店商品列表Tab
     *
     * @param UidDTO    $user
     * @return array[]
     *
     * @author chenyuhua
     * @since 2021.03.29
     */
    public function getUploadTiktokGoodsTabs(UidDTO $user = null): array
    {
        return EellyClient::requestJson('eellyOldCode/GoodsTiktokUpload', __FUNCTION__);
    }

    /**
     * 获取抖店等待上传商品列表数据
     *
     * @param array     $params                搜索参数
     * @param string    $params.tabId          tabId
     * @param string    $params.goodsName      商品名称
     * @param string    $params.storeName      商品ID
     * @param string    $params.goodsNumber    商品编码
     * @param string    $params.cateId         商品分类ID
     * @param int       $params.ifShow         上架状态 -1 全部 0 已下架 1 上架中
     * @param string    $params.sortKey        排序字段，不排序此字段值传空字符串 price 销售价格 stock 库存 expireTime 创建时间
     * @param int       $params.asc            0 降序 1 升序
     * @param int       $page                  页数
     * @param int       $limit                 每页行数
     * @param UidDTO    $user
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.02
     */
    public function getUploadTiktokGoodsList(array $params, int $page = 1, int $limit = 20, UidDTO $user = null): array
    {
        return EellyClient::requestJson('eellyOldCode/GoodsTiktokUpload', __FUNCTION__, [
            'params' => $params,
            'page'  => $page,
            'limit' => $limit,
        ]);
    }

    /**
     * 获取抖音上传商品SKU
     *
     * @param int       $goodsId      商品ID
     * @param int       $type         操作类型 0 修改商品编码 1 修改商品价格 2 修改商品库存
     * @param UidDTO    $user
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.09
     */
    public function getUploadTiktokGoodsSku(int $goodsId, int $type, UserDTO $user = null): array
    {
        return EellyClient::requestJson('eellyOldCode/GoodsTiktokUpload', __FUNCTION__, [
            'goodsId' => $goodsId,
            'type'  => $type,
        ]);
    }

    /**
     * 修改抖音上传商品数据(sku相关)
     *
     * @param int       $goodsId        商品ID
     * @param int       $type           操作类型 0 修改商品编码 1 修改商品价格 2 修改商品库存
     * @param array     $code           商品SKU列表
     * @param UidDTO    $user
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.09
     */
    public function updateUploadTiktokGoodsSku(int $goodsId, int $type, array $code, UserDTO $user = null): array
    {
        return EellyClient::requestJson('eellyOldCode/GoodsTiktokUpload', __FUNCTION__, [
            'goodsId' => $goodsId,
            'type'  => $type,
            'code'  => $code,
        ]);
    }

    /**
     * 批量删除抖音上传列表商品
     *
     * @returnExample({"result": 1})
     *
     * @param array $gtuIds
     * @param UidDTO|null $user
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.21
     */
    public function deleteUploadTiktokGoods(array $gtuIds, UidDTO $user = null): array
    {
        return EellyClient::requestJson('eellyOldCode/GoodsTiktokUpload', __FUNCTION__, [
            'gtuIds' => $gtuIds,
        ]);
    }

    /**
     * 获取抖店待上传商品数量
     *
     * @param UidDTO|null $user
     * @return int
     * @throws \Throwable
     *
     * @author chenyuhua
     * @since 2021.04.26
     */
    public function getWaitUploadTiktokGoodsNum(UidDTO $user = null): int
    {
        return EellyClient::requestJson('eellyOldCode/GoodsTiktokUpload', __FUNCTION__);
    }

    /**
     * 批量上传商品接口
     *
     * @param array $gtuIds 商品上传表id
     * @param UidDTO|null $user
     * @return bool
     *
     *
     * @author chenyuhua
     * @since 2021.04.26
     */
    public function batchTiktokGoodsAddV2(array $gtuIds, UidDTO $user = null): array
    {
        return EellyClient::requestJson('eellyOldCode/GoodsTiktokUpload', __FUNCTION__, ['gtuIds' => $gtuIds]);
    }

    /**
     * 是否有资格上传到抖音待上传列表
     *
     * @param array $goodsIds 商品ids
     * @param UidDTO|null $user
     * @return array
     *
     *
     * @author twb<1174865138@qq.com>
     * @since 2021.04.29
     */
    public function checkTiktokUpload(array $goodsIds, UidDTO $user = null): array
    {
        return EellyClient::requestJson('eellyOldCode/GoodsTiktokUpload', __FUNCTION__, ['goodsIds' => $goodsIds]);
    }

}
