<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Common;

use Eelly\DTO\UidDTO;
use Eelly\SDK\EellyClient;

/**
 * Class Member.
 *
 *  modules/Common/Service/MemberService.php
 *
 * @author zhangyingdi<zhangyingdi@eelly.net>
 */
class Member
{
    /**
     * 批量获取用户管理数据
     *
     * @param array $userIds
     * @return array
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.08.22
     */
    public function listMemberManage(array $userIds):array
    {
        return EellyClient::request('eellyOldCode/common/member', __FUNCTION__, true, $userIds);
    }

    /**
     * 判断商品，档口、市场是否已关注 (批发圈旧接口迁移)
     *
     * @param int $type   1：商品 , 2:档口 , 3:市场
     * @param int $itemId 商品或档口或市场id
     * @param UidDTO|null $user
     * @return string
     *
     * @author chenzhong
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.09.26
     */
    public function checkFollow( int $type, int $itemId, UidDTO $user = null):string
    {
        return EellyClient::request('eellyOldCode/common/member', __FUNCTION__, true, $type, $itemId, $user);
    }
}
