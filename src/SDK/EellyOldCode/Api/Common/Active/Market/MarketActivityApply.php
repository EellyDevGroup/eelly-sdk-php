<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Common\Active\Market;

use Eelly\SDK\EellyClient;

/**
 * Class MarketActivityApply.
 *
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class MarketActivityApply
{
    /**
     * 通过活动id获取活动报名的店铺
     *
     * @param integer $actId 活动id
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.8.23
     */
    public function getStoreForActId(int $actId)
    {
        return EellyClient::request('eellyOldCode/common/active/market/marketActivityApply', __FUNCTION__, true, $actId);

    }

    /**
     * 小程序0元拿样活动 一个店铺只有一款
     *
     * @param integer $actId 活动id
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.9.17
     */
    public function getAppletActivity(int $actId)
    {
        return EellyClient::request('eellyOldCode/common/active/market/marketActivityApply', __FUNCTION__, true, $actId);
    }

    /**
     * 超多人购买的店
     * 
     * @param integer $actIdForStore 店铺活动id
     * @param integer $actIdForGoods 商品活动id
     * @param integer $storeId 店铺id
     * @param integer $page 页码
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.9.16
     */
    public function manyPeopleBuyStore(int $actIdForStore, int $actIdForGoods, int $storeId = 0, int $page = 1)
    {
        return EellyClient::request('eellyOldCode/common/active/market/marketActivityApply', __FUNCTION__, true, $actIdForStore, $actIdForGoods, $storeId, $page);
    }

    /**
     * 判断店铺是否在某个活动里面
     *
     * @param integer $storeId 店铺id
     * @param integer $type 类型 1:好货星期三
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.9.18
     */
    public function checkStoreIsActivity(int $storeId, int $type)
    {
        return EellyClient::request('eellyOldCode/common/active/market/marketActivityApply', __FUNCTION__, true, $storeId, $type);
    }

    /**
     * 批量判断店铺是否在某个活动里面
     *
     * @param array $storeIds 店铺id数组
     * @param int $type 类型 1:好货星期三 2:营销活动id
     * @return array
     *
     * @author zhangyingdi <zhangyingdi@eelly.net>
     * @since 2019.10.22
     */
    public function listCheckStoreIsActivity(array $storeIds, int $type):array
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['storeIds' => $storeIds, 'type' => $type]
        );
    }

    /**
     * 通过活动id，判断店铺是否参加且审核通过
     *
     * @param int $storeId  店铺id
     * @param int $actId  活动id
     * @return bool
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.10.23
     */
    public function checkStoreByActivityId(int $storeId, int $actId):bool
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['storeId' => $storeId, 'actId' => $actId]
        );
    }

    /**
     * 2019双11活动攻略
     *
     * @param $actId
     * @return bool|\GuzzleHttp\Promise\PromiseInterface|int|mixed|null|string
     * @throws \ErrorException
     * @author zhangyangxun
     * @since 2019/10/24
     */
    public function double11Activity($actId)
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId]
        );
    }

    /**
     * 聚能活动会场数据
     *
     * @param int $actId  活动id
     * @param int $page   页码
     * @param int $limit  每页显示数量
     * @return array
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.10.24
     */
    public function listGatheringVenue(int $actId, int $page = 1, int $limit = 20):array
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId, 'page' => $page, 'limit' => $limit]
        );
    }

    /**
     * 获取报名店铺的直播数据
     *
     * @param int $actId  活动id
     * @return array
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.10.24
     */
    public function listStoreLiveInfo(int $actId):array
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId]
        );
    }

    /**
     * 双11领券优惠活动
     *
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.10.28
     */
    public function doubleElevenCoupon(int $actId):array
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId]
        );
    }

    /**
     * 双十一精选人气榜单
     *
     * @param array $actIds 活动id数组
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.10.28
     */
    public function doubleElevenPopularityStore(array $actIds):array
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actIds' => $actIds]
        );
    }

    /**
     * 双十一原创设计
     *
     * @param integer $actId 活动id
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.10.28
     */
    public function doubleElevenOriginalDesign(int $actId):array
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId]
        );
    }

    /**
     * 双十一人气榜单列表
     *
     * @param integer $actId 活动id
     * @param integer $liveActId 直播活动id
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.10.29
     */
    public function doubleElevenPopularityList(int $actId, int $liveActId):array
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId, 'liveActId' => $liveActId]
        );
    }

    /**
     * 通过活动id，判断店铺是否参加且审核通过且是在活动有效期内
     *
     * @param int $storeId  店铺id
     * @param array $actIds  活动ids
     * @return bool
     *
     * @author wechan
     * @since 2019年11月19日
     */
    public function checkValidStoreByActivityIds(int $storeId, array $actIds):bool
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['storeId' => $storeId, 'actIds' => $actIds]
        );
    }

    /**
     * 新人大礼包活动模块2
     * 需求：http://zentao.eelly.test/index.php?m=story&f=view&storyID=3050
     *
     * @param integer $actId 活动id
     * @param integer $page 页码
     * @param integer $limit 条数
     * @return array
     */
    public function newMemberPackageModel2(int $actId, int $page = 1, int $limit =10)
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId, 'page' => $page, 'limit' => $limit]
        );
    }

    /**
     * 获取进货榜单
     *
     * @param int $userId 登录用户的userId
     * @param int $type 类型 1.实时指数（今天） 2.昨天指数（昨天）
     *
     * @return array
     *
     * @author wechan
     * @since 2019年12月09日
     */
    public function purchaseRank(int $userId, int $type = 1)
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['userId' => $userId, 'type' => $type]
        );
    }

    /**
     * 双12看视频模块
     * 需求：http://zentao.eelly.test/index.php?m=story&f=view&storyID=3053
     *
     * @param integer $actId 活动id
     * @param integer $limit 条数
     * @return array
     *
     * @author wechan
     * @since 2019年12月09日
     */
    public function doubleTwelveLookVideo(int $actId, int $limit =10)
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId, 'limit' => $limit]
        );
    }

    /**
     * 双12看直播模块
     * 需求：http://zentao.eelly.test/index.php?m=story&f=view&storyID=3053
     *
     * @param integer $actId 活动id
     * @param integer $limit 条数
     * @return array
     *
     * @MyCache(lifetime=60)
     *
     * @author wechan
     * @since 2019年12月09日
     */
    public function doubleTwelveLookLive(int $actId, int $limit =10)
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId, 'limit' => $limit]
        );
    }

    /**
     * 51 活动
     * 需求：http://zentao.eelly.test/index.php?m=story&f=view&storyID=3092
     *
     * @param integer $actId 活动id
     * @param integer $limit 条数
     * @return array
     *
     *
     * @author wechan
     * @since 2019年12月09日
     */
    public function getActivity51(int $actId, int $limit =100)
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId, 'limit' => $limit]
        );
    }

    /**
     * 618活动发现更多好券
     *
     * @param integer $actId 活动id
     * @param integer $type 类型：1.正在直播 2.大额神券
     * @return array
     *
     *
     * @author wechan
     * @since 2019年12月09日
     */
    public function getCouponLive618(int $actId, int $type = 1)
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId, 'type' => $type]
        );
    }

    public function get99PartyStoreMode(int $actId)
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId]
        );
    }

    public function get99PartyGoodsMode(int $actId)
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId]
        );
    }

    /**
     * 营销活动--店铺模块
     *
     * @param  int  $actId  营销活动id
     * @return array
     *
     * @author chentuying
     * @since 2020/9/2
     *
     */
    public function getActivityStoreMode(int $actId)
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId]
        );
    }

    /**
     * 备货节--超值单品
     *
     * @param int $actId 营销活动id
     * @return array
     *
     * @author chentuying
     * @since 2020-09-18
     */
    public function getEighthStockDayGoods(int $actId)
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId]
        );
    }

    /**
     * 备货节--严选好店
     *
     * @param int $actId 营销活动id
     * @return array
     *
     * @author chentuying
     * @since 2020-09-18
     */
    public function getEighthStockDayStore(int $actId):array
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId]
        );
    }

    /**
     * 备货节--直播
     *
     * @param int $actId 营销活动id
     * @param int $uid  当前登录用户id
     * @return array
     *
     * @author chentuying
     * @since 2020-09-19
     */
    public function getEighthStockDayLiveStore(int $actId, int $uid):array
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId, 'uid' => $uid]
        );
    }

    /**
     * 备货节--时髦好店
     *
     * @param int $actId  营销活动id
     * @return array
     *
     * @author chentuying
     * @since 2020-09-21
     */
    public function getEighthStockDayFashionStore(int $actId):array
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId]
        );
    }

    /**
     * 备货节--店铺商品
     *
     * @param int $actId 营销活动id
     * @return array
     *
     * @author chentuying
     * @since 2020-09-21
     */
    public function getEighthStockDayStoreGoods(int $actId, $limit = 10):array
    {
        return EellyClient::requestJson(
            'eellyOldCode/common/active/market/marketActivityApply',
            __FUNCTION__,
            ['actId' => $actId]
        );
    }
}
