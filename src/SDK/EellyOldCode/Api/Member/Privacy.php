<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Member;

use Eelly\SDK\EellyClient;

/**
 * Class Member.
 *
 *  modules/Member/Service/PrivacyService.php
 *
 * @author chentuying
 */
class Privacy
{
    /**
     * 店+4.5.2 获取商圈模型.
     *
     * @return array
     *
     * @author chentuying
     * @since 2020-12-07
     */
    public static function getDistrictArr()
    {
        return EellyClient::requestJson('eellyOldCode/member/profile/privacy', __FUNCTION__);
    }

    /**
     * 获取官方推荐的所有的商圈信息.
     *
     * @author chentuying
     * @since 2020-12-07
     */
    public static function getStoreDistrict()
    {
        return EellyClient::requestJson('eellyOldCode/member/profile/privacy', __FUNCTION__);
    }
}
