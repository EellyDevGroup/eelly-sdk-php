<?php
namespace Eelly\SDK\EellyOldCode\Api\Active\Coupon;

use Eelly\SDK\EellyClient;

class Coupon
{
    public static function getRedPackageByUid(int $uid):array
    {
        return EellyClient::requestJson('eellyOldCode/active/coupon/coupon', __FUNCTION__, ['uid' => $uid]);
    }

    public static function getCouponSummaryData(int $userId = 0): array
    {
        return EellyClient::requestJson('eellyOldCode/active/coupon/coupon', __FUNCTION__, ['userId' => $userId]);
    }

    public static function receiveCoupon(string $keycode, int $userId): bool
    {
        return EellyClient::requestJson('eellyOldCode/active/coupon/coupon', __FUNCTION__, [
            'keycode' => $keycode,
            'userId' => $userId
        ]);
    }

    public static function sendLiveRoomCoupon(int $liveId, int $storeId, int $userId, int $type): array
    {
        return EellyClient::requestJson('eellyOldCode/active/coupon/coupon', __FUNCTION__, [
            'liveId' => $liveId,
            'storeId' => $storeId,
            'userId' => $userId,
            'type' => $type
        ]);
    }
}
