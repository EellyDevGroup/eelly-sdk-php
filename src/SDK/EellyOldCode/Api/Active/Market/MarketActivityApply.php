<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Active\Market;

use Eelly\SDK\EellyClient;

/**
 * Class MarketActivityApplyService.
 *
 */
class MarketActivityApply
{
    /**
     * 校验活动商品是否已经上线
     *
     * @param array $activityIds 活动id
     * @param array $goodsIds 商品id
     *
     * @author wechan
     * @since 2019年09月17日
     */
    public function checkOnlineGoodsApplyActivity($activityIds ,$goodsIds)
    {
        return EellyClient::requestJson(
            'eellyOldCode/Active/Market/MarketActivityApply',
            __FUNCTION__,
            ['activityIds' => $activityIds, 'goodsIds' => $goodsIds]
        );
    }

    /**
     * 获取拼团商品数据.
     *
     * @param int   $page         分页
     * @param int   $limit        每页条数
     *
     * @return array
     *
     * @author wechan
     * @since 2020年02月21日
     */
    public function getAppletActivityLikeGoods(int $page = 1, int $limit = 10): array
    {
        return EellyClient::requestJson(
            'eellyOldCode/Active/Market/MarketActivityApply',
            __FUNCTION__,
            ['page' => $page, 'limit' => $limit]
        );
    }

    /**
     * 分别获取直播中的直播间和有设置分销佣金的店铺
     *
     * @return array
     *
     * @author wechan
     * @since 2020年02月21日
     */
    public function getSpreadLiveAndStoreGoods():array
    {
        return EellyClient::requestJson(
            'eellyOldCode/Active/Market/MarketActivityApply',
            __FUNCTION__,
            []
        );
    }

    /**
     * 定时更新预约新人活动
     *
     *
     * @author wechan
     * @since 2020年03月20日
     * @return bool
     */
    public function updateNewrecruitsExclusive():bool
    {
        return EellyClient::requestJson(
            'eellyOldCode/Active/Market/MarketActivityApply',
            __FUNCTION__,
            []
        );
    }

    /**
     * 根据商品ids获取最低活动价格
     *
     * @since 2020年4月23日
     */
    public function getActPriceByGoodsIds(array $goodsIds)
    {
        return EellyClient::requestJson(
            'eellyOldCode/Active/Market/MarketActivityApply',
            __FUNCTION__,
            ['goodsIds' => $goodsIds]
        );
    }

}
