<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\BusinessCircle;

use Eelly\SDK\EellyClient;

/**
 * Class DynamicRecord.
 *
 *  modules/BusinessCircle/Service/DynamicRecordService.php
 *
 * @author zhangyingdi<zhangyingdi@eelly.net>
 */
class DynamicRecord
{
    /**
     * @inheritDoc
     */
    public function getDynamicInfoByFmId($itemIds, $userId = 0)
    {
        return EellyClient::requestJson('eellyOldCode/businessCircle/dynamicRecord', __FUNCTION__, [
            'itemIds' => $itemIds, 'userId' => $userId
        ]);
    }

    public static function getTimeBusinessCircleCount($fmType = [1, 2, 3], $days = 30)
    {
        return EellyClient::requestJson('eellyOldCode/businessCircle/dynamicRecord', __FUNCTION__, [
            'fmType' => $fmType, 'days' => $days
        ]);
    }
}
