<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\BusinessCircle;

use Eelly\SDK\EellyClient;

/**
 * Class BusinessCircle.
 *
 *  modules/BusinessCircle/Service/BusinessCircleService.php
 *
 * @author hehui<hehui@eelly.net>
 */
class BusinessCircle
{
    /**
     * @param array $param
     *
     * @return mixed
     */
    public function factoryBusinessCircle2(array $param)
    {
        return EellyClient::requestJson('eellyOldCode/businessCircle/businessCircle', __FUNCTION__, ['param' => $param]);
    }

    /**
     * 获取动态信息
     *
     * @param array $fmIds 动态id
     *
     * @return array
     *
     */
    public function getDynamicByFmIds(array $fmIds)
    {
        return EellyClient::requestJson('eellyOldCode/businessCircle/businessCircle', __FUNCTION__, ['fmIds' => $fmIds]);
    }

    /**
     * @inheritDoc
     */
    public function updateDynamicVideoId(string $fmId, int $videoId = 0):bool
    {
        return EellyClient::requestJson('eellyOldCode/businessCircle/businessCircle', __FUNCTION__, [
            'fmId' => $fmId, 'videoId' => $videoId
        ]);
    }

    public static function monthCardSeenGoods(int $userId, int $page, int $limit): array
    {
        return EellyClient::requestJson('eellyOldCode/businessCircle/businessCircle', __FUNCTION__, [
            'userId' => $userId, 'page' => $page, 'limit' => $limit
        ]);
    }

    /**
     * 统计店+用户喜欢的货的数量
     *
     * @param int $userId 用户id
     * @return int
     *
     * @author chenyuhua
     * @since 2021.01.26
     */
    public function getGoodsLikeCount(int $userId):int
    {
        return EellyClient::requestJson('eellyOldCode/businessCircle/businessCircle', __FUNCTION__, [
            'userId' => $userId,
        ]);
    }

    /**
     * 获取看过的款统计数
     *
     * @param int $userId 用户id
     * @return int
     *
     * @author chenyuhua
     * @since 2021.01.26
     */
    public function getFootprintLookCount(int $userId): int
    {
        return EellyClient::requestJson('eellyOldCode/businessCircle/businessCircle', __FUNCTION__, [
            'userId' => $userId,
        ]);
    }
}
