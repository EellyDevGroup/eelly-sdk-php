<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api;

use Eelly\SDK\EellyClient;
use Eelly\DTO\UidDTO;

class GoodsAgent
{
    /**
     * 获取店铺正在带货商品数
     *
     * @param array $storeIds
     * @return array
     *
     * @author chenyuhua
     * @since 2021.03.05
     */
    public function getAnchorAgentGoodsCount(array $storeIds): array
    {
        return EellyClient::requestJson('eellyOldCode/goodsAgent', __FUNCTION__, ['storeIds' => $storeIds]);
    }

    /**
     * 获取店铺推荐（带货中）商品
     *
     * @param int $storeId
     * @param int $page
     * @param int $limit
     * @return array
     *
     * @author chenyuhua
     * @since 2021.03.04
     */
    public function getAnchorRecommendGoods(int $storeId, int $page = 1, int $limit = 20): array
    {
        return EellyClient::requestJson('eellyOldCode/goodsAgent', __FUNCTION__, [
            'storeId' => $storeId,
            'page' => $page,
            'limit' => $limit,
        ]);
    }

    /**
     * 获取直播带货tab
     *
     * ### 返回参数说明
     *
     * 参数名  |类型 | 说明
     * -----|------------------|---------
     * cateId | int  | 分类ID
     * text | string  | 分类名
     *
     * @returnExample([{"cateId": "289","text": "女装"},{"cateId": "292","text": "男装"},{"cateId": "293","text": "童装"},{"cateId": "343","text": "箱包"},{"cateId": "344","text": "内衣"},{"cateId": "293059","text": "美妆"}])
     *
     * @param UidDTO|null $user
     * @return array
     *
     * @author twb<1174865138@qq.com>
     * @since 2021-05-17T17:20:05+0800
     */
    public function getGoodsAgentTab(UidDTO $user = null): array
    {
        return EellyClient::requestJson('eellyOldCode/goodsAgent', __FUNCTION__, []);
    }

    /**
     * 获取直播带货tab
     *
     * ### 返回参数说明
     *
     * 参数名  |类型 | 说明
     * -----|------------------|---------
     * cateId | int  | 分类ID
     * text | string  | 分类名
     *
     * @returnExample([{"cateId": "289","text": "女装"},{"cateId": "292","text": "男装"},{"cateId": "293","text": "童装"},{"cateId": "343","text": "箱包"},{"cateId": "344","text": "内衣"},{"cateId": "293059","text": "美妆"}])
     *
     * @param UidDTO|null $user
     * @return array
     *
     * @author twb<1174865138@qq.com>
     * @since 2021-05-17T17:20:05+0800
     */
    public function getGoodsAgentTabRandom(UidDTO $user = null): array
    {
        return EellyClient::requestJson('eellyOldCode/goodsAgent', __FUNCTION__, []);
    }


    /**
     * 获取直播带货商品列表
     *
     *
     *
     * ### 返回参数说明
     *
     * 参数名  |类型 | 说明
     * -----|------------------|---------
     * list | array  | 商品列表
     * list.[].goodsId | int  | 商品ID
     * list.[].goodsName | string  | 商品名
     * list.[].storeName | string  | 店铺名
     * list.[].goodsImage | string  | 商品图片
     * list.[].goodsNumber | string  | 货号
     * list.[].price | string  | 商品价格
     * list.[].stock | int  | 商品库存
     * list.[].isAdd | int  | 是否已添加为店铺正在带货商品 0 否 1 是
     * text | string  | 分类名
     *
     * @returnExample({"list": [{"goodsId": 18987848,"goodsName": "刘姥姥初进大观园","storeName": "官方活动店","goodsImage":"https://img07.eelly.com/store2555370/goods/20201130/8256577076061.jpg?t=1606707756","goodsNumber":"货号：10086","price": "¥59.9","stock": "1024","isAdd": "1"},{"goodsId": 18987849,"goodsName": "刘姥姥倒拔垂杨柳","storeName": "官方活动店","goodsImage":"https://img07.eelly.com/store2555370/goods/20201130/8256577076061.jpg?t=1606707756","goodsNumber":"货号：10086","price": "¥59.9","stock": "1024","isAdd": "1", "sale": "234", "addTime": "1550790574"}],"page": {"totalPage": 5,"currentPage": 1,"limit": 20}})
     *
     * @param int         $cateId     商品分类ID; 289: 女装; 292: 男装; 293: 童装; 343: 箱包; 344: 内衣; 293059: 美妆; 342: 鞋类; 0: 其他; -1: 随机,不要求;
     * @param int         $liveId     直播ID
     * @param string      $keyword    关键词
     * @param string      $order       排序; "price": 按价格; "sale": 按销量; "time": 时间; "stock":库存
     * @param int         $sort       顺序; 0:正序, 1:反序
     * @param int         $page       页数
     * @param int         $userId       userId,默认自动获取
     * @return array[]
     * @throws \Throwable
     *
     * @author chenyuhua
     * @since 2020.12.17
     * @IosResponse
     */
    public function searchGoodsAgentListRandom(int $cateId = -1, int $liveId = 0, string $keyword = '', string $order = '', int $sort = 0, int $page = 1, int $userId = 0): array
    {
        return EellyClient::requestJson('eellyOldCode/goodsAgent', __FUNCTION__, [
            'cateId' => $cateId,
            'liveId' => $liveId,
            'keyword' => $keyword,
            'order' => $order,
            'sort' => $sort,
            'page' => $page,
            'userId' => $userId,
        ]);
    }
}
