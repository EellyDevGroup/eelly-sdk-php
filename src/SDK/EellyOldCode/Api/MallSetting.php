<?php

declare (strict_types=1);
namespace Eelly\SDK\EellyOldCode\Api;

use Eelly\SDK\EellyClient as Client20200229;
use Eelly\SDK\GetInstanceTrait;

/**
 * This class has been auto-generated by shadon compiler (2020-02-29 01:48:58).
 */
class MallSetting
{
    use GetInstanceTrait;
    /**
     * 获取商城设置信息.
     *
     * @param string $remark
     * @return string
     */
    public static function getContentByRemark(string $remark) : string
    {
        return Client20200229::requestJson('eellyOldCode/mallSetting', 'getContentByRemark', ['remark' => $remark], true);
    }
    /**
     * 获取商城设置信息.
     *
     * @param string $remark
     * @return string
     */
    public static function getContentByRemarkAsync(string $remark)
    {
        return Client20200229::requestJson('eellyOldCode/mallSetting', 'getContentByRemark', ['remark' => $remark], false);
    }
}