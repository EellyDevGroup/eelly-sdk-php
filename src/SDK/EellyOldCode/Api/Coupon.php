<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\EellyOldCode\Api;

use Eelly\SDK\EellyClient as Client20191213;
use Eelly\SDK\GetInstanceTrait;

/**
 * class Coupon
 */
class Coupon
{
    use GetInstanceTrait;
    
    /**
     * 统计用户优惠券数量
     *
     * @param integer $userId 用户id
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.12.13
     */
    public function count(int $userId):array
    {
        return Client20191213::requestJson('eellyOldCode/coupon', 'count', ['userId' => $userId], true);
    }
}