<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\EellyOldCode\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\GetInstanceTrait;

/**
 * class StoreCategory.
 * 
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class StoreCategory
{
    use GetInstanceTrait;

    public static function getNowGcategory() : array
    {
        return EellyClient::requestJson('eellyOldCode/storeCategory', 'getNowGcategory', [], true);
    }

    public static function getAllStoreCategory(): array
    {
        return EellyClient::requestJson('eellyOldCode/storeCategory', 'getAllStoreCategory', [], true);
    }

    public static function getStoreIdsByCateId(int $cateId) : array
    {
        return EellyClient::requestJson('eellyOldCode/storeCategory', 'getStoreIdsByCateId', ['cateId' => $cateId], true);
    }
}