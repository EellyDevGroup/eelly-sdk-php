<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api;

use Eelly\SDK\EellyClient;

/**
 * Class StoreState.
 *
 * @author chenyuhua
 */
class StoreStat
{
    /**
     * 获取店铺新增数量
     *
     * @param integer $duration
     * @param integer $startTime
     * @param integer $endTime
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.28
     */
    public function getStoreIncreNum(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return EellyClient::requestJson('eellyOldCode/storeStat', __FUNCTION__, [
            'duration' => $duration,
            'startTime' => $startTime,
            'endTime' => $endTime,
        ]);
    }

    /**
     * 平台店铺总量
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.29
     */
    public function getStoreTotalNum(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return EellyClient::requestJson('eellyOldCode/storeStat', __FUNCTION__, [
            'duration' => $duration,
            'startTime' => $startTime,
            'endTime' => $endTime,
        ]);
    }

    /**
     * 店铺状态占比
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.30
     */
    public function getStoreStateProportion(): array
    {
        return EellyClient::requestJson('eellyOldCode/storeStat', __FUNCTION__, []);
    }
}
