<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Goods;

use Eelly\SDK\EellyClient;

class GoodsVideo
{
    /**
     * @inheritdoc
     */
    public static function addVideoData($param)
    {
        return EellyClient::requestJson('eellyOldCode/goods/goodsVideo', __FUNCTION__,
            ['param' => $param]
        );
    }

    /**
     * 获取视频在mysql信息.
     *
     *
     * @param array  $goodIds
     * @param string $fields
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2021.03.11
     */
    public function getDataByGoodsId(array $goodIds, $fields = 'getAllInfo')
    {
        return EellyClient::requestJson('eellyOldCode/goods/goodsVideo', __FUNCTION__,
            ['goodIds' => $goodIds, 'fields' => $fields]
        );
    }

    /**
     * 直播商品录播视频同步修改视频商品数据
     *
     * @param array $param
     * @return bool
     *
     * @author chenyuhua
     * @since 2021.05.11
     */
    public function editVideoDataForLiveGoods(array $param): bool
    {
        return EellyClient::requestJson('eellyOldCode/goods/goodsVideo', __FUNCTION__, ['param' => $param]);
    }
}
