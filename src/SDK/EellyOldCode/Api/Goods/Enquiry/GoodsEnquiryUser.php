<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Goods\Enquiry;

use Eelly\DTO\UidDTO;
use Eelly\SDK\EellyClient;

/**
 * Class GoodsEnquiryUser.
 *
 *  modules/Goods/Service/Enquiry/GoodsEnquiryUserService.php
 *
 * @author zhangyingdi<zhangyingdi@eelly.net>
 */
class GoodsEnquiryUser
{
    /**
     * @inheritdoc
     */
    public function addUserEnquiryListData(array $data):bool
    {
        return EellyClient::requestJson('eellyOldCode/goods/enquiry/goodsEnquiryUser', __FUNCTION__, ['data' => $data]);
    }

    /**
     * @inheritdoc
     */
    public function ifStoreSetTpl(UidDTO $user = null):bool
    {
        return EellyClient::requestJson('eellyOldCode/goods/enquiry/goodsEnquiryUser', __FUNCTION__, ['user' => $user]);
    }
}
