<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Goods\Enquiry;

use Eelly\SDK\EellyClient;

/**
 * Class GoodsEnquiryMessage.
 *
 *  modules/Goods/Service/Enquiry/GoodsEnquiryMessageService.php
 *
 * @author zhangyingdi<zhangyingdi@eelly.net>
 */
class GoodsEnquiryMessage
{
    /**
     * 档口报价
     *
     * @param array  $geIds 询价商品id
     * @param int $fromUserId 发送者id
     * @param int $sendUserId 接收者id
     *
     * @author wechan<liweiquan@eelly.net>
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.11.26
     */
    public function sendOffPriceMessage($geIds, $fromUserId, $sendUserId)
    {
        return EellyClient::requestJson('eellyOldCode/goods/enquiry/goodsEnquiryMessage', __FUNCTION__,
            ['geIds' => $geIds, 'fromUserId' => $fromUserId, 'sendUserId' => $sendUserId]
        );
    }
}
