<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Goods;

use Eelly\SDK\EellyClient;

/**
 * Class Category.
 *
 * modules/Goods/Service/CategoryService.php
 *
 * @author chenyuhua
 */
class Category
{
    /**
     * 获取商品所有一级分类
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.21
     */
    public function getParentCategoryList():array
    {
        return EellyClient::requestJson('eellyOldCode/goods/category', __FUNCTION__);
    }

    /**
     * 根据父级类目获取子类目.
     *
     * @param int $parentId  父级ID
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.21
     */
    public function getChildCategoryData(int $parentId): array
    {
        return EellyClient::requestJson('eellyOldCode/goods/category', __FUNCTION__, ['parentId' => $parentId]);
    }

    /**
     * 通过分类ID获取商品分类表某一字段.
     *
     * @param array  $cateIds 分类ID数组
     * @param string $field   某一字段名
     *
     * @author chenyuhua
     * @since  2020.08.27
     */
    public function getFieldByCateIds(array $cateIds,string $field)
    {
        return EellyClient::requestJson('eellyOldCode/goods/category', __FUNCTION__, ['cateIds' => $cateIds, 'field' => $field]);
    }
}
