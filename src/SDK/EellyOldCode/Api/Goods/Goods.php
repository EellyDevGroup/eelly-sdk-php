<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Goods;

use Eelly\SDK\EellyClient;

/**
 * Class Goods.
 *
 *  modules/Goods/Service/GoodsService.php
 *
 * @author hehui<hehui@eelly.net>
 */
class Goods
{
    /**
     * @param $goodsId
     * @param int $userId
     *
     * @return mixed
     */
    public function getWapGoodsInfo($goodsId, $userId = 0)
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, ['goodsId' => $goodsId, 'userId' => $userId]);
    }

    /**
     * @param $goodsId
     * @param $userId
     * @param int $limit
     *
     * @return mixed
     */
    public function getOtherGoods($goodsId, $userId, $limit = 100)
    {
        return EellyClient::request('eellyOldCode/goods/goods', __FUNCTION__, true, $goodsId, $userId, $limit);
    }

    /**
     * @param $userId
     * @param array $goodsIds
     *
     * @return mixed
     */
    public function goodsIndexPermissionNew($userId, array $goodsIds)
    {
        return EellyClient::request('eellyOldCode/goods/goods', __FUNCTION__, true, $userId, $goodsIds);
    }

    /**
     * @param $searchParams
     * @param string $type
     *
     * @return mixed
     */
    public function buyerSearchGoods($searchParams, $type = 'app')
    {
        return EellyClient::request('eellyOldCode/goods/goods', __FUNCTION__, true, $searchParams, $type);
    }

    /**
     * 获取商品数据.
     *
     * @param array $where 条件
     * @param int   $page  页码
     * @param int   $limit 条数
     *
     * @return array
     *
     * @author 肖俊明<xiaojunming@eelly.net>
     *
     * @since 2018年06月13日
     */
    public function getGoodsData(array $where, int $page = 1, int $limit = 10): array
    {
        return EellyClient::request('eellyOldCode/goods/goods', __FUNCTION__, true, $where, $page, $limit);
    }

    /**
     * 小程序零售化 获取推荐商品.
     *
     * @param array $goodsIds 商品ID
     * @param int   $page     页码
     * @param int   $limit    条数
     *
     * @return array
     *
     * @author 肖俊明<xiaojunming@eelly.net>
     *
     * @since  2018年08月24日
     */
    public function getRememberGoodsData(array $goodsIds, int $page = 1, int $limit = 10): array
    {
        return EellyClient::request('eellyOldCode/goods/goods', __FUNCTION__, true, $goodsIds, $page, $limit);
    }

    /**
     * 根据$goodsIds 获取商品信息.
     *
     * @param array  $goodsIds 商品id
     * @param int    $userId   用户id
     * @param string $type     类型
     *
     * @return array $goodsInfo
     *
     * @since 2015年6月8日
     */
    public function getGoodsInfo(array $goodsIds, int $userId = 0, string $type = 'mall')
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, ['goodsIds' => $goodsIds, 'userId' => $userId, 'type' => $type]);
    }

    /**
     * 获取商品信息.
     *
     * @param array  $priceInfo 商品的价格信息
     * @param number $specId    规格id
     * @param number $quantity  购买量
     *
     * @return number
     *
     * @author  何砚文<heyanwen@eelly.net>
     *
     * @since   2015-6-10
     */
    public static function getGoodsPrice($priceInfo, $specId, $quantity)
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, ['priceInfo' => $priceInfo, 'specId' => $specId, 'quantity' => $quantity]);
    }

    /**
     * 根据商品id，获取分享赚钱海报需要的数据
     *
     * @param array $goodsIds  商品id
     * @return array
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2019.08.02
     */
    public function getGoodsPosterInfo(array $goodsIds):array
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, ['goodsIds' => $goodsIds]);
    }

    /**
     * @inheritdoc
     */
    public function getFiledScope(array $goodsIds, $filedScope, $index = '', $show = false)
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__,
            ['goodsIds' => $goodsIds, 'filedScope' => $filedScope, 'index' => $index, 'show' => $show]
        );
    }

    /**
     * @inheritdoc
     */
    public function listEnquirySpecData(int $type = 1):array
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__,
            ['type' => $type]
        );
    }

    /**
     * @inheritdoc
     */
    public function saveFactoryGoodsData($data)
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__,
            ['data' => $data]
        );
    }

    /**
     * @inheritdoc
     */
    public function adminPublishVideoGoods($data)
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__,
            ['data' => $data]
        );
    }

    /**
     * 统计店铺商品数量.
     *
     * ###使用示例
     * ####一般使用方式
     * <code>
     * GoodsService::getInstance()->getGoodsCount(array('store_id'=>123), 1, 0);
     * //或者
     * GoodsService::getInstance()->getGoodsCount(['in',[123]], 1, 0);
     * </code>
     *
     * @param string|array $storeId      店铺ID
     * @param int|array    $ifShow       商品状态，0 下架 1 上架 2 自动下架 3 等待上架 4 自动上架 5 卖家已删除
     * @param int          $cateId       分类1
     * @param string       $keyword      关键字
     * @param array        $lastShowTime 上架时间
     * @service
     * > 数据说明
     *   key | value
     *   --------------------|--------------------
     *   status              |    状态码:200
     *   info                |    提示信息
     *                       |    200: 成功
     *   retval              |    $retval
     *
     * @return array
     *
     * > $retval 数组说明
     *   key | value
     *   --------------------|--------------------
     *   goods_count         |    int       数量
     */
    public function getGoodsCount($storeId, $ifShow, $cateId = 0, $keyword = '', $lastShowTime = ['egt', 0])
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__,
            [
                'storeId' => $storeId,
                'ifShow' => $ifShow,
                'cateId' => $cateId,
                'keyword' => $keyword,
                'lastShowTime' => $lastShowTime
            ]
        );
    }

    /**
     * app新的商品详情页接口.
     *
     * @param int $goodsId 商品id
     * @param int $userId  用户id
     */
    public function appGetGoodsDetail($goodsId, $userId = 0)
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__,
            [
                'goodsId' => $goodsId,
                'userId' => $userId,
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getShortVideoGoodsInfo(array $goodsIds, string $filedScope, string $index)
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, [
            'goodsIds' => $goodsIds,
            'filedScope' => $filedScope,
            'index' => $index,
        ]);
    }

    /**
     * 获取店铺前N条最新商品&销量最高商品
     *
     * @param array $storeIds
     * @param int $num 取前N条
     * @return array
     *
     * @author chenyuhua
     * @since 2020.11.03
     */
    public static function getStoreBeforeGoods(array $storeIds, int $num): array
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, [
            'storeIds' => $storeIds,
            'num' => $num,
        ]);
    }

    /**
     * 获取店铺前N条销量最高商品
     *
     * @param array $storeIds
     * @param int $num 取前N条
     * @return array
     *
     * @author chenyuhua
     * @since 2020.11.03
     */
    public static function getStoreHighestSalesGoods(array $storeIds, int $num): array
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, [
            'storeIds' => $storeIds,
            'num' => $num,
        ]);
    }

    /**
     * 根据goodsIds获取商品信息
     *
     * @param array $goodsIds
     * @param int $ifShow
     * @param string $fields
     *
     * @author chenyuhua
     * @since 2020.11.04
     */
    public function getInfoByIds(array $goodsIds, int $ifShow = 0, $fields = 'goodsById')
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, [
            'goodsIds' => $goodsIds,
            'ifShow' => $ifShow,
            'fields' => $fields,
        ]);
    }

    /**
     * goods字段排序--根据店铺ID获取上架商品列表的最基本信息，并有排序和分页功能
     *
     * @param string|array $storeId      店铺ID
     * @param string       $order        排序字段
     * @param int          $limit        limit
     * @param int          $page         page
     * @param int          $cateId       分类，cate_id_1字段
     * @param string       $keyword      关键字
     * @param array        $lastShowTime 上架时间
     * @param bool         $ifKddClient  是否kdd
     *
     * @author chenyuhua
     * @since 2021.03.11
     */
    public function getCoreInfoForStoreID($storeId, $order, $limit, $page = 0, $cateId = 0, $keyword = '', $lastShowTime = ['egt', 0], $ifKddClient = false)
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, [
            'storeId' => $storeId,
            'order' => $order,
            'limit' => $limit,
            'page' => $page,
            'cateId' => $cateId,
            'keyword' => $keyword,
            'lastShowTime' => $lastShowTime,
            'ifKddClient' => $ifKddClient,
        ]);
    }

    /**
     * 销量排序--根据店铺ID获取按销量排序的上架商品列表的最基本信息，并有分页功能
     *
     * @param string|array $storeId        店铺ID
     * @param string       $desc           排序方式
     * @param int          $limit          limit
     * @param int          $page           page
     * @param int          $cateId         分类，cate_id_1字段
     * @param string       $keyword        关键字
     * @param bool         $specifyGoodsId 指定商品Id
     * @param bool         $ifKddClient    是否kdd
     *
     * @author chenyuhua
     * @since 2021.03.11
     */
    public function getGoodsListForSales($storeId, $desc, $limit, $page, $cateId = 0, $keyword = '', $specifyGoodsId = false, $ifKddClient = false)
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, [
            'storeId' => $storeId,
            'desc' => $desc,
            'limit' => $limit,
            'page' => $page,
            'cateId' => $cateId,
            'keyword' => $keyword,
            'specifyGoodsId' => $specifyGoodsId,
            'ifKddClient' => $ifKddClient,
        ]);
    }

    /**
     * 通过商品ID获取商品数据.
     *
     * @param array $goodsIds
     * @param bool  $ifKddClient 是否kdd
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2021.03.11
     */
    public function getCoreInfoForGoodsIds(array $goodsIds, bool $ifKddClient = false): array
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, [
            'goodsIds' => $goodsIds,
            'ifKddClient' => $ifKddClient,
        ]);
    }

    /**
     * 获取商品最低价/最高价.
     *
     *
     * @param array $goodsIds 商品ID数组
     * @param int   $type     类型： 1.为以数量报价取价格 2.区分规格报价和数量报价
     *
     * @return array
     *
     * @author chenyuhua
     * @since  2021.04.28
     */
    public function getPriceAreaByGoodsIds(array $goodsIds, $type = 1): array
    {
        return EellyClient::requestJson('eellyOldCode/goods/goods', __FUNCTION__, [
            'goodsIds'  => $goodsIds,
            'type'      => $type,
        ]);
    }
}
