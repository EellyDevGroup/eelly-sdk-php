<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Goods;

use Eelly\SDK\EellyClient;

/**
 * Class GoodsPrice.
 *
 *  modules/Goods/Service/GoodsPriceService.php
 *
 * @author chenyuhua
 * @since 2021.01.13
 */
class GoodsPrice
{
    /**
     * 获取商品价格，有区间的获取区间价格
     *
     * @param array $goodsId
     * @return array
     *
     * @author chenyuhua
     * @since  2021.01.13
     */
    public function getPrice(array $goodsId)
    {
        return EellyClient::requestJson('eellyOldCode/goods/goodsPrice', __FUNCTION__, ['goodsId' => $goodsId]);
    }
}
