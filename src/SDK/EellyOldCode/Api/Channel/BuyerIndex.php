<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Channel;

use Eelly\SDK\EellyClient;

/**
 * Class BuyerIndexService.
 *
 * modules/Channel/Service/BuyerIndexService.php
 *
 * @author zhangyangxun
 */
class BuyerIndex
{
    /**
     * 获取店+app页面广告
     *
     * @param string $pageType 广告类型
     * @param string $position 广告位置
     *
     * @return array
     * @returnExample({"title":"等等","image":"https://img03.eelly.test/G01/M00/00/06/oYYBAFngFm2IH3pxAAFLsYVI-38AAACZwKItSEAAUvJ211.png","type":"9","link_type":"","link_id":0,"content":{"odst":"0","frps":"","bhlf":"","rtn":"","have_video":"","rank_type":"1"},"content_url":"","sort":"1","article":"","rank_type":"1","rank_id":0})
     *
     * @author zhangyangxun
     * @since 2019-01-04
     */
    public static function getPageAd(string $pageType, string $position = '')
    {
        return EellyClient::requestJson('eellyOldCode/Channel/BuyerIndex', __FUNCTION__, ['pageType' => $pageType, 'position' => $position]);
    }

    public static function getIndexHotDistrict()
    {
        return EellyClient::requestJson('eellyOldCode/Channel/BuyerIndex', __FUNCTION__);
    }

    public static function getIndexMsgDynamic()
    {
        return EellyClient::requestJson('eellyOldCode/Channel/BuyerIndex', __FUNCTION__);
    }
}
