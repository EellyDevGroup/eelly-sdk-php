<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Store\ServiceTools\Rado;
use Eelly\SDK\EellyClient;

/**
 * Class ServiceToolsMember.
 *
 * modules/Store/Service/ServiceTools/Rado
 *
 * @author chenyuhua
 */
class ServiceToolsMember
{
    /**
     * 雷达扫客V3(精准获客).
     *
     * @param array $info   扫客条件
     * @param int   $advertiserId 广告商ID
     * @param int   $sRows  扫描行数
     *
     * @author chenyuhua
     *
     * @since  2022.04.28
     */
    public static function getSearchCustomers3(array $info, int $advertiserId, int $sRows = 1000)
    {
        return EellyClient::requestJson('eellyOldCode/store/ServiceTools/Rado/ServiceToolsMember', __FUNCTION__, [
            'info' => $info,
            'advertiserId' => $advertiserId,
            'sRows' => $sRows,
        ]);
    }
}
