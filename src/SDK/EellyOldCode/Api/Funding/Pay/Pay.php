<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\EellyOldCode\Api\Funding\Pay;

use Eelly\SDK\EellyClient;

/**
 * Class Cart.
 *
 *  modules/funding/pay/Service/PayService.php
 *
 * @author wechan
 */
class Pay
{
    /**
     * 根据PrId获取关联的诚信服务ID.
     *
     * @param int $prId 支付ID
     *
     * @return int $serviceId   服务ID
     */
    public function getPayIntegrityRelationByPrId($prId)
    {
        return EellyClient::request('eellyOldCode/funding/pay/pay', __FUNCTION__, true, $prId);
    }

    /**
     * APP接口：添加余额支付跳转参数.
     *
     * @param array $param app请求参数
     *
     * @return string
     *
     * @author wechan
     * @since 2020年2月13日
     */
    public function addBalanceAppPayParams(array $param):array
    {
        return EellyClient::request('eellyOldCode/funding/pay/pay', __FUNCTION__, true, $param);
    }

    /**
     * 买家增值服务支付参数
     *
     * @param array $param   app请求参数
     * @param int   $payType 支付类型：1 余额 2.支付宝 3.微信
     *
     * @author  wechan
     *
     * @since   2017年3月19日
     */
    public function getBuyerGeneralizePayParams(array $param, int $payType)
    {
        return EellyClient::requestJson('eellyOldCode/funding/pay/pay', __FUNCTION__, [
            'param' => $param,
            'payType' => $payType
        ]);
    }
}
