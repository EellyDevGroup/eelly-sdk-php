<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Store\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\Store\Service\AddressInterface;
use Eelly\DTO\UidDTO;
use Eelly\SDK\Store\DTO\AddressDTO;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class FansPackage
{
    /**
     * 添加粉丝包
     *
     * @param array $pack
     * @return bool
     * @author zhangyangxun<542207975@qq.com>
     * @since 2020/4/28
     * @internal
     */
    public static function addFansPackage(array $pack): bool
    {
        return EellyClient::requestJson('store/fansPackage', __FUNCTION__, ['pack' => $pack]);
    }

    /**
     * 粉丝包详情
     *
     * @param int $id
     * @return array
     * @throws \Throwable
     * @author zhangyangxun<542207975@qq.com>
     * @since 2020/4/28
     * @internal
     */
    public static function getFansPackage(int $id): array
    {
        return EellyClient::requestJson('store/fansPackage', __FUNCTION__, ['id' => $id]);
    }

    /**
     * 粉丝包分页列表
     *
     * @param array $condition
     * @param int $page
     * @param int $limit
     * @param string $fieldScope
     * @param string $order
     * @return array
     * @throws \Throwable
     * @author zhangyangxun<542207975@qq.com>
     * @since 2020/4/28
     * @internal
     */
    public static function listFansPackagePage(array $condition, int $page = 1, int $limit = 20, string $fieldScope = 'base', string $order = 'sfp.created_time desc'): array
    {
        return EellyClient::requestJson('store/fansPackage', __FUNCTION__, [
            'condition' => $condition,
            'page' => $page,
            'limit' => $limit,
            'fieldScope' => $fieldScope,
            'order' => $order,
        ]);
    }

    /**
     * 发放粉丝包
     *
     * @param int $id
     * @return bool
     * @throws \Throwable
     * @author zhangyangxun<542207975@qq.com>
     * @since 2020/4/28
     * @internal
     */
    public static function sendFansPackage(int $id): bool
    {
        return EellyClient::requestJson('store/fansPackage', __FUNCTION__, ['id' => $id]);
    }
}