<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Store\Api;

use Eelly\DTO\UidDTO;
use Eelly\SDK\EellyClient;

/**
 * @author shadonTools<localhost.shell@gmail.com>
 */
class StoreTiktok
{
    /*
     * 获取批量上传设置
     *
     * >字段名 | 类型 |描述
     * >-- | ---- | -----
     * toBeUpload | int | 待上传商品件数
     * weight | int | 重量 单位 千克
     * profit | string | 利润
     * profitType | int | 利润方式，0指元，1指百分比
     * marketPrice | string | 划线价
     * payType | int | 支付方式 0 货到付款 1 在线支付 2 货到付款 + 在线支付
     * presellType | int | 发货模式 0 现货发货 1 预售发货
     * deliveryDelayDay | int | 发货时间 数字是多少就是多少天
     * supplyDayReturn | int | 7天无理由 0 不支持 1 支持 2 拆封后不支持
     * reduceType | int | 减库存方式 1 拍下减库存 2 付款减库存
     * mobile | string | 客服电话号码
     *
     * @param int $storeId 用户id
     * @return array
     *
     * @author wechan
     * @since 2021年04月10日
     */
    public function getSettingByStoreId(int $storeId): array
    {
        return EellyClient::requestJson('store/storeTiktok', __FUNCTION__, [
            'storeId' => $storeId,
        ]);
    }

    /**
     * 获取批量上传设置
     *
     * @param UidDTO|null $user
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.02
     */
    public function getStoreTiktokUploadSetting(UidDTO $user = null): array
    {
        return EellyClient::requestJson('store/storeTiktok', __FUNCTION__);
    }

    /**
     * 保存店铺批量上传商品设置
     *
     * @param array $param              设置参数
     * @param int $param.weight         重量 单位kg
     * @param int $param.profit         利润
     * @param int $param.profitType     利润方式，0指元，1指百分比
     * @param int $param.marketPrice    划线价
     * @param int $param.payType        支付方式 0 货到付款 1 在线支付 2 货到付款 + 在线支付
     * @param int $param.presellType    发货模式 0 现货发货 1 预售发货
     * @param int $param.deliveryDelayDay    发货时间 数字是多少就是多少天
     * @param int $param.supplyDayReturn     7天无理由 0 不支持 1 支持 2 拆封后不支持
     * @param int $param.reduceType     减库存方式 1 拍下减库存 2 付款减库存
     * @param string $param.mobile      客服电话号码
     * @param UidDTO|null $user
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.10
     */
    public function saveStoreTiktokUploadSetting(array $param, UidDTO $user = null): array
    {
        return EellyClient::requestJson('store/storeTiktok', __FUNCTION__, ['param' => $param]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
