<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Store\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\Store\Service\AddressInterface;
use Eelly\DTO\UidDTO;
use Eelly\SDK\Store\DTO\AddressDTO;

/**
 * @author shadonTools<localhost.shell@gmail.com>
 */
class StoreAgentList
{
    /**
     * 新增带货供应商或带货主播
     *
     * @param array $data  需要添加的数据
     *
     * @return bool
     *
     * @author chentuying
     * @since 2020-12-17
     */
    public function addSupplierOrAnchor(array $data):bool
    {
        return EellyClient::requestJson('store/storeAgentList', __FUNCTION__, ['data' => $data], true);
    }

    /**
     * 直播带货管理开启或关闭
     *
     * @param int $salId  带货管理配置id
     * @param int $status 状态 0 关闭 1 开启
     * @param int $userId 管理员id
     *
     * @return bool
     *
     * @author chentuying
     * @since 2020-12-18
     */
    public function editSupplierOrAnchorStatus(int $salId, int $status, int $userId):bool
    {
        return EellyClient::requestJson('store/storeAgentList', __FUNCTION__, ['salId' => $salId, 'status' => $status, 'userId' => $userId], true);
    }

    /**
     * 获取带货供应商或带货主播数据
     *
     * @param array $data 查询条件
     * @param int $type 1 带货供应商 2 带货主播
     * @param int $page 第几页
     * @param int $limit 限制条数
     * @param string $order 排序
     *
     * @return array
     *
     * @author chentuying
     * @since 2020-12-19
     */
    public function getSupplierOrAnchorList(array $data, int $type, int $page = 1, int $limit = 10, string $orderBy = 'sal.created_time DESC'):array
    {
        return EellyClient::requestJson('store/storeAgentList', __FUNCTION__, [
            'data'    => $data,
            'type'    => $type,
            'page'    => $page,
            'limit'   => $limit,
            'orderBy' => $orderBy
        ]);
    }

    /**
     * 判断指定店铺是否带货供应商或带货主播
     *
     * @param int $storeId
     * @param int $type 1 直播带货 2 带货主播
     * @return bool
     *
     * @author chenyuhua
     * @since 2020.12.18
     */
    public function checkSupplierOrAnchor(int $storeId, int $type): array
    {
        return EellyClient::requestJson('store/storeAgentList', __FUNCTION__, [
            'storeId' => $storeId,
            'type'  => $type,
        ]);
    }

    /**
     * 跟据带货管理配置id
     *
     * @param int $salId 带货管理配置id
     * @return array
     *
     * @author wechan
     * @since 2020年12月18日
     */
    public function getAgentOneBySalId(int $salId):array
    {
        return EellyClient::requestJson('store/storeAgentList', __FUNCTION__, ['salId' => $salId]);
    }

    /**
     * 跟据店铺id和类型获取一条带货配置信息
     *
     * @param int $storeId 店铺id
     * @param int $type 类型 1.直播带货 2.带货主播
     * @return array
     *
     * @author wechan
     * @since 2020年12月18日
     */
    public function getAgentOneBystoreIdType(int $storeId, int $type):array
    {
        return EellyClient::requestJson('store/storeAgentList', __FUNCTION__, [
            'storeId' => $storeId,
            'type'    => $type,
        ]);
    }

    /**
     * 获取货源供应商店铺数据
     *
     * @param int $type 类型 1 供应商 2 带货主播
     * @return array
     *
     * @author chenyuhua
     * @since 2020.12.17
     */
    public function getSupplierOrAnchor(int $type): array
    {
        return EellyClient::requestJson('store/storeAgentList', __FUNCTION__, ['type' => $type]);
    }

    /**
     * 通过店铺获取带货管理配置id
     *
     * @param array $storeIds
     * @param int $type
     * @return array
     *
     * @author chenyuhua
     * @since 2021.03.05
     */
    public function getAgentByStoreIds(array $storeIds, int $type): array
    {
        return EellyClient::requestJson('store/storeAgentList', __FUNCTION__, [
            'storeIds' => $storeIds,
            'type' => $type,
        ]);
    }

    /**
     * 根据salId 或者  agentLiveId 返回 拿货主播信息
     *
     * @param array $extend 扩展字段
     * @param int $extend['agentLiveId'] 直播id
     * @param int $extend['salId'] 拿货主播id
     * @return array
     *
     * @author wechan
     * @since 2021年03月17日
     */
    public function getAgentByExtend(array $extend):array
    {
        return EellyClient::requestJson('store/storeAgentList', __FUNCTION__, [
            'extend' => $extend
        ]);
    }
}
