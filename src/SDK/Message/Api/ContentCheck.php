<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Message\Api;

use Eelly\SDK\EellyClient;

/**
 * @author shadonTools<localhost.shell@gmail.com>
 */
class ContentCheck
{
    /**
     * 内容检测（微信版本）
     * 
     * @param string $content 内容
     * @param array $extra 额外拓展使用
     *
     * @return boolean
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.9.24
     */
    public function contentCheck(string $content, array $extra = []):bool
    {
        return EellyClient::request('message/contentCheck', __FUNCTION__, true, $content, $extra);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}