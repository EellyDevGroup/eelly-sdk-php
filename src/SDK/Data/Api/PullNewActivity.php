<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Data\Api;

use Eelly\SDK\EellyClient;

/**
 * 裂变拉新活动数据统计
 *
 * @author chenyuhua
 */
class PullNewActivity
{

    public function getActivitySummary(array $activityIds): array
    {
        return EellyClient::requestJson('data/pullNewActivitySummary', __FUNCTION__, ['activityIds' => $activityIds]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
