<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Data\Api;

use Eelly\SDK\EellyClient;

/**
 * 省钱月卡活动统计
 *
 * @author chenyuhua
 */
class ActivityCouponSummary
{

    public function getActivityCouponSummary(array $activityIds): array
    {
        return EellyClient::requestJson('data/activityCouponSummary', __FUNCTION__, ['activityIds' => $activityIds]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
