<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Data\Api;

use Eelly\SDK\EellyClient;

/**
 * 分享赚钱统计
 *
 * @author zhangyingdi<zhangyingdi@eelly.net>
 */
class ActivitySpreadStat
{
    /**
     * @inheritdoc
     */
    public function addStatData(array $data):bool
    {
        return EellyClient::requestJson('data/activitySpreadStat', __FUNCTION__,  [
            'data' => $data
        ]);
    }

    /**
     * @inheritdoc
     */
    public function listActivitySpreadStat(array $conditions, int $page = 1, int $limit = 20, string $order = 'base'):array
    {
        return EellyClient::requestJson('data/activitySpreadStat', __FUNCTION__,  [
            'conditions' => $conditions,
            'page' => $page,
            'limit' => $limit,
            'order' => $order,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function listTotalData(string $condition = '', array $bind = []):array
    {
        return EellyClient::requestJson('data/activitySpreadStat', __FUNCTION__,  [
            'condition' => $condition,
            'bind' => $bind
        ]);
    }

    /**
     * @inheritdoc
     */
    function listStatData(array $userIds):array
    {
        return EellyClient::requestJson('data/activitySpreadStat', __FUNCTION__,  [
            'userIds' => $userIds
        ]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}