<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Data\Api;

use Eelly\SDK\EellyClient;

/**
 * Class FootprintLook.
 *
 *
 */
class FootprintLook
{
    /**
     * 添加用户足迹找货--看过的款数据
     *
     * @param  array $data
     * @return mixed
     *
     * @author wechan
     * @since  2019年10月11日
     */
    public function addFootprintLook(array $data):bool
    {
        return EellyClient::requestJson('data/footPrintLook', __FUNCTION__, [
            'data' => $data,
        ]);
    }
}
