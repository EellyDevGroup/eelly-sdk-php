<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Live\Api;

use Eelly\SDK\EellyClient;

/**
 * 视频直播优惠促销
 * 
 * @author shadonTools<localhost.shell@gmail.com>
 */
class ActivityTip
{
    /**
     * 获取活动管理列表 
     *
     * @param array $liveIds 直播间id
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.10.11
     */
    public function adminList(array $liveIds):array
    {
        return EellyClient::request('live/activityTip', 'adminList', true, $liveIds);
    }

    /**
     * 根据查询条件获取直播优惠促销数据
     *
     * @param array $condition
     * @param string $field
     * @param array $extend
     * @return array
     *
     * @author chenyuhua
     * @since 2020.11.13
     */
    public function getLiveActivityTipList(array $condition, string $field = 'base', array $extend = []): array
    {
        return EellyClient::requestJson('live/activityTip', __FUNCTION__, [
            'condition' => $condition,
            'field'     => $field,
            'extend'    => $extend,
        ]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}