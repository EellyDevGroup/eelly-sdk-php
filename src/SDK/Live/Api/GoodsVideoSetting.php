<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Live\Api;

use Eelly\SDK\EellyClient;

/**
 * @author shadonTools<localhost.shell@gmail.com>
 */
class GoodsVideoSetting
{
    /**
     * 获取直播录制设置
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2021.05.12
     */
    public function getSetting(): array
    {
        return EellyClient::requestJson('live/goodsVideoSetting', __FUNCTION__);
    }

    /**
     * 保存直播录制配置
     *
     * @param array $data
     * @return int[]
     * @throws \ErrorException
     *
     * @author chenyuhua
     * @since 2021.05.17
     */
    public function saveSetting(array $data): array
    {
        return EellyClient::requestJson('live/goodsVideoSetting', __FUNCTION__, ['data' => $data]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
