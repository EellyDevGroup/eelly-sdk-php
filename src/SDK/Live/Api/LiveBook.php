<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\Live\Api;

use Eelly\SDK\EellyClient;
use Eelly\DTO\UidDTO;

class LiveBook
{
    /**
     * 后台 厂+直播预约管理,头部
     *
     * @since 2021-06-09T09:33:37+0800
     * @author twb<1174865138@qq.com>
     */
    public static function getShopSubscribeAmount(array $condition = []): array
    {
        return EellyClient::requestJson('live/liveBook', __FUNCTION__, ['condition' => $condition], true);
    }

    /**
     * 后台 厂+直播预约管理
     *
     * @param array   $condition     搜索条件
     * @param int   $currentPage     页数
     * @param int   $limit     单页限制
     *
     * @since 2021-06-09T09:33:37+0800
     * @author twb<1174865138@qq.com>
     */
    public static function getAdminLiveBookList(array $condition = [], int $currentPage = 1, int $limit = 100): array
    {
        return EellyClient::requestJson('live/liveBook', __FUNCTION__, ['condition' => $condition, 'currentPage' => $currentPage, 'limit' => $limit], true);
    }

    /**
     * 店铺预约历史
     *
     * @param array   $condition     搜索条件
     * @param int   $currentPage     页数
     * @param int   $limit     单页限制
     *
     * @since 2021-06-09T09:33:37+0800
     * @author twb<1174865138@qq.com>
     */
    public static function getAdminliveBookDetail(array $condition = [], int $currentPage = 1, int $limit = 100): array
    {
        return EellyClient::requestJson('live/liveBook', __FUNCTION__, ['condition' => $condition, 'currentPage' => $currentPage, 'limit' => $limit], true);
    }

    /**
     * 查看店铺的可预约数据
     *
     * @param int   $storeId     店铺id
     *
     * @since 2021-06-09T09:33:37+0800
     * @author twb<1174865138@qq.com>
     */
    public static function checkAdminliveBook(int $storeId = 100): array
    {
        return EellyClient::requestJson('live/liveBook', __FUNCTION__, ['storeId' => $storeId], true);
    }

    /**
     * 手动直播预约
     *
     * @param array   $params     预约数据
     *
     * @since 2021-06-09T09:33:37+0800
     * @author twb<1174865138@qq.com>
     */
    public static function adminliveBook(array $params = []): array
    {
        return EellyClient::requestJson('live/liveBook', __FUNCTION__, ['params' => $params], true);
    }

    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
