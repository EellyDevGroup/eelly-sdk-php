<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Live\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\Live\Service\ChatroomInterface;

/**
 * Class Chatroom.
 */
class Chatroom
{
    /**
     * {@inheritdoc}
     */
    public function sendChangeLiveGoodsPriceChatroomMsg(int $liveId, array $extBody): bool
    {
        return EellyClient::request('live/chatroom', __FUNCTION__, true, $liveId, $extBody);
    }

    /**
     * {@inheritdoc}
     */
    public function sendChangeGoodsStockChatroomMsg(int $liveId, array $extBody): bool
    {
        return EellyClient::request('live/chatroom', __FUNCTION__, true, $liveId, $extBody);
    }

    /**
     * {@inheritdoc}
     */
    public function sendLiveTaskMsg(int $liveId, array $extBody): bool
    {
        return EellyClient::request('live/chatroom', __FUNCTION__, true, $liveId, $extBody);
    }

    /**
     * 群发发直播间商品改价弹幕
     *
     * @param array $extBody
     * @return bool
     *
     * @author chenyuhua
     * @since 2021.01.06
     */
    public function sendChangeLiveGoodsPriceChatroomMsgV2(array $extBody): bool
    {
        return EellyClient::requestJson('live/chatroom', __FUNCTION__, ['extBody' => $extBody]);
    }

    /**
     * 群发补库存后重新上架事件
     *
     * @param array $extBody
     * @return bool
     *
     * @author chenyuhua
     * @since 2021.01.06
     */
    public function sendChangeGoodsStockChatroomMsgV2(array $extBody): bool
    {
        return EellyClient::requestJson('live/chatroom', __FUNCTION__, ['extBody' => $extBody]);
    }

    /**
     * 粉丝领取红包事件
     *
     * @param int $liveId
     * @param int $userId
     * @return bool
     * @throws \Throwable
     *
     * @author chenyuhua
     * @since 2021.01.15
     */
    public function sendRedPacketReceiveMsg(int $liveId, int $userId): bool
    {
        return EellyClient::requestJson('live/chatroom', __FUNCTION__, [
            'liveId' => $liveId,
            'userId' => $userId,
        ]);
    }

    /**
     * 红包被抢光事件
     *
     * @param int $liveId
     * @param array $extBody
     * @return bool
     * @throws \Throwable
     *
     * @author chenyuhua
     * @since 2021.01.15
     */
    public function sendRedPacketOverMsg(int $liveId, array $extBody): bool
    {
        return EellyClient::requestJson('live/chatroom', __FUNCTION__, [
            'liveId' => $liveId,
            'extBody' => $extBody,
        ]);
    }

    /**
     * 红包倒计时事件
     *
     * @param int $lrrId
     * @return bool
     * @throws \Throwable
     *
     * @author chenyuhua
     * @since 2020.12.16
     */
    public function sendRedPacketCountDownMsg(int $lrrId): bool
    {
        return EellyClient::requestJson('live/chatroom', __FUNCTION__, ['lrrId' => $lrrId]);
    }

    /**
     * 冲榜公告事件
     *
     * @param int $lrrId
     * @return bool
     *
     * @author chenyuhua
     * @since 2020.12.16
     */
    public function broadCastRedPacketPunchStart(int $lrrId): bool
    {
        return EellyClient::requestJson('live/chatroom', __FUNCTION__, ['lrrId' => $lrrId]);
    }
}
