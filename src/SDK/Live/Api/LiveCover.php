<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Live\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class LiveCover
{
    public function adminGetOne(int $lcId)
    {
        return EellyClient::request('live/liveCover', __FUNCTION__, true, $lcId);
    }

    public function adminGetList(array $condition = [], int $page = 1, int $limit = 20)
    {
        return EellyClient::request('live/liveCover', __FUNCTION__, true, $condition, $page, $limit);
    }

    public function adminDoReview(int $lcId, int $toStatus, array $extend = [])
    {
        return EellyClient::request('live/liveCover', __FUNCTION__, true, $lcId, $toStatus, $extend);
    }

    public function adminGetLog(int $lcId)
    {
        return EellyClient::request('live/liveCover', __FUNCTION__, true, $lcId);
    }

    /**
     * 更新直播封面横屏图
     *
     * @param int $lcId  直播封面id
     * @param array $data  修改数据
     * @return bool
     *
     * @author chentuying
     * @since 2020-09-25
     */
    public function updateLiveCover(int $lcId, array $data)
    {
        return EellyClient::request('live/liveCover', __FUNCTION__, true, $lcId, $data);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}