<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\Live\Api;

use Eelly\SDK\EellyClient as Client20200513;
use Eelly\SDK\GetInstanceTrait;

/**
 * This class has been auto-generated by shadon compiler (2020-05-13 06:46:58).
 */
class Subscribe
{
    use GetInstanceTrait;
    /**
     *
     * @author eellytools<localhost.shell@gmail.com>
     */
    public static function getSubscribe(int $subscribeId) : SubscribeDTO
    {
        return Client20200513::requestJson('live/subscribe', 'getSubscribe', ['subscribeId' => $subscribeId], true);
    }
    /**
     *
     * @author eellytools<localhost.shell@gmail.com>
     */
    public static function getSubscribeAsync(int $subscribeId)
    {
        return Client20200513::requestJson('live/subscribe', 'getSubscribe', ['subscribeId' => $subscribeId], false);
    }
    /**
     * 通过用户获取直播的直播id频道.
     *
     * @param int $userId 用户ID
     * @param array $liveIds 直播ID
     * @return array
     * @requestExample({"userId":148086,"liveIds":[1,2,3]})
     * @returnExample({1,2,3})
     * @author 肖俊明<xiaojunming@eelly.net>
     * @since 2018年01月24日
     * @Validation(
     *  @OperatorValidator(0,{message:"非法用户ID",operator:["gt",0]})
     *)
     */
    public static function getUserSubscribeLiveIds(int $userId, array $liveIds = []) : array
    {
        return Client20200513::requestJson('live/subscribe', 'getUserSubscribeLiveIds', ['userId' => $userId, 'liveIds' => $liveIds], true);
    }
    /**
     * 通过用户获取直播的直播id频道.
     *
     * @param int $userId 用户ID
     * @param array $liveIds 直播ID
     * @return array
     * @requestExample({"userId":148086,"liveIds":[1,2,3]})
     * @returnExample({1,2,3})
     * @author 肖俊明<xiaojunming@eelly.net>
     * @since 2018年01月24日
     * @Validation(
     *  @OperatorValidator(0,{message:"非法用户ID",operator:["gt",0]})
     *)
     */
    public static function getUserSubscribeLiveIdsAsync(int $userId, array $liveIds = [])
    {
        return Client20200513::requestJson('live/subscribe', 'getUserSubscribeLiveIds', ['userId' => $userId, 'liveIds' => $liveIds], false);
    }
    /**
     *
     * @author eellytools<localhost.shell@gmail.com>
     */
    public static function listSubscribePage(array $condition = [], int $currentPage = 1, int $limit = 10) : array
    {
        return Client20200513::requestJson('live/subscribe', 'listSubscribePage', ['condition' => $condition, 'currentPage' => $currentPage, 'limit' => $limit], true);
    }
    /**
     *
     * @author eellytools<localhost.shell@gmail.com>
     */
    public static function listSubscribePageAsync(array $condition = [], int $currentPage = 1, int $limit = 10)
    {
        return Client20200513::requestJson('live/subscribe', 'listSubscribePage', ['condition' => $condition, 'currentPage' => $currentPage, 'limit' => $limit], false);
    }
    /**
     * 根据传过来的查询条件，返回对应的数据
     *
     * @param array $condition 查询的where条件
     * @return array
     *
     * @requestExample({"condition":{"liveId":1}})
     * @returnExample([{"lsId": 1, "liveId": 1, "userId": 148086, "createdTime": 0}])
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2018.01.25
     */
    public static function getSubscribeList(array $condition = []) : array
    {
        return Client20200513::requestJson('live/subscribe', 'getSubscribeList', ['condition' => $condition], true);
    }
    /**
     * 根据传过来的查询条件，返回对应的数据
     *
     * @param array $condition 查询的where条件
     * @return array
     *
     * @requestExample({"condition":{"liveId":1}})
     * @returnExample([{"lsId": 1, "liveId": 1, "userId": 148086, "createdTime": 0}])
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2018.01.25
     */
    public static function getSubscribeListAsync(array $condition = [])
    {
        return Client20200513::requestJson('live/subscribe', 'getSubscribeList', ['condition' => $condition], false);
    }
    /**
     * 添加一条订阅记录
     * @param array $data 订阅信息数据
     * @param int $data["liveId"]  直播id
     * @param int $data["userId"]  用户id
     * @return bool
     *
     * @requestExample({"data":{"userId":148086,"liveId":1}})
     * @returnExample(true)
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2018.02.06
     */
    public static function addSubscribe(array $data) : bool
    {
        return Client20200513::requestJson('live/subscribe', 'addSubscribe', ['data' => $data], true);
    }
    /**
     * 添加一条订阅记录
     * @param array $data 订阅信息数据
     * @param int $data["liveId"]  直播id
     * @param int $data["userId"]  用户id
     * @return bool
     *
     * @requestExample({"data":{"userId":148086,"liveId":1}})
     * @returnExample(true)
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2018.02.06
     */
    public static function addSubscribeAsync(array $data)
    {
        return Client20200513::requestJson('live/subscribe', 'addSubscribe', ['data' => $data], false);
    }
}