<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Live\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\Live\Service\LiveSortSettingInterface;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class LiveSortSetting implements LiveSortSettingInterface
{
    /**
     * 获取所有积分规则
     * @internal
     *
     * @param array    $condition
     * @return array
     *
     * @author zhangyangxun
     * @since 2019-03-27
     */
    public function getSettings(array $condition = []): array
    {
        return EellyClient::request('live/liveSortSetting', 'getSettings', true, $condition);
    }

    /**
     * 获取所有积分规则
     * @internal
     *
     * @param array    $condition
     * @return array
     *
     * @author zhangyangxun
     * @since 2019-03-27
     */
    public function getSettingsAsync(array $condition = [])
    {
        return EellyClient::request('live/liveSortSetting', 'getSettings', false, $condition);
    }

    /**
     * @inheritdoc
     */
    public function getLiveOrderScore(int $userId, int $storeId, int $startTime, int $endTime):int
    {
        return EellyClient::request('live/liveSortSetting', __FUNCTION__, true, $userId, $storeId, $startTime, $endTime);
    }

    /**
     * 根据传过来的金额返回对应的转化分数
     *
     * @param int $orderAmount 支付订单金额（单位：分）
     * @return int
     */
    public function getPayOrderScoreByOrderAmount(int $orderAmount):int
    {
        return EellyClient::requestJson('live/liveSortSetting', __FUNCTION__, [$orderAmount]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
