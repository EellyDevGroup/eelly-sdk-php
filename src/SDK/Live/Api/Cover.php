<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Live\Api;

use Eelly\DTO\UidDTO;
use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class Cover
{
    public function submitReview(string $imageUrl, int $lcId = 0, UidDTO $user = null):bool
    {
        return EellyClient::request('live/cover', __FUNCTION__, true, $imageUrl, $lcId, $user);
    }

    public function listCoverData(UidDTO $user = null):array
    {
        return EellyClient::request('live/cover', __FUNCTION__, true, $user);
    }

    public function deleteCover(int $lcId, UidDTO $user = null):bool
    {
        return EellyClient::request('live/cover', __FUNCTION__, true, $lcId, $user);
    }

    public function setCover(int $lcId, UidDTO $user = null):bool
    {
        return EellyClient::request('live/cover', __FUNCTION__, true, $lcId, $user);
    }

    public function getStoreLiveCover(int $storeId):string
    {
        return EellyClient::request('live/cover', __FUNCTION__, true, $storeId);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}