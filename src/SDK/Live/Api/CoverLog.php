<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\Live\Api;

use Eelly\SDK\EellyClient as Client20191122;
use Eelly\SDK\GetInstanceTrait;

/**
 * This class has been auto-generated by shadon compiler (2019-11-22 02:09:30).
 */
class CoverLog
{
    use GetInstanceTrait;
}