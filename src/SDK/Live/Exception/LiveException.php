<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Live\Exception;

use Eelly\Exception\LogicException;

/**
 * 直播异常类.
 *
 * @author wechan<liweiquan@eelly.net>
 * @since  2018年01月24日
 */
class LiveException extends LogicException
{

    public const CHANNEL_FULL = '预约名额已满,请重选日期,或更换时段试试';

    public const ALREADY_BOOKED = '存在相同预约, 请免重复';

    public const NO_USE_IMAGES = '没有使用中的店家直播图片';

}
