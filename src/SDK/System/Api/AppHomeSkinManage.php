<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\System\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class AppHomeSkinManage
{
    /**
     * App皮肤管理列表
     *
     * @param array $condition  查询条件
     * @return array
     *
     * @author chentuying
     * @since 2020-09-12
     *
     * @internal
     */
    public function getHomeSkinList(array $condition = []): array
    {
        return EellyClient::request('system/appSkin', 'getHomeSkinList', true, $condition);
    }

    /**
     * App首页皮肤修改
     *
     * @param int $asId  皮肤id
     * @param array $data  要修改的数据
     * @return bool
     *
     * @author chentuying
     * @since 2020-09-14
     *
     * @internal
     */
    public function skinQuickUpdate(int $asId, array $data)
    {
        return EellyClient::requestJson('system/appSkin', __FUNCTION__, [
            'asId' => $asId,
            'data' => $data,
        ]);
    }

    /**
     * App首页皮肤删除
     *
     * @param int $asId  皮肤id
     * @return int
     *
     * @author chentuying
     * @since 2020-09-14
     *
     * @internal
     */
    public function deleteSkin(int $asId)
    {
        return EellyClient::request('system/appSkin', 'deleteSkin', true, $asId);
    }

    /**
     * 添加App首页皮肤
     *
     * @param array $data
     * @return bool
     *
     * @author chentuying
     * @since 2020-09-15
     *
     * @internal
     */
    public function addSkin(array $data): bool
    {
        return EellyClient::request('system/appSkin', 'addSkin', true, $data);
    }

    /**
     * 获取首页皮肤数据
     *
     * @param int $asId
     * @return array
     *
     * @author chentuying
     * @since 2020-09-15
     *
     * @internal
     */
    public function getOneSkin(int $asId): array
    {
        return EellyClient::request('system/appSkin', 'getOneSkin', true, $asId);
    }

    /**
     * 更新皮肤
     *
     * @param int   $asId
     * @param array $data
     * @return bool
     *
     * @author chentuying
     * @since 2020-09-15
     *
     * @internal
     */
    public function updateSkin(int $asId, array $data): bool
    {
        return EellyClient::request('system/appSkin', 'updateSkin', true, $asId, $data);
    }

    /**
     * 添加logoSlogan
     *
     * @param int $spvId
     * @param array $data
     * @return bool
     *
     * @author chentuying
     * @since 2020-09-15
     *
     * @internal
     */
    public function editLogoSlogan(int $spvId, array $data): bool
    {
        return EellyClient::request('system/appSkin', 'editLogoSlogan', true, $spvId, $data);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}