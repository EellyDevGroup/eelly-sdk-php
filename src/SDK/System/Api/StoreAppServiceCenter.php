<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\System\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\EellyClient as Client20200310;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class StoreAppServiceCenter
{
    /**
     * 更新remark数据
     *
     * @param string $paramDesc
     * @param string $remark
     * @return int
     *
     * @author chentuying
     * @since 2020-09-16
     *
     * @internal
     */
    public function updateRemark(string $paramDesc, string $remark)
    {
        return Client20200310::request('system/storeAppServiceCenter', 'updateRemark', true, $paramDesc, $remark);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}