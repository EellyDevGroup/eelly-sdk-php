<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\System\Api;

use Eelly\SDK\EellyClient as Client20191122;
use Eelly\SDK\GetInstanceTrait;

/**
 * This class has been auto-generated by shadon compiler (2019-11-22 02:09:32).
 */
class System
{
    use GetInstanceTrait;
    /**
     * 获取当前系统时间
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since   2018.01.31
     */
    public static function getSystemTime() : int
    {
        return Client20191122::requestJson('system/system', 'getSystemTime', [], true);
    }
    /**
     * 获取当前系统时间
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since   2018.01.31
     */
    public static function getSystemTimeAsync()
    {
        return Client20191122::requestJson('system/system', 'getSystemTime', [], false);
    }
}