<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Pay\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class Stat
{
    /**
     * 获取时间段内充值金额和充值笔数
     *
     * @param integer $duration
     * @param integer $startTime
     * @param integer $endTime
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.31
     */
    public function getIncreFundStat(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return EellyClient::requestJson('pay/payStat', 'getIncreFundStat', [
            'duration' => $duration,
            'startTime' => $startTime,
            'endTime' => $endTime,
        ]);
    }    

    /**
     * 获取平台总资金、提现资金、资金店铺数、资金用户数、商家资金金额、采购商资金金额、平台资金金额
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.31
     */
    public function getFundStat(): array
    {
        return EellyClient::requestJson('pay/payStat', 'getFundStat', []);
    }

    /**
     * 用户资金排行
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.31
     */
    public function getUserFundRank(): array
    {
        return EellyClient::requestJson('pay/payStat', 'getUserFundRank', []);
    }

    /**
     * 店铺资金排行
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.31
     */
    public function getStoreFundRank(): array
    {
        return EellyClient::requestJson('pay/payStat', 'getStoreFundRank', []);
    }

    /**
     * 店铺资金变动排行
     *
     * @param integer $duration
     * @param integer $startTime
     * @param integer $endTime
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.31
     */
    public function getStoreFundChangeRank(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return EellyClient::requestJson('pay/payStat', 'getStoreFundChangeRank', [
            'duration' => $duration,
            'startTime' => $startTime,
            'endTime' => $endTime,
        ]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}