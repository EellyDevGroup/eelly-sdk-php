<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Pay\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\Pay\Service\RecordInterface;
use Eelly\SDK\Pay\DTO\RecordDTO;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class Record
{
    /**
     * @author eellytools<localhost.shell@gmail.com>
     */
    public function getRecord(int $prId): RecordDTO
    {
        return EellyClient::request('pay/record', 'getRecord', true, $prId);
    }

    /**
     * @author eellytools<localhost.shell@gmail.com>
     */
    public function getRecordAsync(int $prId)
    {
        return EellyClient::request('pay/record', 'getRecord', false, $prId);
    }

    /**
     * 添加会员核心交易数据.
     *
     * @param array $data
     * @param int $data["fromPaId"]     来源帐户ID
     * @param int $data["toPaId"]       目标帐户ID
     * @param int $data["type"]         操作类型：1 充值 2 提现 3 消费 4 结算 5 退款 6 诚保冻结 7 诚保解冻
     * @param int $data["itemId"]       关联ID
     * @param string $data["billNo"]    衣联交易号
     * @param string $data["money"]     成交金额：单位为分
     * @param string $data["remark"]    备注：JSON格式
     *
     * @throws RecordException
     *
     * @return int
     * @requestExample({"data":{"fromPaId":111,"toPaId":222,"type":2,"itemId":11101,"billNo":"1711114177786cvA2s","money":"100",
     *     "remark":"备注"}})
     * @returnExample(1)
     *
     * @author zhangzeqiang<zhangzeqiang@eelly.net>
     * @since  2017年11月11日
     */
    public function addRecord(array $data): int
    {
        return EellyClient::request('pay/record', 'addRecord', true, $data);
    }

    /**
     * 添加会员核心交易数据.
     *
     * @param array $data
     * @param int $data["fromPaId"]     来源帐户ID
     * @param int $data["toPaId"]       目标帐户ID
     * @param int $data["type"]         操作类型：1 充值 2 提现 3 消费 4 结算 5 退款 6 诚保冻结 7 诚保解冻
     * @param int $data["itemId"]       关联ID
     * @param string $data["billNo"]    衣联交易号
     * @param string $data["money"]     成交金额：单位为分
     * @param string $data["remark"]    备注：JSON格式
     *
     * @throws RecordException
     *
     * @return int
     * @requestExample({"data":{"fromPaId":111,"toPaId":222,"type":2,"itemId":11101,"billNo":"1711114177786cvA2s","money":"100",
     *     "remark":"备注"}})
     * @returnExample(1)
     *
     * @author zhangzeqiang<zhangzeqiang@eelly.net>
     * @since  2017年11月11日
     */
    public function addRecordAsync(array $data)
    {
        return EellyClient::request('pay/record', 'addRecord', false, $data);
    }

    /**
     * pc店铺资金明细
     *
     * @param integer $tab 标签 1:全部 2:收入 3:支出
     * @param integer $category 收支类型 1:全部收支 2:全部收入 3:结算 4:服务退款 5:充值 6:全部支出 7:提现 8:平台交易佣金 9:提现手续费 10:消费
     * @param integer $page 页码 
     * @param integer $limit 每页限制
     * @param integer $timeType 时间类型范围 1:全部 2:近一个月 3:近三个月 4:半年
     * @param string $timeBegin 开始时间范围 标准格式时间 YYYY-mm-dd
     * @param string $timeEnd 结束时间范围 标准格式时间 YYYY-mm-dd
     * @param string $billNo 流水号
     * @param UidDTO $user 当前登录用户
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.10.21
     */
    public function sellerRecordListForPc(int $tab, int $category, int $page, int $limit, int $timeType = 1, string $timeBegin = '', string $timeEnd = '', string $billNo = '', UidDTO $user = null):array
    {
        return EellyClient::request('pay/record', 'sellerRecordListForPc', true, $tab, $category, $page, $limit, $timeType, $timeBegin, $timeEnd, $billNo, $user);
    }

    public static function statisticsForPlatformRedEnvelope(): array
    {
        return EellyClient::requestJson('pay/record', 'statisticsForPlatformRedEnvelope', []);
    }

    /**
     * 获取冲榜红包支付信息
     *
     * @param array $lrrIds  冲榜红包充值记录id
     *
     * @return array
     *
     * @author chentuying
     * @since 2020-12-30
     */
    public function getRedPacketPayInfo(array $lrrIds):array
    {
        return EellyClient::requestJson('pay/record', 'getRedPacketPayInfo', ['lrrIds' => $lrrIds]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}