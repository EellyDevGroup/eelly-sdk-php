<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\Pay\Api;

use Eelly\SDK\EellyClient as Client20200520;
use Eelly\SDK\GetInstanceTrait;
use Eelly\SDK\Pay\DTO\RechargeDTO;

/**
 * This class has been auto-generated by shadon compiler (2020-05-20 02:03:11).
 */
class Recharge
{
    use GetInstanceTrait;
    /**
     * 获取充值交易流水 记录.
     *
     * @param string $billNo 衣联交易号
     *
     * ### 返回数据说明
     *
     * 字段|类型|说明
     * ------------|-------|--------------
     * precId      |string |    充值交易ID，自增主键
     * paId        |string |    会员帐户ID
     * money       |string |    充值金额：单位为分
     * refundMoney |string |    已退款金额：单位为分
     * channel     |string |    充值渠道：0 线下 1 支付宝 2 微信钱包 3 QQ钱包 4 银联 5 移动支付
     * style       |string |    充值方式：0 未知 1 储蓄卡 2 信用卡 3 余额充值
     * bankId      |string |    充值银行ID：el_system->system_bank->bank_id
     * bankName    |string |    充值银行名称（冗余）
     * bankAccount |string |    充值帐号：支付宝账号/微信绑定open_id/QQ
     * billNo      |string |    衣联交易号
     * thirdNo     |string |    第三方交易号(支付宝/微信/银联)
     * status      |string |    处理状态：0 待处理 1 成功 2 处理中 3 失败
     * checkStatus |string |    对帐状态：0 未对帐 1 对帐成功 2 对帐中 3 对帐失败
     * remark      |string |    备注
     * adminRemark |string |    系统及管理备注，不需要给用户看的
     * handleTime  |string |    处理时间
     * createdTime |string |    添加时间
     * updateTime  |string |    修改时间
     *
     * @throws RechargeException
     *
     * @return RechargeDTO
     * @requestExample({"billNo":"1001"})
     * @returnExample({"precId":"1","paId":"1","money":"100","refundMoney":"0","channel":"1","style":"0","bankId":"111",
     *     "bankName":"支付宝","bankAccount":"支付宝","billNo":"17111047444f6cvA6a",
     *     "thirdNo":"","status":"0","checkStatus":"0","remark":"备注","adminRemark":"系统备注",
     *     "handleTime":"0","createdTime":"1510303156","updateTime":"2017-11-10 16:39:16"})
     *
     * @author 张泽强 <zhangzeqiang@eelly.net>
     *
     * @since  2017年11月10日
     */
    public static function getRecharge(string $billNo) : RechargeDTO
    {
        return Client20200520::requestJson('pay/recharge', 'getRecharge', ['billNo' => $billNo], true);
    }
    /**
     * 获取充值交易流水 记录.
     *
     * @param string $billNo 衣联交易号
     *
     * ### 返回数据说明
     *
     * 字段|类型|说明
     * ------------|-------|--------------
     * precId      |string |    充值交易ID，自增主键
     * paId        |string |    会员帐户ID
     * money       |string |    充值金额：单位为分
     * refundMoney |string |    已退款金额：单位为分
     * channel     |string |    充值渠道：0 线下 1 支付宝 2 微信钱包 3 QQ钱包 4 银联 5 移动支付
     * style       |string |    充值方式：0 未知 1 储蓄卡 2 信用卡 3 余额充值
     * bankId      |string |    充值银行ID：el_system->system_bank->bank_id
     * bankName    |string |    充值银行名称（冗余）
     * bankAccount |string |    充值帐号：支付宝账号/微信绑定open_id/QQ
     * billNo      |string |    衣联交易号
     * thirdNo     |string |    第三方交易号(支付宝/微信/银联)
     * status      |string |    处理状态：0 待处理 1 成功 2 处理中 3 失败
     * checkStatus |string |    对帐状态：0 未对帐 1 对帐成功 2 对帐中 3 对帐失败
     * remark      |string |    备注
     * adminRemark |string |    系统及管理备注，不需要给用户看的
     * handleTime  |string |    处理时间
     * createdTime |string |    添加时间
     * updateTime  |string |    修改时间
     *
     * @throws RechargeException
     *
     * @return RechargeDTO
     * @requestExample({"billNo":"1001"})
     * @returnExample({"precId":"1","paId":"1","money":"100","refundMoney":"0","channel":"1","style":"0","bankId":"111",
     *     "bankName":"支付宝","bankAccount":"支付宝","billNo":"17111047444f6cvA6a",
     *     "thirdNo":"","status":"0","checkStatus":"0","remark":"备注","adminRemark":"系统备注",
     *     "handleTime":"0","createdTime":"1510303156","updateTime":"2017-11-10 16:39:16"})
     *
     * @author 张泽强 <zhangzeqiang@eelly.net>
     *
     * @since  2017年11月10日
     */
    public static function getRechargeAsync(string $billNo)
    {
        return Client20200520::requestJson('pay/recharge', 'getRecharge', ['billNo' => $billNo], false);
    }
    /**
     * 添加充值交易流水 记录.
     *
     * @param array  $data
     * @param int    $data["paId"]        会员资金账户id
     * @param int    $data["userId"]      用户id
     * @param int    $data["money"]       充值金额
     * @param int    $data["channel"]     充值渠道：0 线下 1 支付宝 2 微信钱包 3 QQ钱包 4 银联 5 移动支付
     * @param int    $data["style"]       充值方式：0 未知 1 储蓄卡 2 信用卡 3 余额充值
     * @param int    $data["bankId"]      充值银行ID
     * @param string $data["bankName"]    充值银行名称
     * @param string $data["bankAccount"] 充值帐号：支付宝账号/微信绑定open_id/QQ
     * @param string $data["remark"]      备注
     * @param string $data["adminRemark"] 系统及管理备注
     *
     * @throws RechargeException
     *
     * @return int
     * @requestExample({"data":{"userId":148086,"paId":1,"money":100,"channel":1,"style":0,"bankId":111,"bankName":"银行名",
     *     "bankAccount":"银行账号",
     *     "remark":"","adminRemark":""}})
     * @returnExample(1)
     *
     * @author 张泽强 <zhangzeqiang@eelly.net>
     *
     * @since  2017年11月10日
     */
    public static function addRecharge(array $data) : int
    {
        return Client20200520::requestJson('pay/recharge', 'addRecharge', ['data' => $data], true);
    }
    /**
     * 添加充值交易流水 记录.
     *
     * @param array  $data
     * @param int    $data["paId"]        会员资金账户id
     * @param int    $data["userId"]      用户id
     * @param int    $data["money"]       充值金额
     * @param int    $data["channel"]     充值渠道：0 线下 1 支付宝 2 微信钱包 3 QQ钱包 4 银联 5 移动支付
     * @param int    $data["style"]       充值方式：0 未知 1 储蓄卡 2 信用卡 3 余额充值
     * @param int    $data["bankId"]      充值银行ID
     * @param string $data["bankName"]    充值银行名称
     * @param string $data["bankAccount"] 充值帐号：支付宝账号/微信绑定open_id/QQ
     * @param string $data["remark"]      备注
     * @param string $data["adminRemark"] 系统及管理备注
     *
     * @throws RechargeException
     *
     * @return int
     * @requestExample({"data":{"userId":148086,"paId":1,"money":100,"channel":1,"style":0,"bankId":111,"bankName":"银行名",
     *     "bankAccount":"银行账号",
     *     "remark":"","adminRemark":""}})
     * @returnExample(1)
     *
     * @author 张泽强 <zhangzeqiang@eelly.net>
     *
     * @since  2017年11月10日
     */
    public static function addRechargeAsync(array $data)
    {
        return Client20200520::requestJson('pay/recharge', 'addRecharge', ['data' => $data], false);
    }
    /**
     * 更新充值交易流水.
     *
     * @param int    $rechargeId          充值交易ID
     * @param array  $data
     * @param string $data['thirdNo']     第三方交易号
     * @param int    $data['status']      处理状态：0 待处理 1 成功 2 处理中 3 失败
     * @param int    $data['checkStatus'] 对帐状态：0 未对帐 1 对帐成功 2 对帐中 3 对帐失败
     * @param string $data['remark']      备注
     *
     * @throws RechargeException
     *
     * @return bool
     * @requestExample({"rechargeId":1,"data":{"thirdNo":1,"status":1,"checkStatus":1,"remark":"helloWorld"}})
     * @returnExample(true)
     *
     * @author 张泽强<zhangzeqiang@eelly.net>
     *
     * @since  2017年11月15日
     * @internal
     */
    public static function updateRecharge(int $rechargeId, array $data) : bool
    {
        return Client20200520::requestJson('pay/recharge', 'updateRecharge', ['rechargeId' => $rechargeId, 'data' => $data], true);
    }
    /**
     * 更新充值交易流水.
     *
     * @param int    $rechargeId          充值交易ID
     * @param array  $data
     * @param string $data['thirdNo']     第三方交易号
     * @param int    $data['status']      处理状态：0 待处理 1 成功 2 处理中 3 失败
     * @param int    $data['checkStatus'] 对帐状态：0 未对帐 1 对帐成功 2 对帐中 3 对帐失败
     * @param string $data['remark']      备注
     *
     * @throws RechargeException
     *
     * @return bool
     * @requestExample({"rechargeId":1,"data":{"thirdNo":1,"status":1,"checkStatus":1,"remark":"helloWorld"}})
     * @returnExample(true)
     *
     * @author 张泽强<zhangzeqiang@eelly.net>
     *
     * @since  2017年11月15日
     * @internal
     */
    public static function updateRechargeAsync(int $rechargeId, array $data)
    {
        return Client20200520::requestJson('pay/recharge', 'updateRecharge', ['rechargeId' => $rechargeId, 'data' => $data], false);
    }
    /**
     * @author eellytools<localhost.shell@gmail.com>
     */
    public static function listRechargePage(array $condition = [], int $currentPage = 1, int $limit = 10) : array
    {
        return Client20200520::requestJson('pay/recharge', 'listRechargePage', ['condition' => $condition, 'currentPage' => $currentPage, 'limit' => $limit], true);
    }
    /**
     * @author eellytools<localhost.shell@gmail.com>
     */
    public static function listRechargePageAsync(array $condition = [], int $currentPage = 1, int $limit = 10)
    {
        return Client20200520::requestJson('pay/recharge', 'listRechargePage', ['condition' => $condition, 'currentPage' => $currentPage, 'limit' => $limit], false);
    }
    /**
     * 充值交易入口.
     *
     * @param array  $data
     * @param int    $data['userId']      用户id
     * @param int    $data['paId']        会员账户资金id
     * @param int    $data['money']       充值金额
     * @param string $data['subject']     支付请求标题
     * @param string $data['channelType'] 交易类型： payment, recharge, withdraw
     * @param int    $data['channel']     充值渠道：0 线下 1 支付宝 2 微信钱包 3 QQ钱包 4 银联 5 移动支付
     * @param int    $data['style']       充值方式：0 未知 1 储蓄卡 2 信用卡 3 余额充值
     * @param int    $data['bankId']      充值银行ID
     * @param string $data['bankName']    充值银行名称
     * @param string $data['billNo']      衣联交易号(可为空)
     * @param string $data['remark']      备注(可为空)
     * @param string $data['adminRemark'] 系统备注(可为空)
     * @param string $data['bankAccount'] 充值帐号：支付宝账号/微信绑定open_id/QQ
     * @param string $data['platform']    平台的支付网关(tradeLogic->$requestPay的键名)
     * @param string $data['account']     衣联财务账号标识,
     *                                    值为: 126mail.pc, 126mail.wap, eellyMail.pc, eellyMail.app,
     *                                    union.pc,
     *                                    eelly.wap, eellyBuyer.wap, order.app, eelly.app, eellySeller.app, storeUnion.wap
     * @param int    $data['itemId']      关联id：如订单，增值服务,不存在则传0
     * @param int    $data['ppId']        支付交易流水id, 没有则传0
     *
     * @throws RechargeException
     *
     * @return array
     * @requestExample({"userId":"148086","paId":"1","money":"10","subject":"游戏王充值卡10元券","channelType":"recharge","channel":"1","style":0,"bankId":184,"bankName":"支付宝","billNo":"","remark":"游戏王充值卡10元券","adminRemark":"","bankAccount":"13711221122","platform":"ALIPAY_WAP","account":"126mail.pc"})
     * @returnExample({
     *     "platform": 'alipayWap',
     *     'data':{
     *          'platform=alipayWap:url地址','platform=alipayWap:url地址',
     *          'platform=alipayApp:返回是订单ID'
     *     }
     * })
     *
     * @author 张泽强 <zhangzeqiang@eelly.net>
     *
     * @since  2017年11月14日
     */
    public static function goRecharge(array $data) : array
    {
        return Client20200520::requestJson('pay/recharge', 'goRecharge', ['data' => $data], true);
    }
    /**
     * 充值交易入口.
     *
     * @param array  $data
     * @param int    $data['userId']      用户id
     * @param int    $data['paId']        会员账户资金id
     * @param int    $data['money']       充值金额
     * @param string $data['subject']     支付请求标题
     * @param string $data['channelType'] 交易类型： payment, recharge, withdraw
     * @param int    $data['channel']     充值渠道：0 线下 1 支付宝 2 微信钱包 3 QQ钱包 4 银联 5 移动支付
     * @param int    $data['style']       充值方式：0 未知 1 储蓄卡 2 信用卡 3 余额充值
     * @param int    $data['bankId']      充值银行ID
     * @param string $data['bankName']    充值银行名称
     * @param string $data['billNo']      衣联交易号(可为空)
     * @param string $data['remark']      备注(可为空)
     * @param string $data['adminRemark'] 系统备注(可为空)
     * @param string $data['bankAccount'] 充值帐号：支付宝账号/微信绑定open_id/QQ
     * @param string $data['platform']    平台的支付网关(tradeLogic->$requestPay的键名)
     * @param string $data['account']     衣联财务账号标识,
     *                                    值为: 126mail.pc, 126mail.wap, eellyMail.pc, eellyMail.app,
     *                                    union.pc,
     *                                    eelly.wap, eellyBuyer.wap, order.app, eelly.app, eellySeller.app, storeUnion.wap
     * @param int    $data['itemId']      关联id：如订单，增值服务,不存在则传0
     * @param int    $data['ppId']        支付交易流水id, 没有则传0
     *
     * @throws RechargeException
     *
     * @return array
     * @requestExample({"userId":"148086","paId":"1","money":"10","subject":"游戏王充值卡10元券","channelType":"recharge","channel":"1","style":0,"bankId":184,"bankName":"支付宝","billNo":"","remark":"游戏王充值卡10元券","adminRemark":"","bankAccount":"13711221122","platform":"ALIPAY_WAP","account":"126mail.pc"})
     * @returnExample({
     *     "platform": 'alipayWap',
     *     'data':{
     *          'platform=alipayWap:url地址','platform=alipayWap:url地址',
     *          'platform=alipayApp:返回是订单ID'
     *     }
     * })
     *
     * @author 张泽强 <zhangzeqiang@eelly.net>
     *
     * @since  2017年11月14日
     */
    public static function goRechargeAsync(array $data)
    {
        return Client20200520::requestJson('pay/recharge', 'goRecharge', ['data' => $data], false);
    }
    /**
     * 交易完成后处理部分.
     *
     * @param RechargeDTO $rechargeDto
     * @param array       $data
     * @param string      $data['thirdNo']    第三方交易号
     * @param int         $data['recordType'] 操作类型：1 充值 2 提现 3 消费 4 结算 5 退款 6 诚保冻结 7 诚保解冻
     *
     * @throws RechargeException
     *
     * @requestExample()
     * @returnExample(true)
     *
     * @return bool
     *
     * @author 张泽强 <zhangzeqiang@eelly.net>
     *
     * @since  2017年11月14日
     * @internal
     */
    public static function updateGoRecharge(RechargeDTO $rechargeDto, array $data) : bool
    {
        return Client20200520::requestJson('pay/recharge', 'updateGoRecharge', ['rechargeDto' => $rechargeDto, 'data' => $data], true);
    }
    /**
     * 交易完成后处理部分.
     *
     * @param RechargeDTO $rechargeDto
     * @param array       $data
     * @param string      $data['thirdNo']    第三方交易号
     * @param int         $data['recordType'] 操作类型：1 充值 2 提现 3 消费 4 结算 5 退款 6 诚保冻结 7 诚保解冻
     *
     * @throws RechargeException
     *
     * @requestExample()
     * @returnExample(true)
     *
     * @return bool
     *
     * @author 张泽强 <zhangzeqiang@eelly.net>
     *
     * @since  2017年11月14日
     * @internal
     */
    public static function updateGoRechargeAsync(RechargeDTO $rechargeDto, array $data)
    {
        return Client20200520::requestJson('pay/recharge', 'updateGoRecharge', ['rechargeDto' => $rechargeDto, 'data' => $data], false);
    }
    /**
     * 根据交易流水id，获得对应的凭证金额.
     *
     * @param int $prId 交易流水id
     *
     * @return int
     *
     * @requestExample({"prId":1})
     * @returnExample(10)
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     *
     * @since 2017-11-17
     */
    public static function getVoucherMoneyByPrId(int $prId) : int
    {
        return Client20200520::requestJson('pay/recharge', 'getVoucherMoneyByPrId', ['prId' => $prId], true);
    }
    /**
     * 根据交易流水id，获得对应的凭证金额.
     *
     * @param int $prId 交易流水id
     *
     * @return int
     *
     * @requestExample({"prId":1})
     * @returnExample(10)
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     *
     * @since 2017-11-17
     */
    public static function getVoucherMoneyByPrIdAsync(int $prId)
    {
        return Client20200520::requestJson('pay/recharge', 'getVoucherMoneyByPrId', ['prId' => $prId], false);
    }
    /**
     * 根据传过来的交易号，返回对应的信息.
     *
     * @param string $billNo 交易号
     *
     * @throws RechargeException
     *
     * @return RechargeDTO
     *
     * @requestExample({"billNo":"1711214a41356cvAbc"})
     * @returnExample({"precId":13,"paId":1,"money":100,"refundMoney":100,"channel":1,"style":0,"bankId":184,"bankName":"支付宝","bankAccount":"13711221122","billNo":"1711214a41356cvAbc","thirdNo":"","status":1,"checkStatus":"0","remark":"","adminRemark":"","handleTime":"","createdTime":""})
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     *
     * @since 2017-11-22
     */
    public static function getRecordInfoByBillNo(string $billNo) : RechargeDTO
    {
        return Client20200520::requestJson('pay/recharge', 'getRecordInfoByBillNo', ['billNo' => $billNo], true);
    }
    /**
     * 根据传过来的交易号，返回对应的信息.
     *
     * @param string $billNo 交易号
     *
     * @throws RechargeException
     *
     * @return RechargeDTO
     *
     * @requestExample({"billNo":"1711214a41356cvAbc"})
     * @returnExample({"precId":13,"paId":1,"money":100,"refundMoney":100,"channel":1,"style":0,"bankId":184,"bankName":"支付宝","bankAccount":"13711221122","billNo":"1711214a41356cvAbc","thirdNo":"","status":1,"checkStatus":"0","remark":"","adminRemark":"","handleTime":"","createdTime":""})
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     *
     * @since 2017-11-22
     */
    public static function getRecordInfoByBillNoAsync(string $billNo)
    {
        return Client20200520::requestJson('pay/recharge', 'getRecordInfoByBillNo', ['billNo' => $billNo], false);
    }
    /**
     * 根据precId 获取 billNo
     * 
     * @return int 充值交易ID
     * 
     * @author wechan
     * @since 2018年10月22日
     */
    public static function getBillNoByPrecId(int $precId) : string
    {
        return Client20200520::requestJson('pay/recharge', 'getBillNoByPrecId', ['precId' => $precId], true);
    }
    /**
     * 根据precId 获取 billNo
     * 
     * @return int 充值交易ID
     * 
     * @author wechan
     * @since 2018年10月22日
     */
    public static function getBillNoByPrecIdAsync(int $precId)
    {
        return Client20200520::requestJson('pay/recharge', 'getBillNoByPrecId', ['precId' => $precId], false);
    }
    /**
     * 根据precId 获取一条充值记录
     * 
     * @return int 充值交易ID
     * 
     * @author wechan
     * @since 2018年10月22日
     */
    public static function getRecordByPrecId(int $precId) : array
    {
        return Client20200520::requestJson('pay/recharge', 'getRecordByPrecId', ['precId' => $precId], true);
    }
    /**
     * 根据precId 获取一条充值记录
     * 
     * @return int 充值交易ID
     * 
     * @author wechan
     * @since 2018年10月22日
     */
    public static function getRecordByPrecIdAsync(int $precId)
    {
        return Client20200520::requestJson('pay/recharge', 'getRecordByPrecId', ['precId' => $precId], false);
    }
    /**
     * 根据传过来的条件返回对应的记录
     *
     * @param string $conditions 搜索条件
     * @param array $bind  绑定参数
     * @param string $field 字段名
     * @return array
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2018.10.22
     */
    public static function listRechargeInfoByConditions(string $conditions, array $bind, string $field = 'base') : array
    {
        return Client20200520::requestJson('pay/recharge', 'listRechargeInfoByConditions', ['conditions' => $conditions, 'bind' => $bind, 'field' => $field], true);
    }
    /**
     * 根据传过来的条件返回对应的记录
     *
     * @param string $conditions 搜索条件
     * @param array $bind  绑定参数
     * @param string $field 字段名
     * @return array
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2018.10.22
     */
    public static function listRechargeInfoByConditionsAsync(string $conditions, array $bind, string $field = 'base')
    {
        return Client20200520::requestJson('pay/recharge', 'listRechargeInfoByConditions', ['conditions' => $conditions, 'bind' => $bind, 'field' => $field], false);
    }
    /**
     * 根据开始时间和结束时间获取 状态是未支付的充值记录
     *
     * @param int $startTime 搜索条件
     * @param int $endTime  绑定参数
     * @param string $status 字段名
     * @return array
     *
     * @author wechan
     * @since 2018年11月14日
     */
    public static function getPayRechargeRecordByTime(int $startTime, int $endTime, int $status = 0) : array
    {
        return Client20200520::requestJson('pay/recharge', 'getPayRechargeRecordByTime', ['startTime' => $startTime, 'endTime' => $endTime, 'status' => $status], true);
    }
    /**
     * 根据开始时间和结束时间获取 状态是未支付的充值记录
     *
     * @param int $startTime 搜索条件
     * @param int $endTime  绑定参数
     * @param string $status 字段名
     * @return array
     *
     * @author wechan
     * @since 2018年11月14日
     */
    public static function getPayRechargeRecordByTimeAsync(int $startTime, int $endTime, int $status = 0)
    {
        return Client20200520::requestJson('pay/recharge', 'getPayRechargeRecordByTime', ['startTime' => $startTime, 'endTime' => $endTime, 'status' => $status], false);
    }
    /**
     * 根据开始时间和结束时间获取 状态是未支付的充值记录
     *
     * @param int $startTime 搜索条件
     * @param int $endTime  绑定参数
     * @param string $status 字段名
     * @return array
     *
     * @author wechan
     * @since 2018年11月14日
     */
    public static function getHasRechargeNoPayByTime(int $startTime, int $endTime, int $status = 1) : array
    {
        return Client20200520::requestJson('pay/recharge', 'getHasRechargeNoPayByTime', ['startTime' => $startTime, 'endTime' => $endTime, 'status' => $status], true);
    }
    /**
     * 根据开始时间和结束时间获取 状态是未支付的充值记录
     *
     * @param int $startTime 搜索条件
     * @param int $endTime  绑定参数
     * @param string $status 字段名
     * @return array
     *
     * @author wechan
     * @since 2018年11月14日
     */
    public static function getHasRechargeNoPayByTimeAsync(int $startTime, int $endTime, int $status = 1)
    {
        return Client20200520::requestJson('pay/recharge', 'getHasRechargeNoPayByTime', ['startTime' => $startTime, 'endTime' => $endTime, 'status' => $status], false);
    }
    /**
     * 获取后台充值列表数据
     *
     * @param string $condition 查询条件
     * @param array $binds 绑定参数
     * @param int $page 页码
     * @param int $limit 每页显示多少数量
     * @return array
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2018.12.06
     * @internal
     */
    public static function listManageRecharge(string $condition, array $binds = [], int $page = 1, int $limit = 20) : array
    {
        return Client20200520::requestJson('pay/recharge', 'listManageRecharge', ['condition' => $condition, 'binds' => $binds, 'page' => $page, 'limit' => $limit], true);
    }
    /**
     * 获取后台充值列表数据
     *
     * @param string $condition 查询条件
     * @param array $binds 绑定参数
     * @param int $page 页码
     * @param int $limit 每页显示多少数量
     * @return array
     *
     * @author zhangyingdi<zhangyingdi@eelly.net>
     * @since 2018.12.06
     * @internal
     */
    public static function listManageRechargeAsync(string $condition, array $binds = [], int $page = 1, int $limit = 20)
    {
        return Client20200520::requestJson('pay/recharge', 'listManageRecharge', ['condition' => $condition, 'binds' => $binds, 'page' => $page, 'limit' => $limit], false);
    }
    /**
     * 根据充值用途对象id获取退款账号
     * 
     * @param array $itemIds 充值用途对象ID
     * @param int $type 充值用途类型：1 订单支付 2 购买服务 3 诚信保障 4 一件代发保证金
     * 
     * @aurhor wechan/zhangyangxun
     * @since 2019年03月27日
     */
    public static function getToAccountByItemIds(array $itemIds, int $type) : array
    {
        return Client20200520::requestJson('pay/recharge', 'getToAccountByItemIds', ['itemIds' => $itemIds, 'type' => $type], true);
    }
    /**
     * 根据充值用途对象id获取退款账号
     * 
     * @param array $itemIds 充值用途对象ID
     * @param int $type 充值用途类型：1 订单支付 2 购买服务 3 诚信保障 4 一件代发保证金
     * 
     * @aurhor wechan/zhangyangxun
     * @since 2019年03月27日
     */
    public static function getToAccountByItemIdsAsync(array $itemIds, int $type)
    {
        return Client20200520::requestJson('pay/recharge', 'getToAccountByItemIds', ['itemIds' => $itemIds, 'type' => $type], false);
    }

    /**
     * 通过itemId 和 充值用途类型 获取一条已支付的充值信息
     *
     * @param int $itemId  充值用途对象ID
     * @param int $type 充值用途类型：1 订单支付 2 购买服务 3 诚信保障 4 一件代发保证金
     *
     * @return array
     *
     * @author wechan
     * @since 2020年08月27日
     */
    public static function getAlreadyPayRelMoney(int $itemId, int $type):array
    {
        return Client20200520::requestJson('pay/recharge', __FUNCTION__, ['itemId' => $itemId, 'type' => $type], true);
    }
}
