<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Goods\Api;

use Eelly\DTO\UidDTO;
use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class Tiktok
{
    /**
     * 商品一键同步
     *
     * @param UidDTO $user 登录用户信息
     *
     * @author wechan
     * @since 2021年04月02日
     */
    public function syncTiktokGoods(UidDTO $user = null):array
    {
        return EellyClient::requestJson('goods/tiktok', __FUNCTION__, []);
    }

    /**
     * 商品一键同步
     *
     * @returnExample({"result":1})
     *
     * @param int $userId 用户id
     * @param int $shopId 抖音店铺id
     *
     * @author wechan
     * @since 2021年04月02日
     */
    public function syncTiktokGoodsAsync($userId, $shopId):array
    {
        return EellyClient::requestJson('goods/tiktok', __FUNCTION__, [
            'userId' => $userId,
            'shopId' => $shopId
        ]);
    }

    /**
     * 商品推送到抖店平台
     *
     * @param array $productIds 产品id
     * @param UidDTO $user 登录用户信息
     * @return bool
     *
     * @author wechan
     * @since 2021年04月06日
     */
    public function pushTiktokGoods(array $productIds, UidDTO $user = null):array
    {
        return EellyClient::requestJson('goods/tiktok', __FUNCTION__, [
            'productIds' => $productIds
        ]);
    }

    /**
     * 设置抖音商品下架
     *
     * @param array $productIds 商品id
     * @param UidDTO $user 登录用户信息
     * @return bool
     *
     * @author wechan
     * @since 2021年04月08日
     */
    public function productSetOffLine(array $productIds, UidDTO $user = null):array
    {
        return EellyClient::requestJson('goods/tiktok', __FUNCTION__, [
            'productIds' => $productIds
        ]);
    }

    /**
     * 设置抖音商品上架
     *
     * @param array $productIds 商品id
     * @param UidDTO $user 登录用户信息
     * @return bool
     *
     * @author wechan
     * @since 2021年04月08日
     */
    public function productSetOnLine(array $productIds, UidDTO $user = null):array
    {
        return EellyClient::requestJson('goods/tiktok', __FUNCTION__, [
            'productIds' => $productIds
        ]);
    }

    /**
     * 获取抖店商品列表数据
     *
     * @param array     $params                搜索参数
     * @param string    $params.tabId          tabId
     * @param string    $params.goodsName      商品名称
     * @param string    $params.productId      商品ID
     * @param string    $params.productCode    商品编码
     * @param string    $params.tiktokCate     商品分类
     * @param int       $params.payType        支付方式 0 全部 1 在线支付 2 货到付款 3 在线支付&货到付款
     * @param int       $params.presellType    发货方式 0 全部 1 现货发货 2 预售发货 3 阶梯发货
     * @param int       $params.isEellyGoods   商品来源 0 全部 1 抖音同步 2 衣联上传
     * @param string    $params.sortKey        排序字段，不排序此字段值传空字符串 price 抖音价格 stock 库存 createdTime 创建时间
     * @param int       $params.asc            0 降序 1 升序
     * @param int       $page                  页数
     * @param int       $limit                 每页行数
     * @param UidDTO    $user
     * @return array
     *
     * @author chenyuhua
     * @since 2021.03.29
     */
    public function getTiktokGoodsList(array $params, int $page = 1, int $limit = 20, UidDTO $user = null): array
    {
        return EellyClient::requestJson('goods/tiktok', __FUNCTION__, [
            'params' => $params,
            'page'  => $page,
            'limit' => $limit,
        ]);
    }

    /**
     * 获取抖店商品列表Tab
     *
     * @param UidDTO    $user
     * @return array[]
     *
     * @author chenyuhua
     * @since 2021.03.29
     */
    public function getTikTokGoodsTabs(UidDTO $user = null): array
    {
        return EellyClient::requestJson('goods/tiktok', __FUNCTION__);
    }

    /**
     * 获取抖店商品SKU
     *
     * @param int $productId    抖音商品ID
     * @param int $type         操作类型 0 修改商品编码 1 修改商品价格 2 修改商品库存
     * @param UidDTO    $user
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.07
     */
    public function getTiktokGoodsSku(int $productId, int $type, UidDTO $user = null): array
    {
        return EellyClient::requestJson('goods/tiktok', __FUNCTION__, [
            'productId' => $productId,
            'type'  => $type,
        ]);
    }

    /**
     * 修改抖音商品数据(sku相关)
     *
     * @param int       $productId      抖音商品ID
     * @param int       $type           操作类型 0 修改商品编码 1 修改商品价格 2 修改商品库存
     * @param array     $code           商品SKU列表
     * @param UidDTO    $user
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.02
     * @internal
     */
    public function updateProductSku(int $productId, int $type, array $code, UidDTO $user = null): array
    {
        return EellyClient::requestJson('goods/tiktok', __FUNCTION__, [
            'productId' => $productId,
            'type'  => $type,
            'code'  => $code,
        ]);
    }

    /**
     * 修改抖音商品数据
     *
     * @param int       $productId      抖音商品ID
     * @param string    $type           操作类型 name 商品标题
     * @param string    $value          值
     * @param UidDTO    $user
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.02
     */
    public function updateProduct(int $productId, string $type, string $value, UidDTO $user = null): array
    {
        return EellyClient::requestJson('goods/tiktok', __FUNCTION__, [
            'productId' => $productId,
            'type'  => $type,
            'value'  => $value,
        ]);
    }

    /**
     * 批量删除抖音商品
     *
     * @param array $productIds 抖店商品IDs
     * @param UidDTO $user 登录用户信息
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.13
     */
    public function deleteTiktokGoods(array $productIds, UidDTO $user = null): array
    {
        return EellyClient::requestJson('goods/tiktok', __FUNCTION__, ['productIds' => $productIds]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
