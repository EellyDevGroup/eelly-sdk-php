<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Finance\Api;

use Eelly\SDK\EellyClient;
use Eelly\DTO\UidDTO;

/**
 * 金融问卷页面
 *
 * @author chenyuhua
 */
class Finance
{

    /**
     * 贷款金融表单提交.
     *
     * @param string $name 用户名
     * @param string $phone 手机号码
     * @param int $borrowAmount 贷款额度
     * @param int $job 职业
     * @param int $property 资产
     *
     * @return array 结果
     *
     * @author twb<1174865138@qq.com>
     * @since 2021-05-12T15:12:20+0800
     */
    public function addFinanceQuestionnaire(string $name, string $phone, int $borrowAmount, int $job, int $property): array
    {
        return EellyClient::requestJson('finance/finance', __FUNCTION__, ['name' => $name, 'phone' => $phone, 'borrowAmount' => $borrowAmount, 'job' => $job, 'property' => $property]);
    }

    /**
     * 获取后台金融需求登记管理
     *
     * @param array $conditions 查询条件
     * @param int $page   页码
     * @param int $limit  每页显示数量
     * 
     * @return array
     * 
     * @author twb<1174865138@qq.com>
     * @since 2021-05-12T14:50:57+0800
     *
     */
    public function adminListFinanceQuestionnaire(array $conditions, int $page = 1, int $limit = 100): array
    {
        return EellyClient::requestJson('finance/finance', __FUNCTION__, ['conditions' => $conditions, 'page' => $page, 'limit' => $limit]);
    }


    /**
     * 后台金融需求登记管理列表 跟进
     *
     * @param array $fqIds id数组
     * @param string $userId 操作管理员id
     * @param string $userName 操作管理员name
     * 
     * @return array
     * 
     * @author twb<1174865138@qq.com>
     * @since 2021-05-12T14:50:57+0800
     *
     */
    public function listSetFollow(array $fqIds, string $userId, string $userName): array
    {
        return EellyClient::requestJson('finance/finance', __FUNCTION__, ['fqIds' => $fqIds, 'userId' => $userId, 'userName' => $userName]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
