<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Advert\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class AdvertPlan
{
    public static function getListPage(array $condition = [], int $page = 1, int $limit = 20, string $fieldScope = 'base'): array
    {
        return EellyClient::requestJson('advert/advertPlan', __FUNCTION__, [
            'condition' => $condition,
            'page' => $page,
            'limit' => $limit,
            'fieldScope' => $fieldScope
        ]);
    }

    public static function getOne(array $condition = [], string $fieldScope = 'base'): array
    {
        return EellyClient::requestJson('advert/advertPlan', __FUNCTION__, [
            'condition' => $condition,
            'fieldScope' => $fieldScope
        ]);
    }

    public static function drop(array $ids): bool
    {
        return EellyClient::requestJson('advert/advertPlan', __FUNCTION__, [
            'ids' => $ids,
        ]);
    }

    public static function updateOrNew(array $data): bool
    {
        return EellyClient::requestJson('advert/advertPlan', __FUNCTION__, [
            'data' => $data,
        ]);
    }

    public static function quickUpdate(int $planId, array $data): bool
    {
        return EellyClient::requestJson('advert/advertPlan', __FUNCTION__, [
            'planId' => $planId,
            'data' => $data,
        ]);
    }
}