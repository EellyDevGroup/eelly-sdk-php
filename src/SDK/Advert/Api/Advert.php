<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Advert\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class Advert
{
    public static function getListPage(array $condition = [], int $page = 1, int $limit = 20): array
    {
        return EellyClient::requestJson('advert/advert', __FUNCTION__, [
            'condition' => $condition,
            'page' => $page,
            'limit' => $limit
        ]);
    }

    public static function drop(array $ids): bool
    {
        return EellyClient::requestJson('advert/advert', __FUNCTION__, [
            'ids' => $ids,
        ]);
    }

    public static function updateOrNew(array $data): bool
    {
        return EellyClient::requestJson('advert/advert', __FUNCTION__, [
            'data' => $data,
        ]);
    }

    public static function deleteAd(int $apId): bool
    {
        return EellyClient::requestJson('advert/advert', __FUNCTION__, [
            'apId' => $apId,
        ]);
    }

    public static function upSort(int $apId, int $sort): bool
    {
        return EellyClient::requestJson('advert/advert', __FUNCTION__, [
            'apId' => $apId,
            'sort' => $sort
        ]);
    }

    public static function upStatus(int $apId, int $status): string
    {
        return EellyClient::requestJson('advert/advert', __FUNCTION__, [
            'apId' => $apId,
            'status' => $status
        ]);
    }

    /**
     * 获取短视频广告位的广告
     *
     * @param int $spaceType 广告位类型
     * @param int $limit 获取广告数量
     * @param int $offset 偏移量
     * @return array
     *
     * @author chenyuhua
     * @since 2020.09.08
     */
    public static function shortVideoAd(int $spaceType, int $limit, int $offset): array
    {
        return EellyClient::requestJson('advert/advert', __FUNCTION__, [
            'spaceType' => $spaceType,
            'limit' => $limit,
            'offset'=> $offset,
        ]);
    }

    /**
     * 获取商品广告
     *
     * @param int $map          广告位映射
     * @param int $spaceType    映射对应键值
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public static function getAd(string $map, int $spaceType, int $limit = 20, int $offset = 0): array
    {
        return EellyClient::requestJson('advert/advert', __FUNCTION__, [
            'map'       => $map,
            'spaceType' => $spaceType,
            'limit'     => $limit,
            'offset'    => $offset,
        ]);
    }
}