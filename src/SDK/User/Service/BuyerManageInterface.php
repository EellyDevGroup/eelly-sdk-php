<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\User\Service;

/**
 * 买家跟进后台管理.
 */
interface BuyerManageInterface
{
    public function getAdminListPage(array $condition = [], int $page = 1, int $limit = 10): array;

    public function addBuyerManage(array $data): bool;

    public function updateBuyerManage(array $data): bool;

    public function moveBuyerManage(array $manageData, array $newAdmin, array $handleAdmin): bool;

    public function getBuyerManageSummary(array $condition = []): array;
}