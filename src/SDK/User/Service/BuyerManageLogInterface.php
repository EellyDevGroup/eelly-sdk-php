<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\User\Service;

/**
 * 买家跟进记录后台管理.
 */
interface BuyerManageLogInterface
{
    public function addManageLog(array $data): bool;

    public function getAdminListPage(array $condition = [], int $page = 1, int $limit = 20): array;
}