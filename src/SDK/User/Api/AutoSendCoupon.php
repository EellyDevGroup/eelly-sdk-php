<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\User\Api;

use Eelly\SDK\EellyClient;

class AutoSendCoupon
{
    /**
     * 发送卡券
     *
     * @param array $data 数据
     * @return boolean
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.12.9
     */
    public static function sendCoupon(array $data): bool
    {
        return EellyClient::requestJson('user/autoSendCoupon', __FUNCTION__, ['data' => $data]);
    }

    /**
     * 网络端发送卡券
     *
     * @param array $data 数据
     * @return boolean
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.12.12
     */
    public static function httpSendCoupon(array $data): bool
    {
        return EellyClient::requestJson('user/autoSendCoupon', __FUNCTION__, ['data' => $data]);
    }

    /**
     * 获取用户微信卡券
     *
     * @param integer $userId 用户id
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.12.12
     */
    public function getWechatCoupon(int $userId): array
    {
        return EellyClient::requestJson('user/autoSendCoupon', __FUNCTION__, ['userId' => $userId]);
    }

    /**
     * 获取用户微信失效卡券
     *
     * @param integer $userId 用户id
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2019.12.13
     */
    public function getWechatInvalidCoupon(int $userId): array
    {
        return EellyClient::requestJson('user/autoSendCoupon', __FUNCTION__, ['userId' => $userId]);
    }

     /**
     * 优惠券列表
     *
     * @param string $name 优惠券名称
     * @param integer $startTime 开始时间
     * @param integer $endTime 结束时间
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2020.4.27
     */
    public static function wechatCouponList(string $name = '', int $startTime = 0, int $endTime = 0): array
    {
        return EellyClient::requestJson('user/autoSendCoupon', __FUNCTION__, [
            'name' => $name,
            'startTime' => $startTime,
            'endTime' => $endTime
        ]);
    }

    /**
     * 添加微信卡券
     *
     * @param string $couponStockId 卡券批次号
     * @param integer $useDayLimit 领取后有效天数：0 不限制
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2020.4.27
     */
    public static function addWechatCoupon(string $wechatStockid, int $useDayLimit = 0): array
    {
        return EellyClient::requestJson('user/autoSendCoupon', __FUNCTION__, [
            'wechatStockid' => $wechatStockid,
            'useDayLimit' => $useDayLimit,
        ]);
    }

    /**
     * 发送前校验
     * 
     * @param string $wechatStockid 卡券批次号
     * @param array $users 批量用户
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2020.4.27
     */
    public function checkSendBefore(string $wechatStockid, array $users): array
    {
        return EellyClient::requestJson('user/autoSendCoupon', __FUNCTION__, [
            'wechatStockid' => $wechatStockid,
            'users' => $users,
        ]);
    }

    /**
     * 批量发送微信卡券
     * 
     * @param string $wechatStockid 卡券批次号
     * @param array $users 批量用户
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2020.4.27
     */
    public static function batchSendCoupon(string $wechatStockid, array $users): array
    {
        return EellyClient::requestJson('user/autoSendCoupon', __FUNCTION__, [
            'wechatStockid' => $wechatStockid,
            'users' => $users,
        ]);
    }

    /**
     * 发送记录
     * 
     * @param integer $page 页码
     * @param integer $limit 每页限制
     * @param string $wechatStockid 卡券批次号
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2020.4.27
     */
    public static function sendCouponHistory(int $page, int $limit, string $wechatStockid): array
    {
        return EellyClient::requestJson('user/autoSendCoupon', __FUNCTION__, [
            'page' => $page, 
            'limit' => $limit,
            'wechatStockid' => $wechatStockid,
        ]);
    }
}
