<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\User\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\User\Service\BuyerManageLogInterface;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class BuyerManageLog implements BuyerManageLogInterface
{
    
    public function addManageLog(array $data): bool
    {
        return EellyClient::request('user/buyerManageLog', 'addManageLog', true, $data);
    }

    
    public function addManageLogAsync(array $data)
    {
        return EellyClient::request('user/buyerManageLog', 'addManageLog', false, $data);
    }

    
    public function getAdminListPage(array $condition = [], int $page = 1, int $limit = 20): array
    {
        return EellyClient::request('user/buyerManageLog', 'getAdminListPage', true, $condition, $page, $limit);
    }

    
    public function getAdminListPageAsync(array $condition = [], int $page = 1, int $limit = 20)
    {
        return EellyClient::request('user/buyerManageLog', 'getAdminListPage', false, $condition, $page, $limit);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}