<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\User\Api;

use Eelly\DTO\UidDTO;
use Eelly\SDK\EellyClient;

/**
 * @author shadonTools<localhost.shell@gmail.com>
 */
class FansGrade
{
    public function listFansGrade(string $order = 'grade_value ASC'): array
    {
        return EellyClient::request('user/fansGrade', __FUNCTION__, true, $order);
    }

    public function saveFansGrade(array $data): bool
    {
        return EellyClient::request('user/fansGrade', __FUNCTION__, true, $data);
    }

    public function getStoreFansGradeData(int $liveId, UidDTO $uidDTO = null):array
    {
        return EellyClient::request('user/fansGrade', __FUNCTION__, true, $liveId, $uidDTO);
    }

    public function getLiveFansGrade(int $storeId, int $userId):int
    {
        return EellyClient::request('user/fansGrade', __FUNCTION__, true, $storeId, $userId);
    }

    public function getFansGrade(int $storeId, int $userId):array
    {
        return EellyClient::request('user/fansGrade', __FUNCTION__, true, $storeId, $userId);
    }

    public function listLiveFansGrade(array $storeIds, int $userId):array
    {
        return EellyClient::request('user/fansGrade', __FUNCTION__, true, $storeIds, $userId);
    }
    public function listLiveStoreFansGrade(int $storeId, array $userIds):array
    {
        return EellyClient::requestJson('user/fansGrade', __FUNCTION__, ['storeId' => $storeId, 'userIds' => $userIds]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
