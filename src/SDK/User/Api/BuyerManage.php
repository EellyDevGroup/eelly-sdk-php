<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\User\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\User\Service\BuyerManageInterface;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class BuyerManage implements BuyerManageInterface
{
    
    public function getAdminListPage(array $condition = [], int $page = 1, int $limit = 10): array
    {
        return EellyClient::request('user/buyerManage', 'getAdminListPage', true, $condition, $page, $limit);
    }

    
    public function getAdminListPageAsync(array $condition = [], int $page = 1, int $limit = 10)
    {
        return EellyClient::request('user/buyerManage', 'getAdminListPage', false, $condition, $page, $limit);
    }

    
    public function addBuyerManage(array $data): bool
    {
        return EellyClient::request('user/buyerManage', 'addBuyerManage', true, $data);
    }

    
    public function addBuyerManageAsync(array $data)
    {
        return EellyClient::request('user/buyerManage', 'addBuyerManage', false, $data);
    }

    
    public function updateBuyerManage(array $data): bool
    {
        return EellyClient::request('user/buyerManage', 'updateBuyerManage', true, $data);
    }

    
    public function updateBuyerManageAsync(array $data)
    {
        return EellyClient::request('user/buyerManage', 'updateBuyerManage', false, $data);
    }

    
    public function moveBuyerManage(array $manageData, array $newAdmin, array $handleAdmin): bool
    {
        return EellyClient::request('user/buyerManage', 'moveBuyerManage', true, $manageData, $newAdmin, $handleAdmin);
    }

    
    public function moveBuyerManageAsync(array $manageData, array $newAdmin, array $handleAdmin)
    {
        return EellyClient::request('user/buyerManage', 'moveBuyerManage', false, $manageData, $newAdmin, $handleAdmin);
    }

    
    public function getBuyerManageSummary(array $condition = []): array
    {
        return EellyClient::request('user/buyerManage', 'getBuyerManageSummary', true, $condition);
    }

    
    public function getBuyerManageSummaryAsync(array $condition = [])
    {
        return EellyClient::request('user/buyerManage', 'getBuyerManageSummary', false, $condition);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}