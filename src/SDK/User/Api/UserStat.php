<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\User\Api;

use Eelly\SDK\EellyClient as Client20191211;
use Eelly\SDK\GetInstanceTrait;

class UserStat
{
    use GetInstanceTrait;
    /**
     * 获取用户新增数量
     *
     * @param integer $duration
     * @param integer $startTime
     * @param integer $endTime
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.28
     */
    public function getUserIncreNum(int $duration, int $startTime = 0, int $endTime = 0):array
    {
        return Client20191211::requestJson('user/UserStat', __FUNCTION__, [
            'duration' => $duration,
            'startTime' => $startTime,
            'endTime' => $endTime,
        ]);
    }

    /**
     * 获取用户日活量
     *
     * @param integer $duration
     * @param integer $startTime
     * @param integer $endTime
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.29
     */
    public function getUserActiveNum(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return Client20191211::requestJson('user/UserStat', __FUNCTION__, [
            'duration' => $duration,
            'startTime' => $startTime,
            'endTime' => $endTime,
        ]);
    }

    /**
     * 获取店铺日活量
     *
     * @param integer $duration
     * @param integer $startTime
     * @param integer $endTime
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.30
     */
    public function getSellerActiveNum(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return Client20191211::requestJson('user/UserStat', __FUNCTION__, [
            'duration' => $duration,
            'startTime' => $startTime,
            'endTime' => $endTime,
        ]);
    }

    /**
     * 平台用户总量
     *
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.29
     */
    public function getUserTotalNum(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return Client20191211::requestJson('user/UserStat', __FUNCTION__, [
            'duration' => $duration,
            'startTime' => $startTime,
            'endTime' => $endTime,
        ]);
    }

    /**
     * 平台用户来源渠道占比
     *
     * @param integer $duration
     * @param integer $startTime
     * @param integer $endTime
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.29
     */
    public function getUserSourceProportion(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return Client20191211::requestJson('user/UserStat', __FUNCTION__, [
            'duration' => $duration,
            'startTime' => $startTime,
            'endTime' => $endTime,
        ]);
    }
}