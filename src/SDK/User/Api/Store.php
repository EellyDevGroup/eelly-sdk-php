<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\User\Api;

use Eelly\SDK\EellyClient as Client20191231;
use Eelly\SDK\GetInstanceTrait;
use Eelly\DTO\UidDTO;

/**
 * This class has been auto-generated by shadon compiler (2019-12-31 02:24:50).
 */
class Store
{
    use GetInstanceTrait;
    /**
     * 我的店铺.
     *
     * ### 返回数据说明
     *  参数        | 类型  | 说明
     * ----------- | ---- |-----
     * items[].storeId  | int  | 店铺ID
     * items[].storeName  | string  | 店铺名称
     * items[].storeLogo  | string  | 店铺LOGO
     * items[].address   | string  | 店铺地址
     * items[].date  | string  | 店铺最新浏览日期  YYYY-MM-dd
     * items[].newProduct | bool | 统计期间是否有发布商品
     * items[].buyTimes | int | 购买次数
     *
     * @param int         $currentPage 第几页
     * @param int         $perPage     分页大小
     * @param UidDTO|null $uidDTO      需要登录
     *
     * @requestExample({"currentPage":1, "perPage":10})
     *
     * @return array
     *
     *
     *
     * @returnExample( {
     *  "items": [
     *  {
     *  "storeId": "137655",
     *  "storeName": "群聊普通成员",
     *  "storeLogo": "https://img06.eelly.com/404.jpg?x-oss-process=image%2Fresize%2Cw_100",
     *  "address": "广东 广州 越秀 站前流花商圈 白马服装批发市场 -1层 7117",
     *  "date": "2019-12-31",
     *  "newProduct": false,
     *  "buyTimes": 0
     *  }
     *  ],
     *  "first": 1,
     *  "before": 1,
     *  "previous": 1,
     *  "current": 1,
     *  "last": 1,
     *  "next": 1,
     *  "totalPages": 1,
     *  "totalItems": 7,
     *  "limit": 10
     *  })
     *
     * @return array
     *
     * @author hehui<runphp@dingtalk.com>
     *
     * @ChangeLog([
     *     ["2019-12-31","创建接口","hehui<runphp@dingtalk.com>"]
     * ])
     */
    public static function myStore(int $currentPage = 0, int $perPage = 10, UidDTO $uidDTO = null) : array
    {
        return Client20191231::requestJson('user/store', 'myStore', ['currentPage' => $currentPage, 'perPage' => $perPage], true);
    }
    /**
     * 我的店铺.
     *
     * ### 返回数据说明
     *  参数        | 类型  | 说明
     * ----------- | ---- |-----
     * items[].storeId  | int  | 店铺ID
     * items[].storeName  | string  | 店铺名称
     * items[].storeLogo  | string  | 店铺LOGO
     * items[].address   | string  | 店铺地址
     * items[].date  | string  | 店铺最新浏览日期  YYYY-MM-dd
     * items[].newProduct | bool | 统计期间是否有发布商品
     * items[].buyTimes | int | 购买次数
     *
     * @param int         $currentPage 第几页
     * @param int         $perPage     分页大小
     * @param UidDTO|null $uidDTO      需要登录
     *
     * @requestExample({"currentPage":1, "perPage":10})
     *
     * @return array
     *
     *
     *
     * @returnExample( {
     *  "items": [
     *  {
     *  "storeId": "137655",
     *  "storeName": "群聊普通成员",
     *  "storeLogo": "https://img06.eelly.com/404.jpg?x-oss-process=image%2Fresize%2Cw_100",
     *  "address": "广东 广州 越秀 站前流花商圈 白马服装批发市场 -1层 7117",
     *  "date": "2019-12-31",
     *  "newProduct": false,
     *  "buyTimes": 0
     *  }
     *  ],
     *  "first": 1,
     *  "before": 1,
     *  "previous": 1,
     *  "current": 1,
     *  "last": 1,
     *  "next": 1,
     *  "totalPages": 1,
     *  "totalItems": 7,
     *  "limit": 10
     *  })
     *
     * @return array
     *
     * @author hehui<runphp@dingtalk.com>
     *
     * @ChangeLog([
     *     ["2019-12-31","创建接口","hehui<runphp@dingtalk.com>"]
     * ])
     */
    public static function myStoreAsync(int $currentPage = 0, int $perPage = 10, UidDTO $uidDTO = null)
    {
        return Client20191231::requestJson('user/store', 'myStore', ['currentPage' => $currentPage, 'perPage' => $perPage], false);
    }
    /**
     * 添加店铺访问记录.
     *
     * @param int         $storeId 店铺ID
     * @param UidDTO|null $uidDTO  需要登录
     *
     * @requestExample({"storeId":148086})
     *
     * @return bool
     *
     * @returnExample(true)
     *
     * @author hehui<runphp@dingtalk.com>
     *
     * @ChangeLog([
     *     ["2019-12-31","创建接口","hehui<runphp@dingtalk.com>"]
     * ])
     */
    public static function addStore(int $storeId, UidDTO $uidDTO = null) : bool
    {
        return Client20191231::requestJson('user/store', 'addStore', ['storeId' => $storeId], true);
    }
    /**
     * 添加店铺访问记录.
     *
     * @param int         $storeId 店铺ID
     * @param UidDTO|null $uidDTO  需要登录
     *
     * @requestExample({"storeId":148086})
     *
     * @return bool
     *
     * @returnExample(true)
     *
     * @author hehui<runphp@dingtalk.com>
     *
     * @ChangeLog([
     *     ["2019-12-31","创建接口","hehui<runphp@dingtalk.com>"]
     * ])
     */
    public static function addStoreAsync(int $storeId, UidDTO $uidDTO = null)
    {
        return Client20191231::requestJson('user/store', 'addStore', ['storeId' => $storeId], false);
    }
    /**
     * 删除店铺访问记录.
     *
     * @param int         $storeId 店铺ID
     * @param UidDTO|null $uidDTO  需要登录
     *
     * @return bool
     *
     * @returnExample(true)
     *
     * @author hehui<runphp@dingtalk.com>
     *
     * @ChangeLog([
     *     ["2019-12-31","创建接口","hehui<runphp@dingtalk.com>"]
     * ])
     */
    public static function deleteStore(int $storeId, UidDTO $uidDTO = null) : bool
    {
        return Client20191231::requestJson('user/store', 'deleteStore', ['storeId' => $storeId], true);
    }
    /**
     * 删除店铺访问记录.
     *
     * @param int         $storeId 店铺ID
     * @param UidDTO|null $uidDTO  需要登录
     *
     * @return bool
     *
     * @returnExample(true)
     *
     * @author hehui<runphp@dingtalk.com>
     *
     * @ChangeLog([
     *     ["2019-12-31","创建接口","hehui<runphp@dingtalk.com>"]
     * ])
     */
    public static function deleteStoreAsync(int $storeId, UidDTO $uidDTO = null)
    {
        return Client20191231::requestJson('user/store', 'deleteStore', ['storeId' => $storeId], false);
    }
    /**
     * 添加店铺访问记录.
     *
     * @param int $storeId
     * @param int $uid
     *
     * @return bool
     *
     * @internal
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function addStoreInternal(int $storeId, int $uid) : bool
    {
        return Client20191231::requestJson('user/store', 'addStoreInternal', ['storeId' => $storeId, 'uid' => $uid], true);
    }
    /**
     * 添加店铺访问记录.
     *
     * @param int $storeId
     * @param int $uid
     *
     * @return bool
     *
     * @internal
     *
     * @author hehui<runphp@dingtalk.com>
     */
    public static function addStoreInternalAsync(int $storeId, int $uid)
    {
        return Client20191231::requestJson('user/store', 'addStoreInternal', ['storeId' => $storeId, 'uid' => $uid], false);
    }
}