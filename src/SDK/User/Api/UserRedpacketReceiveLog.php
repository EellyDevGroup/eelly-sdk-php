<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\User\Api;

use Eelly\SDK\EellyClient;

/**
 * This class has been auto-generated by shadon compiler (2020-05-06 01:58:33).
 */
class UserRedpacketReceiveLog
{
    /**
     * 添加用户红包领取记录
     *
     * @param array $addData 添加的参数
     * @param int $addData['userId'] 添加的用户id
     * @param int $addData['type'] 红包类型 1.直播
     * @param string $addData['remark'] 记录备注
     * @param int $addData['money'] 红包金额
     * @return bool
     *
     * @author wechan
     * @since 2020年12月15日
     */
    public function addReceiveLog(array $addData):bool
    {
        return EellyClient::requestJson('user/userRedpacketReceiveLog', __FUNCTION__, ['addData' => $addData], true);
    }
}
