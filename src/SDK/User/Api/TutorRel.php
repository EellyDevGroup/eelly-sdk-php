<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\User\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class TutorRel
{
    public function getTutorRelInternal(int $userId)
    {
        return EellyClient::request('user/tutorRel', __FUNCTION__, true, $userId);
    }

    public static function getTutorRelInternalAsync(int $userId)
    {
        return EellyClient::requestJson('user/tutorRel', 'getTutorRelInternal', ['userId' => $userId], false);
    }

    /**
     * @inheritdoc
     */
    public function listFanNumInfo(array $userIds):array
    {
        return EellyClient::requestJson('user/tutorRel', __FUNCTION__,  ['userIds' => $userIds]);
    }

    /**
     * @inheritdoc
     */
    public function listUserTutorRel(array $userIds):array
    {
        return EellyClient::requestJson('user/tutorRel', __FUNCTION__,  ['userIds' => $userIds]);
    }
}