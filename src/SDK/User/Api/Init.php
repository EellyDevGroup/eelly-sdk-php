<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\User\Api;

use Eelly\DTO\UidDTO;
use Eelly\SDK\EellyClient;

/**
 * class Init
 * 
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class Init
{
    public static function checkUserIsNew(int $userId, bool $isApplet = false): bool
    {
        return EellyClient::requestJson('user/init', __FUNCTION__, [
            'userId' => $userId,
            'isApplet' => $isApplet,
        ]);
    }

    public static function checkUserIsNewV2(int $userId, bool $isApplet = false): bool
    {
        return EellyClient::requestJson('user/init', __FUNCTION__, [
            'userId' => $userId,
            'isApplet' => $isApplet,
        ]);
    }
}