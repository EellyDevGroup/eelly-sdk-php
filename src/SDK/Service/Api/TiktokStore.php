<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Service\Api;

use Eelly\DTO\UidDTO;
use Eelly\SDK\EellyClient;

/**
 * 抖店接口
 */
class TiktokStore
{
    /**
     * 获取抖音商品列表
     * 文档地址：https://op.jinritemai.com/docs/api-docs/14/57
     *
     * @param int $userId 用户id
     * @param int $page 页吗
     * @param int $size 每页返回条数，最多支持100条
     *
     * @author wechan
     * @since 2021年4月02日
     */
    public function getProductList(int $userId, int $page = 1, int $size = 100):array
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__,[
            'userId' => $userId,
            'page' => $page,
            'size' => $size
        ]);
    }

    /**
     * 抖店商品发布 https://op.jinritemai.com/docs/api-docs/14/249
     *
     * @param int $userId 用户id
     * @param array $param 商品发布参数
     *
     * @author wechan
     * @since 2021年03月31日
     */
    public function productAddV2(int $userId, array $param):array
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__,[
            'userId' => $userId,
            'param' => $param,
        ]);
    }

    /**
     * 商品详情
     *
     * @param int $userId
     * @param int $productId 商品id
     * @return array
     *
     * @author wechan
     * @since 2021年03月31日
     */
    public function productDetail(int $userId, string $productId):array
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__,[
            'userId' => $userId,
            'productId' => $productId,
        ]);
    }

    /**
     * 商品编辑新接口
     *
     * @param int $userId 用户id
     * @param array $param 商品编辑参数
     * @return array
     *
     * @author wechan
     * @since 2021年03月31日
     */
    public function productEditV2(int $userId, array $param):array
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__,[
            'userId' => $userId,
            'param' => $param,
        ]);
    }

    /**
     * 删除抖音商品
     *
     * @param int $userId 用户id
     * @param int $productId 商品id
     * @return bool
     *
     * @author twb<1174865138@qq.com>
     * @since 2021-05-07T11:50:23+0800
     */
    public function deleteTiktokGoods(int $userId, int $productId):bool
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__,[
            'userId' => $userId,
            'productId' => $productId,
        ]);
    }

    /**
     * 设置抖音商品下架
     *
     * @param int $userId 用户id
     * @param int $productId 商品id
     * @return bool
     *
     * @author wechan
     * @since 2021年04月08日
     */
    public function productSetOffLine(int $userId, int $productId):bool
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__,[
            'userId' => $userId,
            'productId' => $productId,
        ]);
    }

    /**
     * 设置抖音商品上架
     *
     * @param int $userId 用户id
     * @param int $productId 商品id
     * @return bool
     *
     * @author wechan
     * @since 2021年04月08日
     */
    public function productSetOnLine(int $userId, int $productId):bool
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__,[
            'userId' => $userId,
            'productId' => $productId,
        ]);
    }

    /**
     * 查询订购服务时间
     *
     * @param int $userId 用户登录信息
     *
     * > 返回数据说明
     *
     * key | type | desc
     * --- | ---- | ----
     * expireValue | string | 到期值
     * rightsType | string |  0: 试用服务，1:正式服务
     * shopName | string | 抖音店铺名
     * shopId | string | 抖音店铺id
     * tiktokManageUrl | string | 抖音后台地址
     * tiktokAuthUrl | string | 抖音店铺授权地址
     *
     * @returnExample({"expireValue":"1天","rightsType":"1","shopName":"xxx","shopId":"222","tiktokManageUrl":"www.baidu.com","tiktokAuthUrl":"www.baidu.com"})
     *
     * @author wechan
     * @since 2021年03月29日
     * @return array
     */
    public function getInternalRightsInfo(int $userId):array
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__,[
            'userId' => $userId,
        ]);
    }

    /**
     * 检查是否有抖音小店
     *
     * @param int $userId 用户登录信息
     *
     * @author twb<1174865138@qq.com>
     * @since 2021年04月27日
     * @return bool
     */
    public function checkTiktokId(int $userId): bool
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__, [
            'userId' => $userId,
        ]);
    }

    /**
     * 设置授权accessToken
     *
     * @param string $code 授权码
     * @param UidDTO $user 登录用户信息
     *
     * @author wechan
     * @since 2021年04月19日
     * @return bool
     */
    public function setAccessToken(string $code, UidDTO $user = null):bool
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__, [
            'code' => $code
        ]);
    }

    /**
     * 获取店铺后台供商家发布商品的类目
     *
     * @param int $userId
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.28
     */
    public function getShopCategory(int $cid, UidDTO $user = null): array
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__, [
            'cid' => $cid
        ]);
    }

    /**
     * 获取店铺后台供商家发布商品的类目
     *
     * @param int $userId
     * @return array
     *
     * @author chenyuhua
     * @since 2021.04.28
     */
    public function getRightsInfo(UidDTO $user = null): array
    {
        return EellyClient::requestJson('service/tiktokStore', __FUNCTION__);
    }
}
