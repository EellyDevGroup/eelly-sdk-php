<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Service\Api;

use Eelly\DTO\UidDTO;
use Eelly\SDK\EellyClient;

/**
 * 抖店接口
 */
class WechatRobot
{
    /**
     * 直播开启后,机器人发送消息
     *
     * @param array $phones 微信用户手机号
     * @param array $data 替换模板的字符串
     *
     * @return bool
     *
     * @author twb<1174865138@qq.com>
     * @since 2021-06-23T15:25:26+0800
     */
    public function liveStartSendSms(array $phones = [], array $data = []):bool
    {
        return EellyClient::requestJson('service/wechatRobot', __FUNCTION__,[
            'phones' => $phones,
            'data' => $data
        ]);
    }

    /**
     * 支付冲榜红包后,机器人发送消息
     *
     * @param array $phones 微信用户手机号
     * @param array $data 替换模板的字符串
     *
     * @return bool
     *
     * @author twb<1174865138@qq.com>
     * @since 2021-06-23T15:25:26+0800
     */
    public function liveRedPacketPayedSendSms(array $phones = [], array $data = []):bool
    {
        return EellyClient::requestJson('service/wechatRobot', __FUNCTION__,[
            'phones' => $phones,
            'data' => $data
        ]);
    }

    public function OrderCreatedSendSms(array $phones = [], array $data = []):bool
    {
        return EellyClient::requestJson('service/wechatRobot', __FUNCTION__,[
            'phones' => $phones,
            'data' => $data
        ]);
    }
}
