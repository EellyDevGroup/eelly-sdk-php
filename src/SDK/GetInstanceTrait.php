<?php


namespace Eelly\SDK;


trait GetInstanceTrait
{
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}