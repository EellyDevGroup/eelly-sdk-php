<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Advertiser\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class AdvertiserLog
{
    public static function getRadoRecordContact(int $advertiserId, int $alId, int $userId): array
    {
        return EellyClient::requestJson('advertiser/advertiserLog', __FUNCTION__, [
            'advertiserId' => $advertiserId,
            'alId' => $alId,
            'userId' => $userId,
        ]);
    }
}