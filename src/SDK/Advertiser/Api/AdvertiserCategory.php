<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Advertiser\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class AdvertiserCategory
{
    public static function getAdvertiserCateList(int $advertiserId, array $condition, int $page = 1, $limit = 20): array
    {
        return EellyClient::requestJson('advertiser/advertiserCategory', __FUNCTION__, [
            'advertiserId' => $advertiserId,
            'condition' => $condition,
            'page' => $page,
            'limit' => $limit
        ]);
    }

    public static function updateAdvertiserCateInfo(int $acId, array $data): array
    {
        return EellyClient::requestJson('advertiser/advertiserCategory', __FUNCTION__, [
            'acId' => $acId,
            'data' => $data,
        ]);
    }

    public static function createAdvertiserCate(int $advertiserId, array $data): array
    {
        return EellyClient::requestJson('advertiser/advertiserCategory', __FUNCTION__, [
            'advertiserId' => $advertiserId,
            'data' => $data,
        ]);
    }

    public static function deleteAdvertiserCate(int $acId): array
    {
        return EellyClient::requestJson('advertiser/advertiserCategory', __FUNCTION__, [
            'acId' => $acId
        ]);
    }
}