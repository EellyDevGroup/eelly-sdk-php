<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Advertiser\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class AdvertiserGoods
{
    public static function getAdvertiserGoodsList(int $advertiserId, array $condition, int $page = 1, $limit = 20): array
    {
        return EellyClient::requestJson('advertiser/advertiserGoods', __FUNCTION__, [
            'advertiserId' => $advertiserId,
            'condition' => $condition,
            'page' => $page,
            'limit' => $limit
        ]);
    }

    public static function updateAdvertiserGoodsInfo(int $agId, array $data): array
    {
        return EellyClient::requestJson('advertiser/advertiserGoods', __FUNCTION__, [
            'agId' => $agId,
            'data' => $data,
        ]);
    }

    public static function createAdvertiserGoods(int $advertiserId, array $data): array
    {
        return EellyClient::requestJson('advertiser/advertiserGoods', __FUNCTION__, [
            'advertiserId' => $advertiserId,
            'data' => $data,
        ]);
    }

    public static function deleteAdvertiserGoods(int $agId): array
    {
        return EellyClient::requestJson('advertiser/advertiserGoods', __FUNCTION__, [
            'agId' => $agId
        ]);
    }

    public static function getAdvertiserGoodsByAgIds(array $agIds): array
    {
        return EellyClient::requestJson('advertiser/advertiserGoods', __FUNCTION__, [
            'agIds' => $agIds,
        ]);
    }

    public static function getAdvertiserGoodsDetailPage(int $agId): array
    {
        return EellyClient::requestJson('advertiser/advertiserGoods', __FUNCTION__, [
            'agId' => $agId,
        ]);
    }
}