<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Advertiser\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class Advertiser
{
    public static function getAdvertiserList(array $condition, int $page = 1, $limit = 20): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'condition' => $condition,
            'page' => $page,
            'limit' => $limit
        ]);
    }

    public static function getAdvertiserDetail(int $advertiserId): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserId' => $advertiserId,
        ]);
    }

    public static function updateAdvertiserInfo(int $advertiserId, array $data): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserId' => $advertiserId,
            'data' => $data,
        ]);
    }

    public static function createAdvertiser(int $userId, int $adminId = 0): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'userId' => $userId,
            'adminId' => $adminId,
        ]);
    }

    public static function getAdvertiserByAdvertiserIds(array $advertiserIds): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserIds' => $advertiserIds,
        ]);
    }

    public static function getAdvertiserDetailPage(int $advertiserId): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserId' => $advertiserId,
        ]);
    }

    public static function getAdvertiserRadoList(array $condition, int $page = 1, int $limit = 20): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'condition' => $condition,
            'page' => $page,
            'limit' => $limit,
        ]);
    }

    public static function getAdvertiserRadoDetail(int $advertiserId): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserId' => $advertiserId,
        ]);
    }

    public static function getAdvertiserRadoSetting(int $advertiserId): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserId' => $advertiserId,
        ]);
    }

    public static function getAdvertiserRadoUse(int $advertiserId): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserId' => $advertiserId,
        ]);
    }

    public static function updateAdvertiserRadoAuth(int $advertiserId, int $num, int $adminId = 0, string $adminName = ''): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserId' => $advertiserId,
            'num' => $num,
            'adminId' => $adminId,
            'adminName' => $adminName,
        ]);
    }

    public static function increaseAdvertiserRadoNum(int $advertiserId, int $num, int $adminId = 0, string $adminName = ''): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserId' => $advertiserId,
            'num' => $num,
            'adminId' => $adminId,
            'adminName' => $adminName,
        ]);
    }

    public static function updateAdvertiserRadoStatus(int $advertiserId, int $status, int $adminId = 0, string $adminName = ''): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserId' => $advertiserId,
            'status' => $status,
            'adminId' => $adminId,
            'adminName' => $adminName,
        ]);
    }

    public static function searchRadoRecord(int $advertiserId, array $info): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserId' => $advertiserId,
            'info' => $info,
        ]);
    }

    public static function getAdvertiserByAdvertiserName(string $advertiserName): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserName' => $advertiserName,
        ]);
    }

    public static function updateAdvertiserRadoSetting(int $advertiserId, array $data, int $adminId = 0, string $adminName = ''): array
    {
        return EellyClient::requestJson('advertiser/advertiser', __FUNCTION__, [
            'advertiserId' => $advertiserId,
            'data' => $data,
            'adminId' => $adminId,
            'adminName' => $adminName,
        ]);
    }
}