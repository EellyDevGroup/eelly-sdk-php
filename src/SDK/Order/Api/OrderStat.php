<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Order\Api;

use Eelly\SDK\EellyClient;
use Eelly\SDK\Order\Service\OrderStatInterface;

/**
 * @author shadonTools<localhost.shell@gmail.com>
 */
class OrderStat implements OrderStatInterface
{
    /**
     * 统计卖家n天内订单.
     *
     * @internal
     *
     * @param array  $sellerIds 店铺ID数组
     * @param int    $day       n天内
     * @param array  $status    订单状态，默认全部
     * @param string $mode      统计内容，num 订单总数 amount 订单总金额
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019-04-22
     */
    public function getSellerOrderStatDay(array $sellerIds, int $day, array $status = [], string $mode): array
    {
        return EellyClient::request('order/orderStat', 'getSellerOrderStatDay', true, $sellerIds, $day, $status, $mode);
    }

    /**
     * 统计卖家n天内订单.
     *
     * @internal
     *
     * @param array  $sellerIds 店铺ID数组
     * @param int    $day       n天内
     * @param array  $status    订单状态，默认全部
     * @param string $mode      统计内容，num 订单总数 amount 订单总金额
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019-04-22
     */
    public function getSellerOrderStatDayAsync(array $sellerIds, int $day, array $status = [], string $mode)
    {
        return EellyClient::request('order/orderStat', 'getSellerOrderStatDay', false, $sellerIds, $day, $status, $mode);
    }

    /**
     * 统计多个买家在一个店铺的订单总数(付款、完成.etc).
     *
     * @internal
     *
     * @param array $buyerIds
     * @param int   $sellerId
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019-04-23
     */
    public function countBuyersOrder(array $buyerIds, int $sellerId): array
    {
        return EellyClient::request('order/orderStat', 'countBuyersOrder', true, $buyerIds, $sellerId);
    }

    /**
     * 统计多个买家在一个店铺的订单总数(付款、完成.etc).
     *
     * @internal
     *
     * @param array $buyerIds
     * @param int   $sellerId
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019-04-23
     */
    public function countBuyersOrderAsync(array $buyerIds, int $sellerId)
    {
        return EellyClient::request('order/orderStat', 'countBuyersOrder', false, $buyerIds, $sellerId);
    }

    /**
     * 获取n天内某店铺的订单统计，按买家分组.
     *
     * @param int   $day
     * @param int   $sellerId
     * @param array $buyerIds
     * @param array $extend
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019/5/9
     */
    public function statSellerOrderDayBefore(int $day, int $sellerId, array $buyerIds, array $extend = []): array
    {
        return EellyClient::request('order/orderStat', 'statSellerOrderDayBefore', true, $day, $sellerId, $buyerIds, $extend);
    }

    /**
     * 获取n天内某店铺的订单统计，按买家分组.
     *
     * @param int   $day
     * @param int   $sellerId
     * @param array $buyerIds
     * @param array $extend
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019/5/9
     */
    public function statSellerOrderDayBeforeAsync(int $day, int $sellerId, array $buyerIds, array $extend = [])
    {
        return EellyClient::request('order/orderStat', 'statSellerOrderDayBefore', false, $day, $sellerId, $buyerIds, $extend);
    }

    /**
     * @param array $condition
     * @param array $extend
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019/5/16
     */
    public function statOrderForPayScore(array $condition, array $extend = []): array
    {
        return EellyClient::request('order/orderStat', 'statOrderForPayScore', true, $condition, $extend);
    }

    /**
     * @param array $condition
     * @param array $extend
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019/5/16
     */
    public function statOrderForPayScoreAsync(array $condition, array $extend = [])
    {
        return EellyClient::request('order/orderStat', 'statOrderForPayScore', false, $condition, $extend);
    }

    /**
     * 统计一个店铺多个买家的支付订单(支付了就算).
     *
     * @param array $condition
     * @param array $extend
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019/5/16
     */
    public function statPayedOrder(array $condition, array $extend = []): array
    {
        return EellyClient::request('order/orderStat', 'statPayedOrder', true, $condition, $extend);
    }

    /**
     * 统计一个店铺多个买家的支付订单(支付了就算).
     *
     * @param array $condition
     * @param array $extend
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019/5/16
     */
    public function statPayedOrderAsync(array $condition, array $extend = [])
    {
        return EellyClient::request('order/orderStat', 'statPayedOrder', false, $condition, $extend);
    }

    /**
     * 统计一个店铺多个买家的完成订单.
     *
     * @param array $condition
     * @param array $extend
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019/5/16
     */
    public function statFinishedOrder(array $condition, array $extend = []): array
    {
        return EellyClient::request('order/orderStat', 'statFinishedOrder', true, $condition, $extend);
    }

    /**
     * 统计一个店铺多个买家的完成订单.
     *
     * @param array $condition
     * @param array $extend
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019/5/16
     */
    public function statFinishedOrderAsync(array $condition, array $extend = [])
    {
        return EellyClient::request('order/orderStat', 'statFinishedOrder', false, $condition, $extend);
    }

    /**
     * 分页取店铺昨日支付转化分订单.
     *
     * @param int $sellerId
     * @param int $page
     * @param int $limit
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019/5/17
     */
    public function getOrderYesterdayForPayScore(int $sellerId, int $page, int $limit): array
    {
        return EellyClient::request('order/orderStat', 'getOrderYesterdayForPayScore', true, $sellerId, $page, $limit);
    }

    /**
     * 分页取店铺昨日支付转化分订单.
     *
     * @param int $sellerId
     * @param int $page
     * @param int $limit
     *
     * @return array
     *
     * @author zhangyangxun
     *
     * @since 2019/5/17
     */
    public function getOrderYesterdayForPayScoreAsync(int $sellerId, int $page, int $limit)
    {
        return EellyClient::request('order/orderStat', 'getOrderYesterdayForPayScore', false, $sellerId, $page, $limit);
    }

    public static function paySuccessOrders(array $buyerIds, array $sellerIds): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, ['buyerIds' => $buyerIds, 'sellerIds' => $sellerIds]);
    }
    
    public static function getBuyerOrderStatCurrentMonth(array $buyerIds): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, ['buyerIds' => $buyerIds]);
    }

    public static function getBuyerOrderStat(array $buyerIds): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, ['buyerIds' => $buyerIds]);
    }

    public static function getVideoIndexStore(array $condition): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, ['condition' => $condition]);
    }

    public static function getSpreadOrderStatByLive(array $conditions): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, ['conditions' => $conditions]);
    }

    public static function statLiveRoomOrder(int $storeId, int $liveBeginTime): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, ['storeId' => $storeId, 'liveBeginTime' => $liveBeginTime]);
    }

    public static function getStorePayedBuyers(int $storeId, int $page = 1, int $limit = 30): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, ['storeId' => $storeId, 'page' => $page, 'limit' => $limit]);
    }

    /**
     * 获取平台交易统计数据
     *
     * @param int $duration 周期 0 自定义时间段 1 今天 2 近7天 3 近30天
     * @param int $startTime 自定义时间段开始时间戳
     * @param int $endTime 自定义时间段结束时间戳
     * @return array
     * @throws \Throwable
     *
     * @author chenyuhua
     * @since 2021.08.28
     */
    public function getPlatformOrderStat(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, [
            'duration'  => $duration,
            'startTime' => $startTime,
            'endTime'   => $endTime,
        ]);
    }

    /**
     * 获取交易核心指标趋势图表数据
     *
     * @param int $duration 周期 0 自定义时间段 1 今天 2 近7天 3 近30天
     * @param string $target  指标 buyerNum 支付人数 payAmount 支付金额 payNum 支付笔数 refundAmount 退款金额
     * @param int $startTime 自定义时间段开始时间戳
     * @param int $endTime 自定义时间段结束时间戳
     * @return array
     *
     * @author chenyuhua
     * @since 2021.08.28
     */
    public function getOrderStatChart(int $duration, string $target, int $startTime = 0, int $endTime = 0): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, [
            'duration'  => $duration,
            'target'    => $target,
            'startTime' => $startTime,
            'endTime'   => $endTime,
        ]);
    }

    /**
     * 获取交易用户来源分析图表数据
     *
     * @param int $type 地区类型 0 省(直辖市) 1 区县镇
     * @param int $duration 周期 0 自定义时间段 1 今天 2 近7天 3 近30天
     * @param string $target  指标 buyerNum 支付人数 payAmount 支付金额 payNum 支付笔数
     * @param int $startTime 自定义时间段开始时间戳
     * @param int $endTime 自定义时间段结束时间戳
     * @return array
     *
     * @author chenyuhua
     * @since 2021.08.28
     */
    public function getAreaOrderStatChart(int $type, int $duration, string $target, int $startTime = 0, int $endTime = 0): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, [
            'type'      => $type,
            'duration'  => $duration,
            'target'    => $target,
            'startTime' => $startTime,
            'endTime'   => $endTime,
        ]);
    }

    /**
     * 获取交易用户地域分布详情数据列表
     *
     * @param int $type 地区类型 0 省(直辖市) 1 区县镇
     * @param int $duration 周期 0 自定义时间段 1 今天 2 近7天 3 近30天
     * @param int $startTime 自定义时间段开始时间戳
     * @param int $endTime 自定义时间段结束时间戳
     * @param int $page 页数
     * @param int $limit 每页限制
     * @return array
     *
     * @author chenyuhua
     * @since 2021.08.28
     */
    public function getAreaOrderStatList(int $type, int $duration, int $startTime = 0, int $endTime = 0, int $page = 1, int $limit = 100): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, [
            'type'      => $type,
            'duration'  => $duration,
            'startTime' => $startTime,
            'endTime'   => $endTime,
            'page'      => $page,
            'limit'     => $limit,
        ]);
    }

    /**
     * 获取头部采购商排行
     *
     * @param integer $duration
     * @param integer $startTime
     * @param integer $endTime
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.29
     */
    public function getBuyerRanking(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, [
            'duration'  => $duration,
            'startTime' => $startTime,
            'endTime'   => $endTime,
        ]);
    }

    /**
     * 获取头部商家排行
     *
     * @param integer $duration
     * @param integer $startTime
     * @param integer $endTime
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.30
     */
    public function getSellerRanking(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, [
            'duration'  => $duration,
            'startTime' => $startTime,
            'endTime'   => $endTime,
        ]);
    }

    /**
     * 获取头部商品排行
     *
     * @param integer $duration
     * @param integer $startTime
     * @param integer $endTime
     * @return array
     *
     * @author chenyuhua
     * @since 2023.03.30
     */
    public function getGoodsRanking(int $duration, int $startTime = 0, int $endTime = 0): array
    {
        return EellyClient::requestJson('order/orderStat', __FUNCTION__, [
            'duration'  => $duration,
            'startTime' => $startTime,
            'endTime'   => $endTime,
        ]);
    }

    /**
     * @return self
     */
    public static function getInstance(): self
    {
        static $instance;
        if (null === $instance) {
            $instance = new self();
        }

        return $instance;
    }
}
