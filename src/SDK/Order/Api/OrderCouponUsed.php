<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Order\Api;

use Eelly\SDK\EellyClient as Client20200527;
use Eelly\SDK\GetInstanceTrait;

class OrderCouponUsed
{
    use GetInstanceTrait;

    public static function couponUsedStat(array $couponIds): array
    {
        return Client20200527::requestJson('order/orderCouponUsed', __FUNCTION__, ['couponIds' => $couponIds]);
    }

    public static function couponUsedLog(array $condition, string $fieldScope): array
    {
        return Client20200527::requestJson('order/orderCouponUsed', __FUNCTION__, [
            'condition' => $condition,
            'fieldScope' => $fieldScope,
        ]);
    }

    public static function orderAmountStatisticsByAcoIds(array $acoIds, array $extra = []): array
    {
        return Client20200527::requestJson('order/orderCouponUsed', __FUNCTION__, [
            'acoIds' => $acoIds,
            'extra' => $extra
        ]);
    }

    public static function orderAmountStatisticsByAcoIdsAndUserIds(array $acoIdsAndUserIds, array $extra = []): array
    {
        return Client20200527::requestJson('order/orderCouponUsed', __FUNCTION__, [
            'acoIdsAndUserIds' => $acoIdsAndUserIds,
            'extra' => $extra
        ]);
    }
}
