<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Order\Api;

use Eelly\SDK\EellyClient;

class OrderAgent
{
    /**
     * 获取直播间带货订单列表
     *
     * @param int $liveId
     * @param int $salId
     * @param int $type     订单状态 1 待付款 2 已付款
     * @param int $page
     * @param int $limit
     * @return array
     *
     * @author chenyuhua
     * @since 2020.12.18
     */
    public static function getAgentOrderList(int $liveId, int $salId, int $type, int $page = 1, int $limit = 40): array
    {
        return EellyClient::requestJson('order/orderAgent', __FUNCTION__, [
            'liveId' => $liveId,
            'salId' => $salId,
            'type' => $type,
            'page' => $page,
            'limit' => $limit,
        ]);
    }

    /**
     * 获取带货订单列表数据量
     *
     * @param int $liveId
     * @param int $salId
     * @return array
     *
     * @author chenyuhua
     * @since 2020.12.19
     */
    public static function getAgentOrderSellNum(int $liveId, int $salId): array
    {
        return EellyClient::requestJson('order/orderAgent', __FUNCTION__, [
            'liveId' => $liveId,
            'salId' => $salId,
        ]);
    }

    /**
     * 检查下单的代理商信息
     *
     * @param int $orderId 订单id
     * @return array
     *
     * @author wechan
     * @since 2020年12月21日
     */
    public function checkOrderAgent(int $orderId):array
    {
        return EellyClient::requestJson('order/orderAgent', __FUNCTION__, [
            'orderId' => $orderId,
        ]);
    }

    /**
     * 添加订单代理信息
     *
     * @param array $orders 订单数组
     * @return array
     *
     * @author wechan
     * @since 2020年12月19日
     */
    public function addOrderAgent(array $orders):array
    {
        return EellyClient::requestJson('order/orderAgent', __FUNCTION__, [
            'orders' => $orders,
        ]);
    }

    /**
     * 获取店铺带货自提商品销量
     *
     * @param int $storeId
     * @param array $goodsIds
     * @return array
     *
     * @author chenyuhua
     * @since 2021.03.12
     */
    public function getStoreAgentGoodsSelfSellNum(int $storeId, array $goodsIds = []): array
    {
        return EellyClient::requestJson('order/orderAgent', __FUNCTION__, [
            'storeId'  => $storeId,
            'goodsIds' => $goodsIds,
        ]);
    }
}
