<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Im\Api;

use Eelly\SDK\EellyClient;

class TeamNotice
{
    public static function getStorePrepareMsg(int $storeId): array
    {
        return EellyClient::requestJson('im/teamNotice', __FUNCTION__, ['storeId' => $storeId]);
    }

    public static function sendOpenLive(int $storeId): bool
    {
        return EellyClient::requestJson('im/teamNotice', __FUNCTION__, ['storeId' => $storeId]);
    }

    public static function sendTeamLiveEnterMsg(string $tid, string $username): bool
    {
        return EellyClient::requestJson('im/teamNotice', __FUNCTION__, ['tid' => $tid, 'username' => $username]);
    }

    /**
     * 更新群通知数据.
     *
     * @param string      $tid    群id
     * @param array       $data   更新的数据(同查看群通知返回的数据)
     *
     * @return bool
     *
     * @author chenyuhua
     */
    public static function updateNoticeStatus(string $tid, array $data): bool
    {
        return EellyClient::requestJson('im/teamNotice', __FUNCTION__, ['tid' => $tid, 'data' => $data]);
    }
}
