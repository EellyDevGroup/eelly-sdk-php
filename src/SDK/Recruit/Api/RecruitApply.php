<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Recruit\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class RecruitApply
{
    public static function getRecruitApplyList(array $condition, int $page = 1, $limit = 20): array
    {
        return EellyClient::requestJson('recruit/recruitApply', __FUNCTION__, [
            'condition' => $condition,
            'page' => $page,
            'limit' => $limit
        ]);
    }

    public static function createRecruitApply(array $data, int $userId): array
    {
        return EellyClient::requestJson('recruit/recruitApply', __FUNCTION__, [
            'data' => $data,
            'userId' => $userId,
        ]);
    }

    public static function getRecruitApplyDetail(int $raId): array
    {
        return EellyClient::requestJson('recruit/recruitApply', __FUNCTION__, [
            'raId' => $raId,
        ]);
    }

    public static function getUserRecruitAccount(int $userId): array
    {
        return EellyClient::requestJson('recruit/recruitApply', __FUNCTION__, [
            'userId' => $userId,
        ]);
    }
}