<?php

declare(strict_types=1);
/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eelly\SDK\Recruit\Api;

use Eelly\SDK\EellyClient;

/**
 *
 * @author shadonTools<localhost.shell@gmail.com>
 */
class RecruitApplyLog
{
    public static function getRecruitApplyLog(int $raId): array
    {
        return EellyClient::requestJson('recruit/recruitApplyLog', __FUNCTION__, [
            'raId' => $raId,
        ]);
    }

    public static function createRecruitApplyLog(int $raId, string $content, bool $isCreate, int $adminId): bool
    {
        return EellyClient::requestJson('recruit/recruitApplyLog', __FUNCTION__, [
            'raId'      => $raId,
            'content'   => $content,
            'isCreate'  => $isCreate,
            'adminId'   => $adminId,
        ]);
    }
}